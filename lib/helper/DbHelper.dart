import 'package:flutter/cupertino.dart';
import 'package:sharon_cs/widget/barang/BarangModel.dart';
import 'package:sharon_cs/widget/cekstore/CsmodelDetail.dart';
import 'package:sharon_cs/widget/discount/DiscountDetailModel.dart';
import 'package:sharon_cs/widget/ordersales/OrderSalesDetailModel.dart';
import 'package:sharon_cs/widget/pelanggan/PelangganModel.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';

//kelass Dbhelper
class DbHelper {
  static DbHelper _dbHelper;
  static Database _database;

  DbHelper._createObject();

  factory DbHelper() {
    if (_dbHelper == null) {
      _dbHelper = DbHelper._createObject();
    }
    return _dbHelper;
  }

  Future<Database> initDb() async {
    //untuk menentukan nama database dan lokasi yg dibuat
    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path + 'cekstore.db';

    //create, read databases
    var todoDatabase = openDatabase(path, version: 1, onCreate: _createDb);

    //mengembalikan nilai object sebagai hasil dari fungsinya
    return todoDatabase;
  }

  //buat tabel baru dengan nama contact
  void _createDb(Database db, int version) async {
    Batch batch = db.batch();
    batch.execute("DROP TABLE IF EXISTS barang;");
    batch.execute("DROP TABLE IF EXISTS pelanggan;");
    batch.execute("DROP TABLE IF EXISTS discount_detail;");
    batch.execute("DROP TABLE IF EXISTS jwk;");
    batch.execute("DROP TABLE IF EXISTS cekstore;");
    batch.execute("DROP TABLE IF EXISTS cekstore_detail;");
    batch.execute("DROP TABLE IF EXISTS ordersales;");
    batch.execute("DROP TABLE IF EXISTS ordersales_detail;");
    batch.execute('''
      CREATE TABLE barang (
        kd_brg VARCHAR(255) PRIMARY KEY NOT NULL,
        nama VARCHAR(255) NOT NULL,
        harga DOUBLE NOT NULL 
      )
    ''');
    batch.execute('''
      CREATE TABLE pelanggan (
        kd_plg VARCHAR(255) NOT NULL,
        jp VARCHAR(255) NOT NULL,
        kd_baru VARCHAR(255) NOT NULL,
        nama VARCHAR(255) NOT NULL,
        alamat VARCHAR(255) NOT NULL,
        telp VARCHAR(255) NOT NULL,
        hp VARCHAR(255) NOT NULL,
        npwp VARCHAR(255) NOT NULL,
        kd_plg_gr VARCHAR(255) NOT NULL,
        nama_kd_plg_gr VARCHAR(255) NOT NULL,
        kd_discount VARCHAR(255) NOT NULL,
        kd_jalur VARCHAR(255) NOT NULL,
        nama_jalur VARCHAR(255) NOT NULL,
        kd_divisi VARCHAR(255) NOT NULL,
        nama_divisi VARCHAR(255) NOT NULL,
        latitude VARCHAR(255) NOT NULL,
        longitude VARCHAR(255) NOT NULL,
        address VARCHAR(255) NOT NULL,
        image_url TEXT NOT NULL
      )
    ''');
    batch.execute('''
      CREATE TABLE discount_detail (
        kd_discount VARCHAR(255) NOT NULL,
        kd_brg VARCHAR(255) NOT NULL,
        jenis_nilai INTEGER(1) NOT NULL,
        discount DOUBLE NOT NULL
      )
    ''');
    batch.execute('''
      CREATE TABLE jwk (
        id INTEGER(11) NOT NULL,
        nama_jwk VARCHAR(255) NOT NULL,
        hari INTEGER(1) NOT NULL,
        kd_jalur VARCHAR(255) NOT NULL,
        nama_jalur VARCHAR(255) NOT NULL
      )
    ''');
    batch.execute('''
      CREATE TABLE cekstore (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        tanggal DATE NOT NULL,
        kd_plg VARCHAR(255) NOT NULL,
        nama_plg VARCHAR(255) NOT NULL,
        latitude VARCHAR(255) NOT NULL,
        longitude VARCHAR(255) NOT NULL,
        address VARCHAR(255) NOT NULL
      )
    ''');
    batch.execute('''
      CREATE TABLE cekstore_detail (
        id_cekstore INTEGER(11),
        kd_brg VARCHAR(255) NOT NULL,
        nama_brg VARCHAR(255) NOT NULL,
        qty_stok INTEGER(11) NOT NULL,
        qty_retur INTEGER(11) NOT NULL,
        qty_est INTEGER(11) NOT NULL,
        harga DOUBLE NOT NULL 
      )
    ''');
    batch.execute('''
      CREATE TABLE ordersales (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        trx VARCHAR(255) NOT NULL,
        tanggal DATE NOT NULL,
        kd_plg VARCHAR(255) NOT NULL,
        nama_plg VARCHAR(255) NOT NULL,
        alamat VARCHAR(255) NOT NULL,
        latitude VARCHAR(255) NOT NULL,
        longitude VARCHAR(255) NOT NULL,
        address VARCHAR(255) NOT NULL,
        kd_discount VARCHAR(255) NOT NULL,
        total_item INTEGER(11) NOT NULL,
        total_qty INTEGER(11) NOT NULL,
        total_harga DOUBLE NOT NULL,
        catatan TEXT NOT NULL,
        status VARCHAR(255) NOT NULL,
        img_toko TEXT NOT NULL
      )
    ''');
    batch.execute('''
      CREATE TABLE ordersales_detail (
        id_ordersales INTEGER(11),
        kd_brg VARCHAR(255) NOT NULL,
        nama_brg VARCHAR(255) NOT NULL,
        qty_order INTEGER(11) NOT NULL,
        harga DOUBLE NOT NULL 
      )
    ''');
    await batch.commit();
  }

  Future<Database> get database async {
    if (_database == null) {
      _database = await initDb();
    }
    return _database;
  }

  // Future<List<BarangModel>> selectBarang() async {
  //   Database db = await this.database;
  //   String sql;
  //   sql = "SELECT * FROM barang ORDER BY kd_brg ASC";

  //   var result = await db.rawQuery(sql);
  //   if (result.length == 0) return null;

  //   List<BarangModel> list = result.map((item) {
  //     return BarangModel.fromJson(item);
  //   }).toList();

  //   debugPrint(result.toString());
  //   return list;
  // }

  Future<List<Map<String, dynamic>>> selectBarang() async {
    Database db = await this.database;
    var mapList = await db.query('barang', orderBy: 'kd_brg');
    debugPrint(mapList.toString());
    return mapList;
  }

  Future<List<Map<String, dynamic>>> selectPelanggan() async {
    Database db = await this.database;
    var mapList = await db.query('pelanggan', orderBy: 'nama');
    debugPrint(mapList.toString());
    return mapList;
  }

  Future<List<Map<String, dynamic>>> selectDiscountDetail() async {
    Database db = await this.database;
    var mapList = await db.query('discount_detail', orderBy: 'kd_brg');
    debugPrint(mapList.toString());
    return mapList;
  }

  Future<List<Map<String, dynamic>>> selectOrdersales() async {
    Database db = await this.database;
    // var mapList = await db.query('ordersales_detail', orderBy: 'kd_brg');
    var mapList = await db.query('ordersales_detail');
    debugPrint(mapList.toString());
    return mapList;
  }

  Future<List<Map<String, dynamic>>> selectCekstore() async {
    Database db = await this.database;
    // var mapList = await db.query('cekstore_detail', orderBy: 'kd_brg');
    var mapList = await db.query('cekstore_detail');
    debugPrint(mapList.toString());
    return mapList;
  }

//create databases
  Future<int> insertBarang(BarangModel object) async {
    Database db = await this.database;
    int count;
    count = await db.insert('barang', object.toMap());
    return count;
  }

  Future<int> insertPelanggan(PelangganModel object) async {
    Database db = await this.database;
    int count;
    count = await db.insert('pelanggan', object.toMap());
    return count;
  }

  Future<int> insertDiscountDetail(object) async {
    Database db = await this.database;
    int count;
    count = await db.insert('discount_detail', object.toMap());
    return count;
  }

  Future<int> insertOrdersales(OrderSalesDetailModel object) async {
    Database db = await this.database;
    int count;
    var mapList = await db.query('ordersales_detail',
        where: 'kd_brg=?', whereArgs: [object.kdbrg], limit: 1);
    // print('mapList.length'+mapList.length.toString());
    if (mapList.length > 0) {
      int countList = mapList.length;
      List<OrderSalesDetailModel> contactList = List<OrderSalesDetailModel>();
      for (int i = 0; i < countList; i++) {
        contactList.add(OrderSalesDetailModel.fromJson(mapList[i]));
      }
      int total = 0;
      for (int i = 0; i < contactList.length; i++) {
        total += contactList[i].qtyorder;
      }
      object.qtyorder = object.qtyorder + total;
      count = await db.update('ordersales_detail', object.toMap(),
          where: 'kd_brg=?', whereArgs: [object.kdbrg]);
    } else {
      count = await db.insert('ordersales_detail', object.toMap());
    }
    return count;
  }

  Future<int> insertCekstore(CsModelDetail object) async {
    Database db = await this.database;
    int count;
    var mapList = await db.query('cekstore_detail',
        where: 'kd_brg=?', whereArgs: [object.kdbrg], limit: 1);
    // print('mapList.length'+mapList.length.toString());
    if (mapList.length > 0) {
      int countList = mapList.length;
      List<CsModelDetail> contactList = List<CsModelDetail>();
      for (int i = 0; i < countList; i++) {
        contactList.add(CsModelDetail.fromJson(mapList[i]));
      }
      int stok = 0;
      int retur = 0;
      int estimasi = 0;
      for (int i = 0; i < contactList.length; i++) {
        stok += contactList[i].qtystok;
        retur += contactList[i].qtyretur;
        estimasi += contactList[i].qtyest;
      }
      object.qtystok = object.qtystok + stok;
      object.qtyretur = object.qtyretur + retur;
      object.qtyest = object.qtyest + estimasi;
      count = await db.update('cekstore_detail', object.toMap(),
          where: 'kd_brg=?', whereArgs: [object.kdbrg]);
    } else {
      count = await db.insert('cekstore_detail', object.toMap());
    }
    return count;
  }

//update databases
  Future<int> updateOrdersales(OrderSalesDetailModel object) async {
    Database db = await this.database;
    int count = await db.update('ordersales_detail', object.toMap(),
        where: 'kd_brg=?', whereArgs: [object.kdbrg]);
    return count;
  }

  Future<int> updateCekstore(CsModelDetail object) async {
    Database db = await this.database;
    int count = await db.update('cekstore_detail', object.toMap(),
        where: 'kd_brg=?', whereArgs: [object.kdbrg]);
    return count;
  }

//delete databases
  Future<int> deleteOrdersales(String id) async {
    Database db = await this.database;
    int count = await db
        .delete('ordersales_detail', where: 'kd_brg=?', whereArgs: [id]);
    return count;
  }

  Future<int> deleteCekstore(String id) async {
    Database db = await this.database;
    int count =
        await db.delete('cekstore_detail', where: 'kd_brg=?', whereArgs: [id]);
    return count;
  }

//delete all databases
  Future<int> deleteallPelanggan() async {
    Database db = await this.database;
    int count = await db.delete('pelanggan');
    return count;
  }

  Future<int> deleteallBarang() async {
    Database db = await this.database;
    int count = await db.delete('barang');
    return count;
  }

  Future<int> deleteallDiscountDetail() async {
    Database db = await this.database;
    int count = await db.delete('discount_detail');
    return count;
  }

  Future<int> deleteallJwk() async {
    Database db = await this.database;
    int count = await db.delete('jwk');
    return count;
  }

  Future<int> deleteallOrdersales() async {
    Database db = await this.database;
    int count = await db.delete('ordersales_detail');
    return count;
  }

  Future<int> deleteallCekstore() async {
    Database db = await this.database;
    int count = await db.delete('cekstore_detail');
    return count;
  }

//select
  Future<List<BarangModel>> getBarangList() async {
    var mapList = await selectBarang();
    int count = mapList.length;
    List<BarangModel> contactList = List<BarangModel>();
    for (int i = 0; i < count; i++) {
      contactList.add(BarangModel.fromJson(mapList[i]));
    }
    print('getBarangList');
    debugPrint(contactList.toString());
    return contactList;
  }

  Future<List<PelangganModel>> getPelangganList() async {
    var mapList = await selectPelanggan();
    int count = mapList.length;
    List<PelangganModel> contactList = List<PelangganModel>();
    for (int i = 0; i < count; i++) {
      contactList.add(PelangganModel.fromJson(mapList[i]));
    }
    print('getPelangganList');
    debugPrint(contactList.toString());
    return contactList;
  }

  Future<List<DiscountDetailModel>> getDiscountDetailList() async {
    var mapList = await selectDiscountDetail();
    int count = mapList.length;
    List<DiscountDetailModel> contactList = List<DiscountDetailModel>();
    for (int i = 0; i < count; i++) {
      contactList.add(DiscountDetailModel.fromJson(mapList[i]));
    }
    print('getDiscountDetailList');
    debugPrint(contactList.toString());
    return contactList;
  }

  Future<List<OrderSalesDetailModel>> getOrderSalesList() async {
    var contactMapList = await selectOrdersales();
    int count = contactMapList.length;
    List<OrderSalesDetailModel> contactList = List<OrderSalesDetailModel>();
    for (int i = 0; i < count; i++) {
      contactList.add(OrderSalesDetailModel.fromJson(contactMapList[i]));
    }
    return contactList;
  }

  Future<List<CsModelDetail>> getCekStoreList() async {
    var contactMapList = await selectCekstore();
    int count = contactMapList.length;
    List<CsModelDetail> contactList = List<CsModelDetail>();
    for (int i = 0; i < count; i++) {
      contactList.add(CsModelDetail.fromJson(contactMapList[i]));
    }
    return contactList;
  }
}
