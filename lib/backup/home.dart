// import 'package:flutter/material.dart';
// import 'package:sharon_cs/popupmenu.dart';
// import './hal_dashboard.dart' as dashboard;
// import './hal_list.dart' as listing;
// import './hal_lokasi.dart' as lokasi;

// class Home extends StatefulWidget {
//   @override
//   _Homestate createState() => new _Homestate();
// }

// List<CustomPopupMenu> choices = <CustomPopupMenu>[
//   CustomPopupMenu(title: 'Daftar User', icon: Icons.supervisor_account),
//   CustomPopupMenu(title: 'Logout', icon: Icons.exit_to_app),
// ];

// class _Homestate extends State<Home> with SingleTickerProviderStateMixin {
//   TabController controller;

//   CustomPopupMenu _selectedChoices = choices[0];
//   void _select(CustomPopupMenu choice) {
//     setState(() {
//       _selectedChoices = choice;
//     });
//   }

//   @override
//   void initState() {
//     controller = new TabController(vsync: this, length: 3);
//     super.initState();
//   }

//   @override
//   void dispose() {
//     controller.dispose();
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: new AppBar(
//         backgroundColor: Colors.brown,
//         title: new Text("Home"),
//         actions: <Widget>[
//           PopupMenuButton<CustomPopupMenu>(
//             elevation: 3.2,
//             initialValue: choices[1],
//             onCanceled: () {
//               print('You have not chossed anything');
//             },
//             tooltip: 'This is tooltip',
//             onSelected: _select,
//             itemBuilder: (BuildContext context) {
//               return choices.map((CustomPopupMenu choice) {
//                 return PopupMenuItem<CustomPopupMenu>(
//                   value: choice,
//                   child: Text(choice.title),
//                 );
//               }).toList();
//             },
//           )
//         ],
//         bottom: new TabBar(
//           controller: controller,
//           tabs: <Widget>[
//             new Tab(
//               icon: new Icon(Icons.home),
//             ),
//             new Tab(
//               icon: new Icon(Icons.format_list_bulleted),
//             ),
//             new Tab(
//               icon: new Icon(Icons.location_on),
//             ),
//           ],
//         ),
//       ),
//       body: new TabBarView(
//         controller: controller,
//         children: <Widget>[
//           dashboard.Dashboard(),
//           listing.Listing(),
//           lokasi.Lokasi(),
//         ],
//       ),
//     );
//   }
//   bodyWidget() {
//     return Container(
//       child: SelectedOption(choice: _selectedChoices),
//     );
//   }
// }

// class SelectedOption extends StatelessWidget {
//   CustomPopupMenu choice;

//   SelectedOption({Key key, this.choice}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       child: Center(
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             Icon(choice.icon, size: 140.0, color: Colors.white),
//             Text(
//               choice.title,
//               style: TextStyle(color: Colors.white, fontSize: 30),
//             )
//           ],
//         ),
//       ),
//     );
//   }
// }
