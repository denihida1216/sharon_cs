import 'package:flutter/material.dart';

class Listing extends StatefulWidget {
  @override
  _ListingState createState() => _ListingState();
}

List languages = [
  "ALFA",
  "Usep",
  "Dahlan",
  "Kamal",
  "Usup",
  "Ujang Kurnia",
  "Aseng",
  "Gunawan",
  "Septian",
  "Ayunda",
  "Nugroho",
];


class _ListingState extends State<Listing> {
  @override
Widget build(BuildContext context) {
  return Container(
    color: Colors.grey[300],
    child: ListView.builder(
      itemCount: languages.length,
      itemBuilder: (BuildContext context, int index) {
        int number = index + 1;
        String language = languages[index].toString();
        return Card(
          child: ListTile(
            leading: Text(number.toString()),
            title: Text(language),
            trailing: Icon(Icons.check),
          ),
        );
      },
    ),
  );
}
  // @override
  // Widget build(BuildContext context) {
  //   return new Container(
  //     child: new Center(
  //       child: new Column(
  //         children: <Widget>[
  //           new Padding(
  //             padding: new EdgeInsets.all(20.0),
  //           ),
  //           new Text(
  //             "Listing",
  //             style: new TextStyle(fontSize: 30.0),
  //           ),
  //           new Padding(
  //             padding: new EdgeInsets.all(20.0),
  //           ),
  //           new Icon(
  //             Icons.list,
  //             size: 90.0,
  //           )
  //         ],
  //       ),
  //     ),
  //   );
  // }
}
