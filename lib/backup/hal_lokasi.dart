import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class Lokasi extends StatefulWidget {
  @override
  _LokasiState createState() => _LokasiState();
}

class _LokasiState extends State<Lokasi> {

final Set<Marker> _markers = {};
  final LatLng _currentPosition = LatLng(-7.064166, 107.746914);

  @override
  void initState() {
    _markers.add(
      Marker(
        markerId: MarkerId("-7.064166, 107.746914"),
        position: _currentPosition,
        icon: BitmapDescriptor.defaultMarker,
        
      ),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GoogleMap(
        mapType: MapType.normal,
        initialCameraPosition: CameraPosition(
          target: _currentPosition,
          zoom: 14.0,
        ),
        markers: _markers,
      ),
    );
  }
  //   return new Container(
  //     child: new Center(
  //       child: new Column(
  //         children: <Widget>[
  //           new Padding(padding: new EdgeInsets.all(20.0),),
  //           new Text("Lokasi", style: new TextStyle(fontSize: 30.0),),
  //           new Padding(padding: new EdgeInsets.all(20.0),),
  //           new Icon(Icons.location_on, size: 90.0,)
  //         ],
  //       ),
  //     ),
  //   );
  // }
}