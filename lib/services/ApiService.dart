import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' show Client;
import 'package:sharon_cs/widget/agenda/AgendaModel.dart';
import 'package:sharon_cs/widget/barang/BarangModel.dart';
import 'package:sharon_cs/widget/cekstore/Csmodel.dart';
import 'package:sharon_cs/widget/cekstore/CsmodelDetail.dart';
import 'package:sharon_cs/widget/dashboard/DashboardModel.dart';
import 'package:sharon_cs/widget/discount/DiscountDetailModel.dart';
import 'package:sharon_cs/widget/jwk/JwkModel.dart';
import 'package:sharon_cs/widget/ordersales/OrderSalesDetailModel.dart';
import 'package:sharon_cs/widget/ordersales/OrderSalesModel.dart';
import 'package:sharon_cs/widget/pelanggan/PelangganModel.dart';
import 'package:sharon_cs/widget/users/UsersModel.dart';

class ApiService {
  //10.0.2.2 192.168.137.1 192.168.43.203 10.0.20.169 sharon.ddns.net:1989
  final String baseUrl = "http://10.0.20.169/estimasi/api";
  final String baseImageUrl = "http://10.0.20.169/estimasi/upload/image/";
  Client client = Client();

  Future<List> getbarangSuggestions(String query) async {
    await Future.delayed(Duration(seconds: 1));

    final response = await client.get("$baseUrl/barang?kdbrg=" + query);
    if (response.statusCode == 200) {
      // final data = json.decode(response.body);
      final data = barangModelFromJson(response.body);
      // print(data.length);
      return List.generate(data.length, (index) {
        BarangModel rec = data[index];
        return {
          'kd_brg': rec.kdbrg,
          'nama': rec.nama,
          'harga': rec.harga,
        };
      });
    } else {
      return null;
    }
    // return List.generate(10, (index) {
    //   return {'name': query + index.toString(), 'price': Random().nextInt(100)};
    // });
  }

  Future<List> getpelangganSuggestions(String query) async {
    await Future.delayed(Duration(seconds: 1));

    final response = await client.get("$baseUrl/pelanggan?nama=" + query);
    if (response.statusCode == 200) {
      // final data = json.decode(response.body);
      final data = pelangganModelFromJson(response.body);
      // print(data.length);
      return List.generate(data.length, (index) {
        PelangganModel rec = data[index];
        return {
          'kd_plg': rec.kdplg,
          'jp': rec.jp,
          'nama': rec.nama,
          'alamat': rec.alamat,
          'kd_discount': rec.kddiscount,
          'kd_plg_gr': rec.kdplggr,
          'nama_kd_plg_gr': rec.namakdplggr,
          'kd_jalur': rec.kdjalur,
          'nama_jalur': rec.namajalur,
          'kd_divisi': rec.kddivisi,
          'nama_divisi': rec.namadivisi,
          'image_url': rec.imageurl,
        };
      });
    } else {
      return null;
    }
    // return List.generate(10, (index) {
    //   return {'name': query + index.toString(), 'price': Random().nextInt(100)};
    // });
  }

  Future<List<UsersModel>> postLogin(UsersModel data) async {
    final response = await client.post(
      "$baseUrl/login",
      headers: {"content-type": "application/json"},
      body: usersModelToJson(data),
    );
    print(response);
    if (response.statusCode == 200) {
      return usersModelFromJson(response.body);
    } else {
      return null;
    }
  }

  // Future<List<PelangganModel>> getPelanggan(String nama) async {
  //   await Future.delayed(Duration(seconds: 1));
  //   final response = await client.get("$baseUrl/pelanggan?nama=" + nama);
  //   if (response.statusCode == 200) {
  //     return pelangganModelFromJson(response.body);
  //   } else {
  //     return null;
  //   }
  // }

  // Future<String> getBarangloadJson(String kdbrg) async {
  //   final response = await client.get("$baseUrl/barang?kdbrg=" + kdbrg);
  //   print(response.body);
  //   if (response.statusCode == 200) {
  //     return response.body;
  //   } else {
  //     return null;
  //   }
  // }

  // Future<List<BarangModel>> getBarang(String kdbrg) async {
  //   await Future.delayed(Duration(seconds: 1));
  //   final response = await client.get("$baseUrl/barang?kdbrg=" + kdbrg);
  //   if (response.statusCode == 200) {
  //     return barangModelFromJson(response.body);
  //   } else {
  //     return null;
  //   }
  // }

  Future<List<DashboardModel>> getDashboard(String id) async {
    final response = await client.get("$baseUrl/dashboard?uid=" + id);
    if (response.statusCode == 200) {
      return dashboardModelFromJson(response.body);
    } else {
      return null;
    }
  }

  Future<List<CsModel>> getCekstore(String uid) async {
    final response = await client.get("$baseUrl/cekstore?uid=" + uid);
    if (response.statusCode == 200) {
      return csModelFromJson(response.body);
    } else {
      return null;
    }
  }

  Future<List<CsModelDetail>> getCekstoreDetail(String id) async {
    final response = await client.get("$baseUrl/cekstoredetail?id=" + id);
    if (response.statusCode == 200) {
      return csModelDetailFromJson(response.body);
    } else {
      return null;
    }
  }

  Future<String> createCekstore(CsModel data) async {
    final response = await client.post(
      "$baseUrl/cekstore",
      headers: {"content-type": "application/json"},
      body: csModelToJson(data),
    );
    debugPrint(response.body);
    if (response.statusCode == 201) {
      return response.body.toString();
    } else {
      return null;
    }
  }

  Future<bool> createCekstoreDetail(CsModelDetail data) async {
    final response = await client.post(
      "$baseUrl/cekstoredetail",
      headers: {"content-type": "application/json"},
      body: csModelDetailToJson(data),
    );
    debugPrint(response.body);
    if (response.statusCode == 201) {
      return true;
    } else {
      return false;
    }
  }

  Future<String> updateCekstore(CsModel data) async {
    final response = await client.put(
      "$baseUrl/cekstore/${data.id}",
      headers: {"content-type": "application/json"},
      body: csModelToJson(data),
    );
    if (response.statusCode == 200) {
      return response.body.toString();
    } else {
      return null;
    }
  }

  Future<bool> deleteCekstoreDetail(int id) async {
    final response = await client.delete(
      "$baseUrl/cekstoredetail/$id",
      headers: {"content-type": "application/json"},
    );
    print(id);
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  Future<List<AgendaModel>> getAgenda() async {
    final response = await client.get("$baseUrl/agenda");
    if (response.statusCode == 200) {
      return agendaModelFromJson(response.body);
    } else {
      return null;
    }
  }

  Future<List<BarangModel>> getBarang(String kdbrg) async {
    final response = await client.get("$baseUrl/barang?kdbrg=" + kdbrg);
    if (response.statusCode == 200) {
      return barangModelFromJson(response.body);
    } else {
      return null;
    }
  }

  Future<List<PelangganModel>> getPelanggan(String kdjalur) async {
    final response = await client.get("$baseUrl/pelanggan?kdjalur=" + kdjalur);
    if (response.statusCode == 200) {
      return pelangganModelFromJson(response.body);
    } else {
      return null;
    }
  }

  Future<List<DiscountDetailModel>> getDiscountDetail(String uid) async {
    final response = await client.get("$baseUrl/discountdetail?uid=" + uid);
    if (response.statusCode == 200) {
      return discountDetailModelFromJson(response.body);
    } else {
      return null;
    }
  }

  Future<List<JwkModel>> getJwk(String hari) async {
    final response = await client.get("$baseUrl/jwk?hari=" + hari);
    if (response.statusCode == 200) {
      return jwkModelFromJson(response.body);
    } else {
      return null;
    }
  }

  Future<List<OrderSalesModel>> getOrderSales(
      String uid, String nama, String tg1, String tg2) async {
    final response = await client
        .get("$baseUrl/ordersales?uid=$uid&nama=$nama&tg1=$tg1&tg2=$tg2");
    if (response.statusCode == 200) {
      return orderSalesModelFromJson(response.body);
    } else {
      return null;
    }
  }

  //  Future<String> getSumOrderSales(String uid, String tg1, String tg2) async {
  //   final response = await client.get("$baseUrl/ordersalessum?uid=$uid&tg1=$tg1&tg2=$tg2");
  //   if (response.statusCode == 200) {
  //     return response.body.toString();
  //   } else {
  //     return '0';
  //   }
  // }

  Future<String> createOrderSales(OrderSalesModel data) async {
    final response = await client.post(
      "$baseUrl/ordersales",
      headers: {"content-type": "application/json"},
      body: orderSalesModelToJson(data),
    );
    debugPrint(response.body);
    if (response.statusCode == 201) {
      return response.body.toString();
    } else {
      return null;
    }
  }

  Future<bool> createOrderSalesDetail(OrderSalesDetailModel data) async {
    final response = await client.post(
      "$baseUrl/ordersalesdetail",
      headers: {"content-type": "application/json"},
      body: orderSalesDetailModelToJson(data),
    );
    debugPrint(response.body);
    if (response.statusCode == 201) {
      return true;
    } else {
      return false;
    }
  }

  Future<List<OrderSalesDetailModel>> getOrderSalesDetail(String id) async {
    final response = await client.get("$baseUrl/ordersalesdetail?id=" + id);
    if (response.statusCode == 200) {
      return orderSalesDetailModelFromJson(response.body);
    } else {
      return null;
    }
  }

  Future<String> updateOrderSales(OrderSalesModel data) async {
    final response = await client.put(
      "$baseUrl/ordersales/${data.id}",
      headers: {"content-type": "application/json"},
      body: orderSalesModelToJson(data),
    );
    if (response.statusCode == 200) {
      return response.body.toString();
    } else {
      return null;
    }
  }

  Future<bool> deleteOrderSales(int id) async {
    final response = await client.delete(
      "$baseUrl/ordersales/$id",
      headers: {"content-type": "application/json"},
    );
    print(id);
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> deleteOrderSalesDetail(int id) async {
    final response = await client.delete(
      "$baseUrl/ordersalesdetail/$id",
      headers: {"content-type": "application/json"},
    );
    print(id);
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  // Future<List<TransaksiModelDetail>> getTransaksiDetail(String id) async {
  //   final response = await client.get("$baseUrl/transaksi_detail/" + id);
  //   if (response.statusCode == 200) {
  //     return transaksiModelDetailFromJson(response.body);
  //   } else {
  //     return null;
  //   }
  // }

  // Future<bool> createTransaksi(TransaksiModel data) async {
  //   final response = await client.post(
  //     "$baseUrl/transaksi",
  //     headers: {"content-type": "application/json"},
  //     body: transaksiModelToJson(data),
  //   );
  //   if (response.statusCode == 201) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }

  // Future<bool> createTransaksiDetail(TransaksiModelDetail data) async {
  //   final response = await client.post(
  //     "$baseUrl/transaksi_detail",
  //     headers: {"content-type": "application/json"},
  //     body: transaksiModelDetailToJson(data),
  //   );
  //   if (response.statusCode == 201) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }

  // Future<bool> updateTransaksi(TransaksiModel data) async {
  //   final response = await client.put(
  //     "$baseUrl/transaksi/${data.id}",
  //     headers: {"content-type": "application/json"},
  //     body: transaksiModelToJson(data),
  //   );
  //   if (response.statusCode == 200) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }

  // Future<bool> deleteTransaksi(int id) async {
  //   final response = await client.delete(
  //     "$baseUrl/transaksi/$id",
  //     headers: {"content-type": "application/json"},
  //   );
  //   print(id);
  //   if (response.statusCode == 200) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }

  // Future<List<MenuModel>> getMenu() async {
  //   final response = await client.get("$baseUrl/menu");
  //   if (response.statusCode == 200) {
  //     return menuModelFromJson(response.body);
  //   } else {
  //     return null;
  //   }
  // }

  // Future<bool> createMenu(MenuModel data) async {
  //   final response = await client.post(
  //     "$baseUrl/menu",
  //     headers: {"content-type": "application/json"},
  //     body: menuModelToJson(data),
  //   );
  //   if (response.statusCode == 201) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }

  // Future<bool> updateMenu(MenuModel data) async {
  //   final response = await client.put(
  //     "$baseUrl/menu/${data.id}",
  //     headers: {"content-type": "application/json"},
  //     body: menuModelToJson(data),
  //   );
  //   if (response.statusCode == 200) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }

  // Future<bool> deleteMenu(int id) async {
  //   final response = await client.delete(
  //     "$baseUrl/menu/$id",
  //     headers: {"content-type": "application/json"},
  //   );
  //   if (response.statusCode == 200) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }

  // Future<List<BumbuModel>> getBumbu() async {
  //   final response = await client.get("$baseUrl/bumbu");
  //   if (response.statusCode == 200) {
  //     return bumbuModelFromJson(response.body);
  //   } else {
  //     return null;
  //   }
  // }

  // Future<bool> createBumbu(BumbuModel data) async {
  //   final response = await client.post(
  //     "$baseUrl/bumbu",
  //     headers: {"content-type": "application/json"},
  //     body: bumbuModelToJson(data),
  //   );
  //   if (response.statusCode == 201) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }

  // Future<bool> updateBumbu(BumbuModel data) async {
  //   final response = await client.put(
  //     "$baseUrl/bumbu/${data.id}",
  //     headers: {"content-type": "application/json"},
  //     body: bumbuModelToJson(data),
  //   );
  //   if (response.statusCode == 200) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }

  // Future<bool> deleteBumbu(int id) async {
  //   final response = await client.delete(
  //     "$baseUrl/bumbu/$id",
  //     headers: {"content-type": "application/json"},
  //   );
  //   if (response.statusCode == 200) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }

  // Future<List<SatuanModel>> getSatuan() async {
  //   final response = await client.get("$baseUrl/satuan");
  //   if (response.statusCode == 200) {
  //     return satuanModelFromJson(response.body);
  //   } else {
  //     return null;
  //   }
  // }

  // Future<bool> createSatuan(SatuanModel data) async {
  //   final response = await client.post(
  //     "$baseUrl/satuan",
  //     headers: {"content-type": "application/json"},
  //     body: satuanModelToJson(data),
  //   );
  //   if (response.statusCode == 201) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }

  // Future<bool> updateSatuan(SatuanModel data) async {
  //   final response = await client.put(
  //     "$baseUrl/satuan/${data.id}",
  //     headers: {"content-type": "application/json"},
  //     body: satuanModelToJson(data),
  //   );
  //   if (response.statusCode == 200) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }

  // Future<bool> deleteSatuan(int id) async {
  //   final response = await client.delete(
  //     "$baseUrl/satuan/$id",
  //     headers: {"content-type": "application/json"},
  //   );
  //   if (response.statusCode == 200) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }

  // Future<List<UsersModel>> getUsers() async {
  //   final response = await client.get("$baseUrl/users");
  //   if (response.statusCode == 200) {
  //     return usersModelFromJson(response.body);
  //   } else {
  //     return null;
  //   }
  // }

  // Future<bool> createUsers(UsersModel data) async {
  //   final response = await client.post(
  //     "$baseUrl/users",
  //     headers: {"content-type": "application/json"},
  //     body: usersModelToJson(data),
  //   );
  //   if (response.statusCode == 201) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }

  // Future<bool> updateUsers(UsersModel data) async {
  //   final response = await client.put(
  //     "$baseUrl/users/${data.id}",
  //     headers: {"content-type": "application/json"},
  //     body: usersModelToJson(data),
  //   );
  //   if (response.statusCode == 200) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }

  // Future<bool> deleteUsers(int id) async {
  //   final response = await client.delete(
  //     "$baseUrl/users/$id",
  //     headers: {"content-type": "application/json"},
  //   );
  //   if (response.statusCode == 200) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }
}
