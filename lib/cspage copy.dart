import 'dart:io';

import 'package:flutter/material.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:image_picker/image_picker.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';
import 'package:sharon_cs/services/ApiService.dart';

void main() {
  runApp(new MaterialApp(
    title: "Check Store",
    home: new CsPage(),
  ));
}

class CsPage extends StatefulWidget {
  static String tag = 'cs-page';

  // static const String routeName = "/cspage";
  @override
  _CsPageState createState() => _CsPageState();
}

class _CsPageState extends State<CsPage> {
  BuildContext context;
  ApiService apiService;
  bool _isLoading = false;
  String barcode = "";
  bool _isFieldKodeValid;
  bool _isFieldNamaValid;
  bool _isFieldAlamatValid;
  bool _isFieldKetValid;
  TextEditingController _controllerKode = TextEditingController();
  TextEditingController _controllerNama = TextEditingController();
  TextEditingController _controllerAlamat = TextEditingController();
  TextEditingController _controllerKet = TextEditingController();
  List<DropdownMenuItem> items = [];
  String selectedValue;
  File _image;

  @override
  void initState() {
    apiService = ApiService();
    // apiService.getSatuan().then((response) {
    //   if (response != null) {
    //     setState(() {
    //       dataSatuan = response;
    //     });

    //   }
    // });
    // if (widget.bumbu != null) {
    //   _isFieldKodeValid = true;
    //   _isFieldBahanValid = true;
    //   _isFieldTglBeliValid = true;
    //   _isFieldHargaBeliValid = true;
    //   _isFieldQtyValid = true;
    //   _controllerKode.text = widget.bumbu.kodebumbu;
    //   _controllerBahan.text = widget.bumbu.bahan;
    //   _controllerTglBeli.text = widget.bumbu.tglbeli;
    //   _controllerHargaBeli.text = widget.bumbu.hargabeli;
    //   _controllerQty.text = widget.bumbu.qty;
    //   _controllerIdSatuan.text = widget.bumbu.id;
    // }
    super.initState();
  }

  Future scanCode() async {
    try {
      barcode = await BarcodeScanner.scan();
      setState(() => this.barcode = barcode);
    } catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          this.barcode = 'The user did not grant the camera permission!';
        });
      } else {
        setState(() => this.barcode = 'Unknown error: $e');
      }
    }
  }

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      _image = image;
    });
  }

  @override
  Widget build(BuildContext context) {
    // final alucard = Hero(
    //   tag: 'hero',
    //   child: Padding(
    //     padding: EdgeInsets.all(16.0),
    //     child: CircleAvatar(
    //       radius: 72.0,
    //       backgroundColor: Colors.transparent,
    //       backgroundImage: AssetImage('assets/sharon.jpg'),
    //     ),
    //   ),
    // );
    final imageToko = Center(
      child: _image == null ? Text('No image selected.') : Image.file(_image),
    );
    // final welcome = Padding(
    //   padding: EdgeInsets.all(8.0),
    //   child: Text(
    //     'ALFA Raya Katerban I692',
    //     style: TextStyle(fontSize: 28.0, color: Colors.black),
    //   ),
    // );

    // final lorem = Padding(
    //   padding: EdgeInsets.all(8.0),
    //   child: Text(
    //     'Jl. Tentara Pelajar Rt. 01 Re. 07 Katerban Kec. Kutoarjo Kodepos 54214 Kabr Purworejo',
    //     style: TextStyle(fontSize: 16.0, color: Colors.black),
    //   ),
    // );

    final body = Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(28.0),
      child: ListView(
        children: <Widget>[
          imageToko,
          _buildButtonAddImage(),
          _buildDropdownKode(),
          _buildTextFieldKode(),
          _buildTextFieldNama(),
          _buildTextFieldAlamat(),
          _buildTextFieldKet(),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: RaisedButton(
              onPressed: () {
                getImage();
                // if (_isFieldNamaValid == null ||
                // _isFieldUserValid == null ||
                // !_isFieldNamaValid || !_isFieldUserValid) {
                //   setState(() => {
                //     _isFieldNamaValid =  _controllerNama.text.trim().isNotEmpty,
                //     _isFieldUserValid =  _controllerUser.text.trim().isNotEmpty
                //     });
                //   _scaffoldState.currentState.showSnackBar(
                //     SnackBar(
                //       content: Text("Data belum lengkap !!!"),
                //     ),
                //   );
                //   return;
                // }
                //   setState(() => _isLoading = true);
                //   String nama = _controllerNama.text.toString();
                //   String user = _controllerUser.text.toString();
                //   String pass = _controllerPass.text.toString();
                //   String akses = _controllerAkses.text.toString();
                //   UsersModel users = UsersModel(
                //       nama: nama, username: user, password: pass, akses: akses);
                //   if (widget.users == null) {
                //     _apiService.createUsers(users).then((isSuccess) {
                //       setState(() => _isLoading = false);
                //       if (isSuccess) {
                //         Navigator.pop(_scaffoldState.currentState.context);
                //       } else {
                //         _scaffoldState.currentState.showSnackBar(SnackBar(
                //           content: Text("Submit data gagal!!!"),
                //         ));
                //       }
                //     });
                //   } else {
                //     users.id = widget.users.id;
                //     print(users);
                //     _apiService.updateUsers(users).then((isSuccess) {
                //       print(isSuccess);

                //       setState(() => _isLoading = false);
                //       if (isSuccess) {
                //         Navigator.pop(_scaffoldState.currentState.context);
                //       } else {
                //         _scaffoldState.currentState.showSnackBar(SnackBar(
                //           content: Text("Update data gagal!!!"),
                //         ));
                //       }
                //     });
                //   }
              },
              child: Text(
                "Submit",
                // widget.users == null
                //     ? "Submit".toUpperCase()
                //     : "Update Data".toUpperCase(),
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              color: Colors.brown,
            ),
          ),
          _isLoading
              ? Stack(
                  children: <Widget>[
                    Opacity(
                      opacity: 0.3,
                      child: ModalBarrier(
                        dismissible: false,
                        color: Colors.grey,
                      ),
                    ),
                    Center(
                      child: CircularProgressIndicator(),
                    ),
                  ],
                )
              : Container(),
        ],
      ),
    );

    return Scaffold(
      appBar: AppBar(
        title: new Text("Check Store"),
        backgroundColor: Colors.brown,
      ),
      body: body,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          scanCode();
        },
        tooltip: 'Scan',
        child: Icon(Icons.cast_connected),
        backgroundColor: Colors.brown,
      ),
    );
  }

  Widget _buildTextFieldKode() {
    return TextField(
      controller: _controllerKode,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        labelText: "Kode",
        errorText: _isFieldKodeValid == null || _isFieldKodeValid
            ? null
            : "Kode harus diisi!!!",
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldKodeValid) {
          setState(() => _isFieldKodeValid = isFieldValid);
        }
      },
    );
  }

  Widget _buildButtonAddImage() {
    return Padding(
      padding: const EdgeInsets.only(top: 40.0, left: 10.0, right: 10.0),
      child: OutlineButton(
        onPressed: () {
          getImage();
        },
        borderSide: BorderSide(color: Colors.brown, width: 1.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.camera_alt),
            SizedBox(
              width: 5.0,
            ),
            Text('Add Image'),
          ],
        ),
      ),
    );
  }

  Widget _buildTextFieldNama() {
    return TextField(
      controller: _controllerNama,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        labelText: "Nama",
        errorText: _isFieldNamaValid == null || _isFieldNamaValid
            ? null
            : "Nama harus diisi!!!",
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldNamaValid) {
          setState(() => _isFieldNamaValid = isFieldValid);
        }
      },
    );
  }

  Widget _buildTextFieldAlamat() {
    return TextField(
      controller: _controllerAlamat,
      keyboardType: TextInputType.text,
      maxLines: null,
      decoration: InputDecoration(
        labelText: "Alamat",
        errorText: _isFieldAlamatValid == null || _isFieldAlamatValid
            ? null
            : "Alamat harus diisi!!!",
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldAlamatValid) {
          setState(() => _isFieldAlamatValid = isFieldValid);
        }
      },
    );
  }

  Widget _buildTextFieldKet() {
    return TextField(
      controller: _controllerKet,
      keyboardType: TextInputType.text,
      maxLines: null,
      decoration: InputDecoration(
        labelText: "Keterangan",
        errorText: _isFieldKetValid == null || _isFieldKetValid
            ? null
            : "Keterangan harus diisi!!!",
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldKetValid) {
          setState(() => _isFieldKetValid = isFieldValid);
        }
      },
    );
  }

  Widget _buildDropdownKode() {
    return SearchableDropdown(
      items: items,
      value: selectedValue,
      hint: new Text('Pilih Kode Pelanggan'),
      searchHint: new Text(
        'Pilih Kode Pelanggan',
        style: new TextStyle(fontSize: 20),
      ),
      onChanged: (value) {
        setState(() {
          selectedValue = value;
        });
      },
    );
  }
}
