import 'package:flutter/material.dart';

import 'package:sharon_cs/widget/cekstore/CsPage.dart';
import 'package:sharon_cs/widget/cekstore/CsPageDetailForm.dart';
import 'package:sharon_cs/widget/cekstore/CsPageForm.dart';
import 'package:sharon_cs/widget/home/homepage.dart';
import 'package:sharon_cs/widget/ordersales/OrderSalesDetailPageForm.dart';
import 'package:sharon_cs/widget/ordersales/OrderSalesPage.dart';
import 'package:sharon_cs/widget/ordersales/OrderSalesPageForm.dart';
import 'package:sharon_cs/widget/users/loginpage.dart';

void main() {
  runApp(new MaterialApp(
    home: new LoginPage(),
    title: "Home",
    routes: <String, WidgetBuilder>{
     HomePage.tag: (BuildContext context) => new HomePage(),
     LoginPage.tag: (BuildContext context) => new LoginPage(),
     CsPage.tag: (BuildContext context) => new CsPage(),
     CsPageForm.tag: (BuildContext context) => new CsPageForm(),
     CsPageDetailForm.tag: (BuildContext context) => new CsPageDetailForm(),
     OrderSalesPage.tag: (BuildContext context) => new OrderSalesPage(),
     OrderSalesPageForm.tag: (BuildContext context) => new OrderSalesPageForm(),
     OrderSalesDetailPageForm.tag: (BuildContext context) => new OrderSalesDetailPageForm(),
    },
  ));
}
