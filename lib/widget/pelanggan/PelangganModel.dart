import 'dart:convert';

class PelangganModel {
  String kdplg;
  String jp;
  String kdbaru;
  String nama;
  String alamat;
  String telp;
  String hp;
  String npwp;
  String kddiscount;
  String kdplggr;
  String namakdplggr;
  String kdjalur;
  String namajalur;
  String kddivisi;
  String namadivisi;
  String latitude;
  String longitude;
  String address;
  String imageurl;

  PelangganModel({
    this.kdplg,
    this.jp,
    this.kdbaru,
    this.nama,
    this.alamat,
    this.telp,
    this.hp,
    this.npwp,
    this.kddiscount,
    this.kdplggr,
    this.namakdplggr,
    this.kdjalur,
    this.namajalur,
    this.kddivisi,
    this.namadivisi,
    this.latitude,
    this.longitude,
    this.address,
    this.imageurl,
  });

  factory PelangganModel.fromJson(Map<String, dynamic> map) {
    return PelangganModel(
      kdplg: map["kd_plg"],
      jp: map["jp"],
      kdbaru: map["kd_baru"],
      nama: map["nama"],
      alamat: map["alamat"],
      telp: map["telp"],
      hp: map["hp"],
      npwp: map["npwp"],
      kddiscount: map["kd_discount"],
      kdplggr: map["kd_plg_gr"],
      namakdplggr: map["nama_kd_plg_gr"],
      kdjalur: map["kd_jalur"],
      namajalur: map["nama_jalur"],
      kddivisi: map["kd_divisi"],
      namadivisi: map["nama_divisi"],
      latitude: map["latitude"],
      longitude: map["longitude"],
      address: map["address"],
      imageurl: map["image_url"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "kd_plg": kdplg,
      "jp": jp,
      "kd_baru": kdbaru,
      "nama": nama,
      "alamat": alamat,
      "telp": telp,
      "hp": hp,
      "npwp": npwp,
      "kd_discount": kddiscount,
      "kd_plg_gr": kdplggr,
      "nama_kd_plg_gr": namakdplggr,
      "kd_jalur": kdjalur,
      "nama_jalur": namajalur,
      "kd_divisi": kddivisi,
      "nama_divisi": namadivisi,
      "latitude": latitude,
      "longitude": longitude,
      "address": address,
      "image_url": imageurl,
    };
  }

  @override
  String toString() {
    return 'PelangganModel{kd_plg: $kdplg, ' +
        'jp: $jp, ' +
        'kdbaru: $kdbaru, ' +
        'nama: $nama, ' +
        'alamat: $alamat, ' +
        'telp: $telp, ' +
        'hp: $hp, ' +
        'npwp: $npwp, ' +
        'kd_discount: $kddiscount, ' +
        'kd_plg_gr: $kdplggr, ' +
        'nama_kd_plg_gr: $namakdplggr, ' +
        'kd_jalur: $kdjalur, ' +
        'nama_jalur: $namajalur, ' +
        'kd_divisi: $kddivisi, ' +
        'nama_divisi: $namadivisi, ' +
        'latitude: $latitude, ' +
        'longitude: $longitude, ' +
        'address: $address, ' +
        'image_url: $imageurl}';
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['kd_plg'] = kdplg;
    map['jp'] = jp;
    map['kd_baru'] = kdbaru;
    map['nama'] = nama;
    map['alamat'] = alamat;
    map['telp'] = telp;
    map['hp'] = hp;
    map['npwp'] = npwp;
    map['kd_discount'] = kddiscount;
    map['kd_plg_gr'] = kdplggr;
    map['nama_kd_plg_gr'] = namakdplggr;
    map['kd_jalur'] = kdjalur;
    map['nama_jalur'] = namajalur;
    map['kd_divisi'] = kddivisi;
    map['nama_divisi'] = namadivisi;
    map['latitude'] = latitude;
    map['longitude'] = longitude;
    map['address'] = address;
    map['image_url'] = imageurl;
    return map;
  }
}

List<PelangganModel> pelangganModelFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<PelangganModel>.from(
      data.map((item) => PelangganModel.fromJson(item)));
}

String pelangganModelToJson(PelangganModel data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}
