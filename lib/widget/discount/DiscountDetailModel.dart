import 'dart:convert';

class DiscountDetailModel {
  String kddiscount;
  String kdbrg;
  int jenisnilai;
  double discount;

  DiscountDetailModel({
    this.kddiscount,
    this.kdbrg,
    this.jenisnilai,
    this.discount,
  });

  factory DiscountDetailModel.fromJson(Map<String, dynamic> map) {
    return DiscountDetailModel(
      kddiscount: map["kd_discount"],
      kdbrg: map["kd_brg"],
      jenisnilai: int.parse(map["jenis_nilai"].toString()),
      discount: double.parse(map["discount"].toString()),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "kd_discount": kddiscount,
      "kd_brg": kdbrg,
      "jenis_nilai": jenisnilai,
      "discount": discount,
    };
  }

  @override
  String toString() {
    return 'DiscountDetailModel{kd_discount: $kddiscount, ' +
        'kd_brg: $kdbrg, ' +
        'jenis_nilai: $jenisnilai, ' +
        'discount: $discount}';
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['kd_discount'] = kddiscount;
    map['kd_brg'] = kdbrg;
    map['jenis_nilai'] = jenisnilai;
    map['discount'] = discount;
    return map;
  }
}

List<DiscountDetailModel> discountDetailModelFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<DiscountDetailModel>.from(
      data.map((item) => DiscountDetailModel.fromJson(item)));
}

String discountDetailModelToJson(DiscountDetailModel data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}
