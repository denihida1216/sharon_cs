import 'dart:convert';

class DashboardModel {
  String imageurl;
  String nama;
  String tanggal;
  String kdjalur;
  String namajalur;
  String totaljadwal;
  String totaldikunjungi;
  
  DashboardModel({
    this.imageurl,
    this.nama,
    this.tanggal,
    this.kdjalur,
    this.namajalur,
    this.totaljadwal,
    this.totaldikunjungi,
  });

  factory DashboardModel.fromJson(Map<String, dynamic> map) {
    return DashboardModel(
      imageurl: map["image_url"],
      nama: map["nama"],
      tanggal: map["tanggal"],
      kdjalur: map["kd_jalur"],
      namajalur: map["nama_jalur"],
      totaljadwal: map["total_jadwal"],
      totaldikunjungi: map["total_dikunjungi"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "image_url": imageurl,
      "nama": nama,
      "tanggal": tanggal,
      "kd_jalur": kdjalur,
      "nama_jalur": namajalur,
      "total_jadwal": totaljadwal,
      "total_dikunjungi": totaldikunjungi,
    };
  }

  @override
  String toString() {
    return 'DashboardModel{image_url: $imageurl, '+
           'nama: $nama, '+
           'tanggal: $tanggal, '+
           'kd_jalur: $kdjalur, '+
           'nama_jalur: $namajalur, '+
           'total_jadwal: $totaljadwal, '+
           'total_dikunjungi: $totaldikunjungi}';
  }
}

List<DashboardModel> dashboardModelFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<DashboardModel>.from(data.map((item) => DashboardModel.fromJson(item)));
}

String dashboardModelToJson(DashboardModel data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}
