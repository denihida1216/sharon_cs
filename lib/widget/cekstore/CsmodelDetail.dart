import 'dart:convert';

class CsModelDetail {
  String idcekstore;
  String kdbrg;
  String namabrg;
  int qtystok;
  int qtyretur;
  int qtyest;
  double harga;

  CsModelDetail({
    this.idcekstore,
    this.kdbrg,
    this.namabrg,
    this.qtystok,
    this.qtyretur,
    this.qtyest,
    this.harga,
  });

  factory CsModelDetail.fromJson(Map<String, dynamic> map) {
    return CsModelDetail(
      idcekstore: map["id_cekstore"],
      kdbrg: map["kd_brg"],
      namabrg: map["nama_brg"],
      qtystok: int.parse(map["qty_stok"].toString()),
      qtyretur: int.parse(map["qty_retur"].toString()),
      qtyest: int.parse(map["qty_est"].toString()),
      harga: map["harga"] == null || map["harga"] == ''
          ? 0
          : double.parse(map["harga"].toString()),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id_cekstore": idcekstore,
      "kd_brg": kdbrg,
      "nama_brg": namabrg,
      "qty_stok": qtystok,
      "qty_retur": qtyretur,
      "qty_est": qtyest,
      "harga": harga,
    };
  }

  @override
  String toString() {
    return 'CsModelDetail{' +
        'id_cekstore: $idcekstore, ' +
        'kd_brg: $kdbrg, ' +
        'nama_brg: $namabrg, ' +
        'qty_stok: $qtystok, ' +
        'qty_retur: $qtyretur, ' +
        'qty_est: $qtyest, ' +
        'harga: $harga}';
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['id_cekstore'] = idcekstore;
    map['kd_brg'] = kdbrg;
    map['nama_brg'] = namabrg;
    map['qty_stok'] = qtystok;
    map['qty_retur'] = qtyretur;
    map['qty_est'] = qtyest;
    map['harga'] = harga;
    return map;
  }
}

List<CsModelDetail> csModelDetailFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<CsModelDetail>.from(
      data.map((item) => CsModelDetail.fromJson(item)));
}

String csModelDetailToJson(CsModelDetail data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}
