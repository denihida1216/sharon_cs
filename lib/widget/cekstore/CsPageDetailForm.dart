import 'package:flutter/material.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/services.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:sharon_cs/helper/DbHelper.dart';
import 'package:sharon_cs/services/ApiService.dart';
import 'package:sharon_cs/widget/cekstore/CsmodelDetail.dart';
import 'package:sqflite/sqflite.dart';

final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

void main() {
  runApp(new MaterialApp(
    title: "Stock Toko",
    home: new CsPageDetailForm(),
  ));
}

class CsPageDetailForm extends StatefulWidget {
  static String tag = 'CsPageDetailForm-pageform';
  final CsModelDetail csdetail;
  CsPageDetailForm({this.csdetail});

  @override
  _CsPageDetailFormState createState() => _CsPageDetailFormState();
}

class _CsPageDetailFormState extends State<CsPageDetailForm> {
  BuildContext context;
  ApiService apiService;
  List<CsModelDetail> csDetail = List();
  DbHelper dbHelper = DbHelper();
  int count = 0;
  bool _isLoading = false;
  String barcode = "";
  bool _isFieldKodeBrgValid;
  bool _isFieldNamaBrgValid;
  bool _isFieldQtyBrgValid;
  bool _isFieldQtyBrgReturValid;
  bool _isFieldQtyBrgEstValid;
  TextEditingController _controllerKodeBrg = TextEditingController();
  TextEditingController _controllerNamaBrg = TextEditingController();
  TextEditingController _controllerQtyBrg = TextEditingController();
  TextEditingController _controllerQtyBrgRetur = TextEditingController();
  TextEditingController _controllerQtyBrgEst = TextEditingController();
  String dropdownValue = 'persentase';
  String holder = '';

  List<String> jenis = [
    'persentase',
    'quantity',
  ];

  Future scanCode() async {
    try {
      barcode = await BarcodeScanner.scan();
      setState(() => this.barcode = barcode);
      // Fluttertoast.showToast(
      //     msg: "" + barcode,
      //     toastLength: Toast.LENGTH_SHORT,
      //     gravity: ToastGravity.BOTTOM,
      //     timeInSecForIos: 1,
      //     backgroundColor: Colors.white,
      //     textColor: Colors.black,
      //     fontSize: 16.0);
    } catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          this.barcode = 'The user did not grant the camera permission!';
        });
      } else {
        setState(() => this.barcode = 'Unknown error: $e');
      }
    }
  }

  @override
  void initState() {
    apiService = ApiService();
    if (widget.csdetail != null) {
      _isFieldKodeBrgValid = true;
      _isFieldNamaBrgValid = true;
      _isFieldQtyBrgValid = true;
      _isFieldQtyBrgReturValid = true;
      _isFieldQtyBrgEstValid = true;
      _controllerKodeBrg.text = widget.csdetail.kdbrg;
      _controllerNamaBrg.text = widget.csdetail.namabrg;
      _controllerQtyBrg.text = widget.csdetail.qtystok.toString();
      _controllerQtyBrgRetur.text = widget.csdetail.qtyretur.toString();
      _controllerQtyBrgEst.text = widget.csdetail.qtyest.toString();
    }
    super.initState();
  }

  @override
  void dispose() {
    _controllerKodeBrg.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;

    return Scaffold(
      key: _scaffoldState,
      appBar: AppBar(
        title: Text('Stock Toko'),
        backgroundColor: Colors.brown,
      ),
      body: Theme(
        data: ThemeData(
          primaryColor: Colors.brown,
          accentColor: Colors.brown,
          cursorColor: Colors.brown,
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: _buildContainer(),
        ),
      ),
    );
  }

  Widget _buildContainer() {
    return ListView(
      shrinkWrap: true,
      children: <Widget>[
        _buildTextFieldKodeBrg(),
        _buildTextFieldNamaBrg(),
        _buildTextFieldQtyBrg(),
        _buildTextFieldQtyBrgRetur(),
        _buildTextFieldQtyBrgEst(),
        _buildSubmit(),
        _isLoading
            ? Stack(
                children: <Widget>[
                  Opacity(
                    opacity: 0.3,
                    child: ModalBarrier(
                      dismissible: false,
                      color: Colors.grey,
                    ),
                  ),
                  Center(
                    child: CircularProgressIndicator(),
                  ),
                ],
              )
            : Container(),
      ],
    );
  }

  Widget _buildTextFieldKodeBrg() {
    return TypeAheadField(
      textFieldConfiguration: TextFieldConfiguration(
        autofocus: false,
        controller: _controllerKodeBrg,
        style: TextStyle(
          color: Colors.black54,
          fontSize: 18.0,
        ),
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          labelText: "Nama",
          errorText: _isFieldKodeBrgValid == null || _isFieldKodeBrgValid
              ? null
              : "Nama harus diisi!!!",
          labelStyle: TextStyle(color: Colors.black38),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.brown),
          ),
        ),
        onChanged: (value) {
          bool isFieldValid = value.trim().isNotEmpty;
          if (isFieldValid != _isFieldKodeBrgValid) {
            setState((){
              _isFieldKodeBrgValid = isFieldValid;
              _isFieldNamaBrgValid = isFieldValid;
              });
          }
        },
      ),
      suggestionsCallback: (pattern) async {
        return await apiService.getbarangSuggestions(pattern);
      },
      itemBuilder: (context, suggestion) {
        return ListTile(
          // leading: Icon(Icons.check_box),
          title: Text(suggestion['nama']),
          subtitle: Text(
              'Kode : ${suggestion['kd_brg']}\nNama : ${suggestion['nama']}\nKelompok : ${suggestion['kelompok']}'),
        );
      },
      onSuggestionSelected: (suggestion) {
        _controllerKodeBrg.text = suggestion['kd_brg'];
        _controllerNamaBrg.text = suggestion['nama'];
        // Navigator.of(context).push(MaterialPageRoute(
        //     builder: (context) => ProductPage(product: suggestion)));
      },
    );
  }

  Widget _buildTextFieldNamaBrg() {
    return TextField(
      controller: _controllerNamaBrg,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        labelText: "Nama Barang",
        errorText: _isFieldNamaBrgValid == null || _isFieldNamaBrgValid
            ? null
            : "Nama Barang harus diisi!!!",
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldNamaBrgValid) {
          setState(() => _isFieldNamaBrgValid = isFieldValid);
        }
      },
    );
  }

  Widget _buildTextFieldQtyBrg() {
    return TextField(
      controller: _controllerQtyBrg,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        labelText: "Qty Stock",
        errorText: _isFieldQtyBrgValid == null || _isFieldQtyBrgValid
            ? null
            : "Qty Stock harus diisi!!!",
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldQtyBrgValid) {
          setState(() => _isFieldQtyBrgValid = isFieldValid);
        }
      },
    );
  }

  Widget _buildTextFieldQtyBrgRetur() {
    return TextField(
      controller: _controllerQtyBrgRetur,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        labelText: "Qty Retur",
        errorText: _isFieldQtyBrgReturValid == null || _isFieldQtyBrgReturValid
            ? null
            : "Qty Retur harus diisi!!!",
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldQtyBrgReturValid) {
          setState(() => _isFieldQtyBrgReturValid = isFieldValid);
        }
      },
    );
  }

  Widget _buildTextFieldQtyBrgEst() {
    return TextField(
      controller: _controllerQtyBrgEst,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        labelText: "Qty Estimasi",
        errorText: _isFieldQtyBrgEstValid == null || _isFieldQtyBrgEstValid
            ? null
            : "Qty Estimasi harus diisi!!!",
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldQtyBrgEstValid) {
          setState(() => _isFieldQtyBrgEstValid = isFieldValid);
        }
      },
    );
  }

  // Widget _buildDropdownJenis() {
  //   return DropdownButton<String>(
  //     value: dropdownValue,
  //     icon: Icon(Icons.arrow_drop_down),
  //     isExpanded: true,
  //     iconSize: 24,
  //     elevation: 16,
  //     hint: Text('Pilih Jenis Estimasi'),
  //     style: TextStyle(color: Colors.black, fontSize: 18),
  //     underline: Container(
  //       height: 2,
  //       color: Colors.brown,
  //     ),
  //     onChanged: (String data) {
  //       setState(() {
  //         dropdownValue = data;
  //         _controllerJenis.text = dropdownValue;
  //       });
  //     },
  //     items: jenis.map<DropdownMenuItem<String>>((String value) {
  //       return DropdownMenuItem<String>(
  //         value: value,
  //         child: Text(value),
  //       );
  //     }).toList(),
  //   );
  // }

  Widget _buildSubmit() {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: RaisedButton(
        onPressed: () {
          print(
            _isFieldKodeBrgValid.toString()+' '+
            _isFieldNamaBrgValid.toString()+' '+
            _isFieldQtyBrgValid.toString()+' '+
            _isFieldQtyBrgEstValid.toString()+' '
          );
          if (_isFieldKodeBrgValid == null ||
              _isFieldNamaBrgValid == null ||
              _isFieldQtyBrgValid == null ||
              _isFieldQtyBrgReturValid == null||
              _isFieldQtyBrgEstValid == null) {
            setState(() => {
                  _isFieldKodeBrgValid =
                      _controllerKodeBrg.text.trim().isNotEmpty,
                  _isFieldNamaBrgValid =
                      _controllerNamaBrg.text.trim().isNotEmpty,
                  _isFieldQtyBrgValid =
                      _controllerQtyBrg.text.trim().isNotEmpty,
                  _isFieldQtyBrgReturValid =
                      _controllerQtyBrgRetur.text.trim().isNotEmpty,
                  _isFieldQtyBrgEstValid =
                      _controllerQtyBrgEst.text.trim().isNotEmpty
                });
            _scaffoldState.currentState.showSnackBar(
              SnackBar(
                content: Text("Data belum lengkap !!!"),
              ),
            );
            return;
          }
          setState(() => _isLoading = true);
          CsModelDetail data = CsModelDetail(
            kdbrg: _controllerKodeBrg.text.toString(),
            namabrg: _controllerNamaBrg.text.toString(),
            qtystok: _controllerQtyBrg.text.toString().trim().isNotEmpty
                ? int.parse(_controllerQtyBrg.text.toString())
                : 0,
            qtyretur: _controllerQtyBrgRetur.text.toString().trim().isNotEmpty
                ? int.parse(_controllerQtyBrgRetur.text.toString())
                : 0,
            qtyest: _controllerQtyBrgEst.text.toString().trim().isNotEmpty
                ? int.parse(_controllerQtyBrgEst.text.toString())
                : 0,
            harga: 0,
          );
          print(data);
          if (widget.csdetail == null) {
            addOrder(data);
            setState(() => _isLoading = false);
            Navigator.pop(_scaffoldState.currentState.context);
          } else {
            data.kdbrg = widget.csdetail.kdbrg;
            editOrder(data);
            setState(() => _isLoading = false);
            Navigator.pop(_scaffoldState.currentState.context);
          }
        },
        child: Text(
          widget.csdetail == null
              ? "Submit".toUpperCase()
              : "Update Data".toUpperCase(),
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        color: Colors.brown,
      ),
    );
  }

  //buat order
  void addOrder(CsModelDetail object) async {
    int result = await dbHelper.insertCekstore(object);
    if (result > 0) {
      updateListView();
    }
  }

  //edit order
  void editOrder(CsModelDetail object) async {
    int result = await dbHelper.updateCekstore(object);
    if (result > 0) {
      updateListView();
    }
  }

  //delete order
  void deleteOrder(CsModelDetail object) async {
    int result = await dbHelper.deleteCekstore(object.kdbrg);
    if (result > 0) {
      updateListView();
    }
  }

  //update contact
  void updateListView() {
    final Future<Database> dbFuture = dbHelper.initDb();
    dbFuture.then((database) {
      // Future<List<OrderSalesDetailModel>> contactListFuture =
      //     dbHelper.getOrderSalesList();
      // contactListFuture.then((contactList) {
      //   setState(() {
      //     this.orderDetail = contactList;
      //     this.count = contactList.length;
      //   });
      // });
    });
  }
}
