import 'dart:convert';

class CsModel {
  String id;
  String tanggal;
  String cluster;
  String kdplg1;
  String kdplg2;
  String nama;
  String alamat;
  String kdjalur;
  String namajalur;
  String imagetoko;
  String imagepajangan;
  String catatan;
  String latitude;
  String longitude;
  String address;
  String uid;
  String user;
  String timestamp;

  CsModel({
    this.id,
    this.tanggal,
    this.cluster,
    this.kdplg1,
    this.kdplg2,
    this.nama,
    this.alamat,
    this.kdjalur,
    this.namajalur,
    this.imagetoko,
    this.imagepajangan,
    this.catatan,
    this.latitude,
    this.longitude,
    this.address,
    this.uid,
    this.user,
    this.timestamp,
  });

  factory CsModel.fromJson(Map<String, dynamic> map) {
    return CsModel(
      id: map["id"],
      tanggal: map["tanggal"],
      cluster: map["cluster"],
      kdplg1: map["kd_plg"],
      kdplg2: map["jp"],
      nama: map["nama"],
      alamat: map["alamat"],
      kdjalur: map["kd_jalur"],
      namajalur: map["nama_jalur"],
      imagetoko: map["image_toko"],
      imagepajangan: map["image_pajangan"],
      catatan: map["catatan"],
      latitude: map["latitude"],
      longitude: map["longitude"],
      address: map["address"],
      uid: map["uid"],
      user: map["user"],
      timestamp: map["timestamp"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "tanggal": tanggal,
      "cluster": cluster,
      "kd_plg": kdplg1,
      "jp": kdplg2,
      "nama": nama,
      "alamat": alamat,
      "kd_jalur": kdjalur,
      "nama_jalur": namajalur,
      "image_toko": imagetoko,
      "image_pajangan": imagepajangan,
      "catatan": catatan,
      "latitude": latitude,
      "longitude": longitude,
      "address": address,
      "uid": uid,
      "user": user,
      "timestamp": timestamp,
    };
  }

  @override
  String toString() {
    return 'CsModel{id: $id, ' +
        'tanggal: $tanggal, ' +
        'cluster: $cluster, ' +
        'kd_plg: $kdplg1, ' +
        'jp: $kdplg2, ' +
        'nama: $nama, ' +
        'alamat: $alamat, ' +
        'kd_jalur: $kdjalur, ' +
        'nama_jalur: $namajalur, ' +
        'image_toko: $imagetoko, ' +
        'image_pajangan: $imagepajangan, ' +
        'catatan: $catatan, ' +
        'latitude: $latitude, ' +
        'longitude: $longitude, ' +
        'address: $address, ' +
        'uid: $uid, ' +
        'user: $user, ' +
        'timestamp: $timestamp}';
  }
}

List<CsModel> csModelFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<CsModel>.from(data.map((item) => CsModel.fromJson(item)));
}

String csModelToJson(CsModel data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}
