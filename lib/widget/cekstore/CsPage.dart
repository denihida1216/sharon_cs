import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sharon_cs/services/ApiService.dart';
import 'package:sharon_cs/widget/cekstore/CsPageForm.dart';
import 'package:sharon_cs/widget/cekstore/Csmodel.dart';

void main() {
  runApp(new MaterialApp(
    title: "Check Store List",
    home: new CsPage(),
  ));
}

class CsPage extends StatefulWidget {
  static String tag = 'CsPage-page';

  @override
  _CsPageState createState() => _CsPageState();
}

class _CsPageState extends State<CsPage> {
  BuildContext context;
  ApiService apiService;
  Widget appBarTitle = new Text(
    "Kunjungan Hari Ini",
    style: new TextStyle(color: Colors.white),
  );
  Icon actionIcon = new Icon(
    Icons.search,
    color: Colors.white,
  );
  final key = new GlobalKey<ScaffoldState>();
  final TextEditingController _searchQuery = new TextEditingController();
  bool _IsSearching;
  String _searchText = "";

  _SearchListState() {
    _searchQuery.addListener(() {
      if (_searchQuery.text.isEmpty) {
        setState(() {
          _IsSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          _IsSearching = true;
          _searchText = _searchQuery.text;
        });
      }
    });
  }

  @override
  void initState() {
    super.initState();
    apiService = ApiService();
    getPref();
    _IsSearching = false;
  }

  var title;
  var uid;
  var user;
  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      title = preferences.getString("title");
      uid = preferences.getString("id");
      user = preferences.getString("user");
    });
  }

  void _handleSearchStart() {
    setState(() {
      _IsSearching = true;
    });
  }

  void _handleSearchEnd() {
    setState(() {
      this.actionIcon = new Icon(
        Icons.search,
        color: Colors.white,
      );
      this.appBarTitle = new Text(
        "Kunjungan Hari Ini",
        style: new TextStyle(color: Colors.white),
      );
      _IsSearching = false;
      _searchQuery.clear();
    });
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    return Scaffold(
      appBar: AppBar(
        title: appBarTitle,
        backgroundColor: Colors.brown,
        automaticallyImplyLeading: false,
        leading: Container(),
        actions: <Widget>[
          IconButton(
            icon: actionIcon,
            onPressed: () {
              setState(() {
                if (this.actionIcon.icon == Icons.search) {
                  this.actionIcon = Icon(
                    Icons.close,
                    color: Colors.white,
                  );
                  this.appBarTitle = TextField(
                    controller: _searchQuery,
                    style: TextStyle(
                      color: Colors.white,
                    ),
                    decoration: InputDecoration(
                        // prefixIcon: new Icon(Icons.search, color: Colors.white),
                        hintText: "Cari...",
                        hintStyle: TextStyle(color: Colors.white)),
                  );
                  _handleSearchStart();
                } else {
                  _handleSearchEnd();
                }
              });
            },
          ),
          // IconButton(
          //     icon: Icon(Icons.search),
          //     onPressed: () {
          //       // signOut();
          //     }),
          // IconButton(
          //     icon: Icon(Icons.filter_list),
          //     onPressed: () {
          //       // signOut();
          //     }),
        ],
      ),
      body: FutureBuilder(
        future: apiService.getCekstore(uid),
        builder: (BuildContext context, AsyncSnapshot<List<CsModel>> snapshot) {
          if (snapshot.hasError) {
            return Center(
              child: Text(
                  "Something wrong with message: ${snapshot.error.toString()}"),
            );
          } else if (snapshot.connectionState == ConnectionState.done) {
            List<CsModel> data = snapshot.data;
            return _buildListView(data);
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }

  Widget _buildListView(List<CsModel> list) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
      child: ListView.builder(
        itemBuilder: (context, index) {
          CsModel rec = list[index];
          return Card(
            child: ListTile(
              onTap: () {
                // print('bismilah' + rec.id);
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return CsPageForm(csModel: rec);
                }));
              },
              leading: CircleAvatar(
                backgroundColor: Colors.transparent,
                backgroundImage: rec.imagetoko == ''
                    ? AssetImage('assets/sharon.jpg')
                    : NetworkImage(apiService.baseImageUrl + rec.imagetoko),
              ),
              title: Row(
                children: <Widget>[
                  Expanded(
                      child: Text(
                    rec.nama,
                  )),
                ],
              ),
              subtitle: Row(
                children: <Widget>[
                  Expanded(
                    child: Text(
                      'Kode Internal: ' +
                          rec.kdplg1 +
                          '\nJalur: ' +
                          rec.kdjalur +
                          ' - ' +
                          rec.namajalur +
                          '\nAlamat: ' +
                          rec.alamat,
                    ),
                  ),
                ],
              ),
            ),
          );
        },
        itemCount: list.length,
      ),
    );
  }
}
