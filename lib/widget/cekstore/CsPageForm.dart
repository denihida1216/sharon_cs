import 'dart:io';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/services.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:location_permissions/location_permissions.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sharon_cs/helper/DbHelper.dart';
import 'package:sharon_cs/services/ApiService.dart';
import 'package:sharon_cs/widget/cekstore/CsPageDetailForm.dart';
import 'package:sharon_cs/widget/cekstore/Csmodel.dart';
import 'package:sharon_cs/widget/cekstore/CsmodelDetail.dart';
import 'package:sqflite/sqflite.dart';

void main() {
  runApp(new MaterialApp(
    title: "Check Store",
    home: new CsPageForm(),
  ));
}

final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

class CsPageForm extends StatefulWidget {
  static String tag = 'cs-pageform';
  final CsModel csModel;
  CsPageForm({this.csModel});

  // static const String routeName = "/CsPageForm";
  @override
  _CsPageFormState createState() => _CsPageFormState();
}

class _CsPageFormState extends State<CsPageForm> {
  int _currentStep = 0;
  BuildContext context;
  ApiService apiService;
  bool _isLoading = false;
  String barcode = "";
  String kdbrg = "";
  bool _isFieldKode1Valid;
  bool _isFieldKode2Valid;
  bool _isFieldNamaValid;
  bool _isFieldAlamatValid;
  bool _isFieldNoteValid;
  TextEditingController _controllerID = TextEditingController();
  TextEditingController _controllerKode1 = TextEditingController();
  TextEditingController _controllerKode2 = TextEditingController();
  TextEditingController _controllerNama = TextEditingController();
  TextEditingController _controllerCluster = TextEditingController();
  TextEditingController _controllerAlamat = TextEditingController();
  TextEditingController _controllerKdjalur = TextEditingController();
  TextEditingController _controllerNamajalur = TextEditingController();
  TextEditingController _controllerNote = TextEditingController();
  TextEditingController _controllerLat = TextEditingController();
  TextEditingController _controllerLong = TextEditingController();
  TextEditingController _controllerAddress = TextEditingController();
  String selectedValue1;
  String selectedValue2;
  File _image1;
  File _image2;
  String base64Image1 = '';
  String base64Image2 = '';
  PermissionStatus _permissionStatus = PermissionStatus.unknown;
  TextEditingController controller = new TextEditingController();
  String filter;
  List<CsModelDetail> csdetail = List();
  DbHelper dbHelper = DbHelper();
  int count = 0;

  _getLocation() async {
    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    print('location: ${position.latitude}');
    final coordinates = new Coordinates(position.latitude, position.longitude);
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;
    print(
        "${first.postalCode} : ${first.adminArea} : ${first.countryName} : ${first.countryCode} : ${first.featureName} : ${first.addressLine}");
    _controllerLat.text = position.latitude.toString();
    _controllerLong.text = position.longitude.toString();
    _controllerAddress.text = first.addressLine;
  }

  var uid;
  var user;
  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      uid = preferences.getString("id");
      user = preferences.getString("user");
    });
  }

  Future scanCode() async {
    try {
      barcode = await BarcodeScanner.scan();
      setState(() => this.barcode = barcode);
      Fluttertoast.showToast(
          msg: "" + barcode,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: Colors.white,
          textColor: Colors.black,
          fontSize: 16.0);
    } catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          this.barcode = 'The user did not grant the camera permission!';
        });
      } else {
        setState(() => this.barcode = 'Unknown error: $e');
      }
    }
  }

  Future getImage1() async {
    final file1 = await ImagePicker.pickImage(source: ImageSource.camera);

    if (file1 != null) {
      Directory appDocDir = await getApplicationDocumentsDirectory();
      appDocDir.delete();
      String appDocPath = appDocDir.path + 'IMG-TEMP-' + DateTime.now().toString() + '.jpg';
      var result = await FlutterImageCompress.compressAndGetFile(
        file1.absolute.path,
        appDocPath,
        quality: 80,
      );
      // print(file1.lengthSync());
      // print(result.lengthSync());
      setState(() {
        _image1 = result;
        base64Image1 = base64Encode(result.readAsBytesSync());
      });
      // print(base64Image1);
    }
  }

  Future getImage2() async {
    final file2 = await ImagePicker.pickImage(
        source: ImageSource.camera, imageQuality: 85);
    if (file2 != null) {
      Directory appDocDir = await getApplicationDocumentsDirectory();
      appDocDir.delete();
      String appDocPath = appDocDir.path + 'IMG-TEMP-' + DateTime.now().toString() + '.jpg';
      var result = await FlutterImageCompress.compressAndGetFile(
        file2.absolute.path,
        appDocPath,
        quality: 80,
      );
      // print(file2.lengthSync());
      // print(result.lengthSync());
      setState(() {
        _image2 = result;
        base64Image2 = base64Encode(result.readAsBytesSync());
      });
      // print(base64Image2);
    }
  }

  @override
  void initState() {
    apiService = ApiService();
    getPref();
    if (widget.csModel != null) {
      // _isFieldKodeValid = true;
      // _isFieldBahanValid = true;
      // _isFieldTglBeliValid = true;
      // _isFieldHargaBeliValid = true;
      // _isFieldQtyValid = true;
      _controllerID.text = widget.csModel.id;
      _controllerNama.text = widget.csModel.nama;
      _controllerKode1.text = widget.csModel.kdplg1;
      _controllerKode2.text = widget.csModel.kdplg2;
      _controllerCluster.text = widget.csModel.cluster;
      _controllerAlamat.text = widget.csModel.alamat;
      _controllerKdjalur.text = widget.csModel.kdjalur;
      _controllerNamajalur.text = widget.csModel.namajalur;
      _controllerLat.text = widget.csModel.latitude;
      _controllerLong.text = widget.csModel.longitude;
      _controllerAddress.text = widget.csModel.address;
      _controllerNote.text = widget.csModel.catatan;
      dbHelper.deleteallCekstore();
      print(widget.csModel.id);
      apiService.getCekstoreDetail(widget.csModel.id).then((order) {
        print(order);
        int count = order.length;
        for (int i = 0; i < count; i++) {
          print(order[i].kdbrg);
          CsModelDetail data = CsModelDetail(
            kdbrg: order[i].kdbrg,
            namabrg: order[i].namabrg,
            qtystok: order[i].qtystok.toInt(),
            qtyretur: order[i].qtyretur.toInt(),
            qtyest: order[i].qtyest.toInt(),
            harga: 0,
          );
          dbHelper.insertCekstore(data);
        }
      });
    } else {
      dbHelper.deleteallCekstore();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;

    return Scaffold(
      key: _scaffoldState,
      appBar: AppBar(
        title: Text('Check store'),
        backgroundColor: Colors.brown,
      ),
      body: Theme(
        data: ThemeData(
          primaryColor: Colors.brown,
          accentColor: Colors.brown,
          cursorColor: Colors.brown,
        ),
        child: _typeStep(),
      ),
    );
  }

  Widget _typeStep() {
    return Stepper(
        type: StepperType.horizontal,
        currentStep: _currentStep,
        onStepTapped: (int step) => setState(() => _currentStep = step),
        onStepContinue:
            _currentStep < 2 ? () => setState(() => _currentStep += 1) : null,
        onStepCancel:
            _currentStep > 0 ? () => setState(() => _currentStep -= 1) : null,
        steps: [
          Step(
            title: Text('Informasi\nToko'),
            content: _buildConten1(),
            isActive: _currentStep >= 0,
            state: _currentStep >= 0 ? StepState.complete : StepState.disabled,
          ),
          Step(
            title: Text('Pajangan\nToko'),
            content: _buildConten2(),
            isActive: _currentStep >= 0,
            state: _currentStep >= 1 ? StepState.complete : StepState.disabled,
          ),
          Step(
            title: Text('Stock\nToko'),
            content: _buildConten3(),
            isActive: _currentStep >= 0,
            state: _currentStep >= 2 ? StepState.complete : StepState.disabled,
          ),
        ],
        controlsBuilder: (BuildContext context,
            {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
          return Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              _currentStep >= 1
                  ? RaisedButton(
                      onPressed: () {
                        onStepCancel();
                      },
                      child: const Text('Kembali'),
                    )
                  : Container(),
              _currentStep >= 2
                  ? RaisedButton(
                      onPressed: () {
                        print(_currentStep);
                        _simpanDb();
                        Navigator.pop(_scaffoldState.currentState.context);
                      },
                      color: Colors.brown,
                      textColor: Colors.white,
                      child: const Text('Simpan'),
                    )
                  : RaisedButton(
                      onPressed: () {
                        print(_currentStep);
                        onStepContinue();
                      },
                      color: Colors.brown,
                      textColor: Colors.white,
                      child: const Text('Lanjutkan'),
                    ),
            ],
          );
        });
  }

  _simpanDb() {
    CsModel data = CsModel(
      id: _controllerID.text.toString(),
      kdplg1: _controllerKode1.text.toString(),
      kdplg2: _controllerKode2.text.toString(),
      nama: _controllerNama.text.toString(),
      cluster: _controllerCluster.text.toString(),
      alamat: _controllerAlamat.text.toString(),
      kdjalur: _controllerKdjalur.text.toString(),
      namajalur: _controllerNamajalur.text.toString(),
      latitude: _controllerLat.text.toString(),
      longitude: _controllerLong.text.toString(),
      address: _controllerAddress.text.toString(),
      catatan: _controllerNote.text.toString(),
      imagetoko: base64Image1,
      imagepajangan: base64Image2,
      uid: uid,
      user: user,
    );
    // print(data);
    if (widget.csModel == null) {
      apiService.createCekstore(data).then((idcekstore) {
        // print(cekstore);
        final Future<Database> dbFuture = dbHelper.initDb();
        dbFuture.then((database) {
          Future<List<CsModelDetail>> contactListFuture =
              dbHelper.getCekStoreList();
          contactListFuture.then((contactList) {
            int count = contactList.length;
            for (int i = 0; i < count; i++) {
              print(idcekstore);
              CsModelDetail data = CsModelDetail(
                idcekstore: idcekstore,
                kdbrg: contactList[i].kdbrg,
                namabrg: contactList[i].namabrg,
                qtystok: contactList[i].qtystok,
                qtyretur: contactList[i].qtyretur,
                qtyest: contactList[i].qtyest,
                harga: contactList[i].harga,
              );
              apiService.createCekstoreDetail(data);
            }
          });
        });
      });
    } else {
      print('updateCekstore');
      print(data);
      apiService.updateCekstore(data).then((idcekstore) {
        print('updateCekstore=>' + idcekstore.replaceAll('"', ''));

        apiService.deleteCekstoreDetail(idcekstore != null
            ? int.parse(idcekstore.replaceAll('"', '').toString())
            : 0);
        final Future<Database> dbFuture = dbHelper.initDb();
        dbFuture.then((database) {
          Future<List<CsModelDetail>> contactListFuture =
              dbHelper.getCekStoreList();
          contactListFuture.then((contactList) {
            int count = contactList.length;
            for (int i = 0; i < count; i++) {
              print('idcekstore=>' + idcekstore.replaceAll('"', ''));
              CsModelDetail data = CsModelDetail(
                idcekstore: idcekstore.replaceAll('"', ''),
                kdbrg: contactList[i].kdbrg,
                namabrg: contactList[i].namabrg,
                qtystok: contactList[i].qtystok,
                qtyretur: contactList[i].qtyretur,
                qtyest: contactList[i].qtyest,
                harga: contactList[i].harga,
              );
              apiService.createCekstoreDetail(data);
            }
          });
        });
      });
    }
  }

  Widget _buildConten1() {
    return ListView(
      physics: const ClampingScrollPhysics(),
      shrinkWrap: true,
      children: <Widget>[
        _buildTextFieldNama(),
        _buildTextFieldKode1(),
        _buildTextFieldKode2(),
        _buildTextFieldCluster(),
        _buildTextFieldAlamat(),
        _buildTextFieldKdJalur(),
        _buildTextFieldNamaJalur(),
        SizedBox(height: 16.0),
        Container(
          height: 200.0,
          child: Center(
            child: widget.csModel != null && widget.csModel.imagetoko != ''
                ? Image.network(
                    apiService.baseImageUrl + widget.csModel.imagetoko)
                : _image1 == null
                    ? Text('No image selected.')
                    : Image.file(_image1),

            //  _image1 == null
            //     ? Text('No image selected.')
            //     : widget.csModel != null ||
            //             widget.csModel.imagetoko != null ||
            //             widget.csModel.imagetoko != ''
            //         ? _image1 == null
            //             ? Image.network(apiService.baseImageUrl+widget.csModel.imagetoko)
            //             : Image.file(_image1)
            //         : Image.file(_image1),
          ),
        ),
        _buildButtonAddImage1(),
        SizedBox(height: 16.0),
      ],
    );
  }

  Widget _buildConten2() {
    return ListView(
      physics: const ClampingScrollPhysics(),
      shrinkWrap: true,
      children: <Widget>[
        Container(
          height: 200.0,
          child: Center(
            child: widget.csModel != null && widget.csModel.imagepajangan != ''
                ? Image.network(
                    apiService.baseImageUrl + widget.csModel.imagepajangan)
                : _image2 == null
                    ? Text('No image selected.')
                    : Image.file(_image2),
          ),
        ),
        _buildButtonAddImage2(),
        _buildTextFieldLat(),
        _buildTextFieldLong(),
        _buildTextFieldAddress(),
        SizedBox(height: 16.0),
      ],
    );
  }

  Widget _buildConten3() {
    return ListView(
      physics: const ClampingScrollPhysics(),
      shrinkWrap: true,
      children: <Widget>[
        _buildMenuStockInput(),
        FutureBuilder(
          future: dbHelper.getCekStoreList(),
          builder: (context, snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text(
                    "Something wrong with message: ${snapshot.error.toString()}"),
              );
            } else if (snapshot.connectionState == ConnectionState.done) {
              List<CsModelDetail> data = snapshot.data;
              return _buildListView(data);
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ),
        _buildTextFieldNote(),
        SizedBox(height: 16.0),
        _isLoading
            ? Stack(
                children: <Widget>[
                  Opacity(
                    opacity: 0.3,
                    child: ModalBarrier(
                      dismissible: false,
                      color: Colors.grey,
                    ),
                  ),
                  Center(
                    child: CircularProgressIndicator(),
                  ),
                ],
              )
            : Container(),
      ],
    );
  }

//update contact
  void updateListView() {
    final Future<Database> dbFuture = dbHelper.initDb();
    dbFuture.then((database) {
      Future<List<CsModelDetail>> contactListFuture =
          dbHelper.getCekStoreList();
      contactListFuture.then((contactList) {
        setState(() {
          this.csdetail = contactList;
          this.count = contactList.length;
        });
      });
    });
  }

  Widget _buildListView(List<CsModelDetail> list) {
    return Container(
      height: 300,
      child: ListView.builder(
        shrinkWrap: true,
        itemBuilder: (context, index) {
          CsModelDetail rec = list[index];
          return Card(
            child: ListTile(
              onTap: () {
                // print('bismilah' + rec.id);
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return CsPageDetailForm(csdetail: rec);
                }));
              },
              leading: CircleAvatar(
                backgroundColor: Colors.brown,
                child: Icon(
                  Icons.swap_horiz,
                  color: Colors.white,
                ),
              ),
              title: Text(
                rec.kdbrg,
              ),
              subtitle: Text(
                '' +
                    rec.namabrg +
                    '\nQty Stok: ' +
                    rec.qtystok.toString() +
                    ' Pcs\nQty Retur: ' +
                    rec.qtyretur.toString() +
                    ' Pcs\nQty Estimasi: ' +
                    rec.qtyest.toString() +
                    ' Pcs',
              ),
              trailing: IconButton(
                  icon: Icon(
                    Icons.delete,
                  ),
                  onPressed: () {
                    dbHelper.deleteCekstore(rec.kdbrg).then((count) {
                      if (count > 0) {
                        updateListView();
                      }
                    });
                  }),
            ),
          );
        },
        itemCount: list.length,
      ),
    );
  }

  Widget _buildTextFieldNama() {
    return TypeAheadField(
      textFieldConfiguration: TextFieldConfiguration(
        autofocus: false,
        controller: _controllerNama,
        style: TextStyle(
          color: Colors.black54,
          fontSize: 18.0,
        ),
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          labelText: "Nama",
          errorText: _isFieldNamaValid == null || _isFieldNamaValid
              ? null
              : "Nama harus diisi!!!",
          labelStyle: TextStyle(color: Colors.black38),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.brown),
          ),
        ),
        onChanged: (value) {
          bool isFieldValid = value.trim().isNotEmpty;
          if (isFieldValid != _isFieldNamaValid) {
            setState(() => _isFieldNamaValid = isFieldValid);
          }
        },
      ),
      suggestionsCallback: (pattern) async {
        return await apiService.getpelangganSuggestions(pattern);
      },
      itemBuilder: (context, suggestion) {
        return ListTile(
          // leading: Icon(Icons.check_box),
          title: Text(suggestion['nama']),
          subtitle: Text(
              'Kode internal : ${suggestion['kd_plg']}\nKode external : ${suggestion['jp']}\nJalur : ${suggestion['kd_jalur']} - ${suggestion['nama_jalur']}\nAlamat : ${suggestion['alamat']}'),
        );
      },
      onSuggestionSelected: (suggestion) {
        _controllerNama.text = suggestion['nama'];
        _controllerKode1.text = suggestion['kd_plg'];
        _controllerKode2.text = suggestion['jp'];
        _controllerCluster.text = suggestion['cluster'];
        _controllerAlamat.text = suggestion['alamat'];
        _controllerKdjalur.text = suggestion['kd_jalur'];
        _controllerNamajalur.text = suggestion['nama_jalur'];
        // Navigator.of(context).push(MaterialPageRoute(
        //     builder: (context) => ProductPage(product: suggestion)));
      },
    );
  }

  Widget _buildTextFieldKode1() {
    return TextField(
      controller: _controllerKode1,
      keyboardType: TextInputType.text,
      readOnly: true,
      decoration: InputDecoration(
        labelText: "Kode Internal",
        errorText: _isFieldKode1Valid == null || _isFieldKode1Valid
            ? null
            : "Kode Internal harus diisi!!!",
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldKode1Valid) {
          setState(() => _isFieldKode1Valid = isFieldValid);
        }
      },
    );
  }

  Widget _buildTextFieldKode2() {
    return TextField(
      controller: _controllerKode2,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        labelText: "Kode External",
        errorText: _isFieldKode2Valid == null || _isFieldKode2Valid
            ? null
            : "Kode External harus diisi!!!",
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldKode2Valid) {
          setState(() => _isFieldKode2Valid = isFieldValid);
        }
      },
    );
  }

  Widget _buildTextFieldCluster() {
    return TextField(
      controller: _controllerCluster,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        labelText: "Klasifikasi",
        errorText: _isFieldKode2Valid == null || _isFieldKode2Valid
            ? null
            : "Klasifikasi harus diisi!!!",
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldKode2Valid) {
          setState(() => _isFieldKode2Valid = isFieldValid);
        }
      },
    );
  }

  Widget _buildTextFieldAlamat() {
    return TextField(
      controller: _controllerAlamat,
      keyboardType: TextInputType.text,
      maxLines: null,
      decoration: InputDecoration(
        labelText: "Alamat",
        errorText: _isFieldAlamatValid == null || _isFieldAlamatValid
            ? null
            : "Alamat harus diisi!!!",
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldAlamatValid) {
          setState(() => _isFieldAlamatValid = isFieldValid);
        }
      },
    );
  }

  Widget _buildTextFieldKdJalur() {
    return TextField(
      controller: _controllerKdjalur,
      keyboardType: TextInputType.text,
      readOnly: true,
      decoration: InputDecoration(
        labelText: "Kode Jalur",
        errorText: _isFieldKode2Valid == null || _isFieldKode2Valid
            ? null
            : "Kode Jalur harus diisi!!!",
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldKode2Valid) {
          setState(() => _isFieldKode2Valid = isFieldValid);
        }
      },
    );
  }

  Widget _buildTextFieldNamaJalur() {
    return TextField(
      controller: _controllerNamajalur,
      keyboardType: TextInputType.text,
      readOnly: true,
      decoration: InputDecoration(
        labelText: "Nama Jalur",
        errorText: _isFieldKode2Valid == null || _isFieldKode2Valid
            ? null
            : "Nama Jalur harus diisi!!!",
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldKode2Valid) {
          setState(() => _isFieldKode2Valid = isFieldValid);
        }
      },
    );
  }

  Widget _buildButtonAddImage1() {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
      child: OutlineButton(
        onPressed: () {
          getImage1();
        },
        borderSide: BorderSide(color: Colors.brown, width: 1.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.camera_alt),
            SizedBox(
              width: 5.0,
            ),
            Text('Tambah photo depan toko'),
          ],
        ),
      ),
    );
  }

  Widget _buildTextFieldNote() {
    return TextField(
      controller: _controllerNote,
      keyboardType: TextInputType.text,
      maxLines: null,
      decoration: InputDecoration(
        labelText: "Catatan",
        errorText: _isFieldNoteValid == null || _isFieldNoteValid
            ? null
            : "Catatan harus diisi!!!",
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldNoteValid) {
          setState(() => _isFieldNoteValid = isFieldValid);
        }
      },
    );
  }

  Widget _buildTextFieldLat() {
    return TextField(
      controller: _controllerLat,
      keyboardType: TextInputType.text,
      readOnly: true,
      decoration: InputDecoration(
        labelText: "Latitude",
      ),
    );
  }

  Widget _buildTextFieldLong() {
    return TextField(
      controller: _controllerLong,
      keyboardType: TextInputType.text,
      readOnly: true,
      decoration: InputDecoration(
        labelText: "Longitude",
      ),
    );
  }

  Widget _buildTextFieldAddress() {
    return TextField(
      controller: _controllerAddress,
      keyboardType: TextInputType.text,
      maxLines: null,
      readOnly: true,
      decoration: InputDecoration(
        labelText: "Address Line",
      ),
    );
  }

  Widget _buildButtonAddImage2() {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
      child: OutlineButton(
        onPressed: () {
          getImage2();
          _getLocation();
        },
        borderSide: BorderSide(color: Colors.brown, width: 1.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.camera_alt),
            SizedBox(
              width: 5.0,
            ),
            Text('Tambah photo pajangan'),
          ],
        ),
      ),
    );
  }

  Widget _buildMenuStockInput() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        FlatButton(
            child: Column(
              children: <Widget>[
                Icon(
                  Icons.add_to_queue,
                  color: Colors.green,
                ),
                Text("Tambah Item")
              ],
            ),
            onPressed: () {
              Navigator.pushNamed(context, CsPageDetailForm.tag);
              // showDialog(
              //     context: context,
              //     builder: (context) {
              //       return SimpleDialog(
              //         children: <Widget>[
              //           new Container(
              //             height: 100.0,
              //             width: 100.0,
              //             child: new ListView(
              //               children: <Widget>[
              //                 SearchableDropdown(
              //                   items: items,
              //                   value: selectedValue2,
              //                   hint: new Text('Pilih Barang'),
              //                   isExpanded: true,
              //                   searchHint: new Text(
              //                     'Barang',
              //                     style: new TextStyle(fontSize: 20),
              //                   ),
              //                   onChanged: (value) {
              //                     setState(() {
              //                       selectedValue2 = value;
              //                     });
              //                   },
              //                 ),
              //                 Expanded(
              //                   child: TextField(
              //                     keyboardType: TextInputType.number,
              //                     decoration: new InputDecoration(
              //                         labelText: 'Qty Stock',
              //                         hintText: 'Masukan qty stock di toko'),
              //                   ),
              //                 ),
              //                 Expanded(
              //                   child: TextField(
              //                     keyboardType: TextInputType.number,
              //                     decoration: new InputDecoration(
              //                         labelText: 'Qty Estimasi',
              //                         hintText: 'Masukan qty estimasi'),
              //                   ),
              //                 )
              //               ],
              //             ),
              //           )
              //         ],
              //       );
              //     });
            }),
        FlatButton(
            child: Column(
              children: <Widget>[
                Icon(
                  Icons.settings_remote,
                  color: Colors.blue,
                ),
                Text("Tambah Scan")
              ],
            ),
            onPressed: () {
              scanCode();
            }),
            FlatButton(
            child: Column(
              children: <Widget>[
                Icon(
                  Icons.refresh,
                  color: Colors.orange,
                ),
                Text("Refresh")
              ],
            ),
            onPressed: () {
              updateListView();
            }),
      ],
    );
  }

  // Widget _buildDropdownKodeBrg() {
  //   return SearchableDropdown(
  //     items: items,
  //     value: selectedValue2,
  //     isExpanded: true,
  //     hint: new Text('Pilih Barang'),
  //     searchHint: new Text(
  //       'Barang',
  //       style: new TextStyle(fontSize: 20),
  //     ),
  //     onChanged: (value) {
  //       setState(() {
  //         selectedValue2 = value;
  //       });
  //     },
  //   );
  // }

  // Widget _buildSubmit() {
  //   return Padding(
  //     padding: const EdgeInsets.only(top: 8.0),
  //     child: RaisedButton(
  //       onPressed: () {
  //         Navigator.pop(context);
  //         // if (_isFieldNamaValid == null ||
  //         // _isFieldUserValid == null ||
  //         // !_isFieldNamaValid || !_isFieldUserValid) {
  //         //   setState(() => {
  //         //     _isFieldNamaValid =  _controllerNama.text.trim().isNotEmpty,
  //         //     _isFieldUserValid =  _controllerUser.text.trim().isNotEmpty
  //         //     });
  //         //   _scaffoldState.currentState.showSnackBar(
  //         //     SnackBar(
  //         //       content: Text("Data belum lengkap !!!"),
  //         //     ),
  //         //   );
  //         //   return;
  //         // }
  //         //   setState(() => _isLoading = true);
  //         //   String nama = _controllerNama.text.toString();
  //         //   String user = _controllerUser.text.toString();
  //         //   String pass = _controllerPass.text.toString();
  //         //   String akses = _controllerAkses.text.toString();
  //         //   UsersModel users = UsersModel(
  //         //       nama: nama, username: user, password: pass, akses: akses);
  //         //   if (widget.users == null) {
  //         //     _apiService.createUsers(users).then((isSuccess) {
  //         //       setState(() => _isLoading = false);
  //         //       if (isSuccess) {
  //         //         Navigator.pop(_scaffoldState.currentState.context);
  //         //       } else {
  //         //         _scaffoldState.currentState.showSnackBar(SnackBar(
  //         //           content: Text("Submit data gagal!!!"),
  //         //         ));
  //         //       }
  //         //     });
  //         //   } else {
  //         //     users.id = widget.users.id;
  //         //     print(users);
  //         //     _apiService.updateUsers(users).then((isSuccess) {
  //         //       print(isSuccess);

  //         //       setState(() => _isLoading = false);
  //         //       if (isSuccess) {
  //         //         Navigator.pop(_scaffoldState.currentState.context);
  //         //       } else {
  //         //         _scaffoldState.currentState.showSnackBar(SnackBar(
  //         //           content: Text("Update data gagal!!!"),
  //         //         ));
  //         //       }
  //         //     });
  //         //   }
  //       },
  //       child: Text(
  //         "Submit".toUpperCase(),
  //         // widget.users == null
  //         //     ? "Submit".toUpperCase()
  //         //     : "Update Data".toUpperCase(),
  //         style: TextStyle(
  //           color: Colors.white,
  //         ),
  //       ),
  //       color: Colors.brown,
  //     ),
  //   );
  // }
}
