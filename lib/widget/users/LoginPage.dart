import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sharon_cs/services/ApiService.dart';
import 'package:sharon_cs/widget/home/homepage.dart';
import 'package:sharon_cs/widget/users/UsersModel.dart';

class LoginPage extends StatefulWidget {
  static String tag = 'login-page';
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  BuildContext context;
  ApiService apiService;
  bool _isLoading = false;
  String email, password;
  // final _key = new GlobalKey<FormState>();
  List<UsersModel> dataUser = List();
  bool _secureText = true;
  bool _isFieldUserValid;
  bool _isFieldPassValid;
  TextEditingController _controllerUser = TextEditingController();
  TextEditingController _controllerPass = TextEditingController();
  PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );

  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
    });
  }

  showHide() {
    setState(() {
      _secureText = !_secureText;
    });
  }

  login(String user, String pass) {
    setState(() => _isLoading = false);
    UsersModel users = UsersModel(username: user, password: pass);
    print(users);
    apiService.postLogin(users).then((response) {
      if (response != null) {
        setState(() {
          dataUser = response;
        });
        final data = dataUser[0];
        print(data);
        savePref(1, data.kdsales, data.namasales, user, data.nama, data.akses,
            data.id);
        getPref();
        setState(() => _isLoading = true);
      } else {
        setState(() => _isLoading = true);
        Fluttertoast.showToast(
            msg: "Username / Password Salah!!!",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.white,
            textColor: Colors.black,
            fontSize: 16.0);
      }
    });
  }

  savePref(int value, String kdsales, String namasales, String user,
      String nama, String akses, String id) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      preferences.setInt("value", value);
      preferences.setString("kd_sales", kdsales);
      preferences.setString("nama_sales", namasales);
      preferences.setString("nama", nama);
      preferences.setString("user", user);
      preferences.setString("akses", akses);
      preferences.setString("id", id);
      // preferences.commit();
    });
  }

  var value;
  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      value = preferences.getInt("value");
      if (value == 1) {
        Navigator.of(context).pushNamedAndRemoveUntil(
            HomePage.tag, (Route<dynamic> route) => false);
      }
    });
  }

  @override
  void initState() {
    super.initState();
    apiService = ApiService();
    getPref();
    _initPackageInfo();
    // _isFieldUserValid = false;
    // _isFieldPassValid = false;
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 72.0,
        child: Image.asset('assets/sharon.jpg'),
      ),
    );

    final welcome = Padding(
      padding: EdgeInsets.all(8.0),
      child: Center(
        child: Text(
          'Sharon Apps',
          style: TextStyle(
            fontSize: 28.0,
            color: Colors.black,
          ),
        ),
      ),
    );

    final username = TextFormField(
      controller: _controllerUser,
      keyboardType: TextInputType.text,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Username',
        errorText: _isFieldUserValid == null || _isFieldUserValid
            ? null
            : "Username harus diisi!!!",
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldUserValid) {
          setState(() => _isFieldUserValid = isFieldValid);
        }
      },
    );

    final password = TextFormField(
      controller: _controllerPass,
      autofocus: false,
      obscureText: true,
      decoration: InputDecoration(
        hintText: 'Password',
        errorText: _isFieldPassValid == null || _isFieldPassValid
            ? null
            : "Password harus diisi!!!",
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldPassValid) {
          setState(() => _isFieldPassValid = isFieldValid);
        }
      },
    );

    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(30.0),
            side: BorderSide(color: Colors.brown)),
        onPressed: () {
          if (_isFieldUserValid == null || !_isFieldUserValid) {
            setState(() => {_isFieldUserValid = false});
            return;
          } else if (_isFieldPassValid == null || !_isFieldPassValid) {
            setState(() => {_isFieldPassValid = false});
            return;
          }
          login(
              _controllerUser.text.toString(), _controllerPass.text.toString());
        },
        color: Colors.brown,
        textColor: Colors.white,
        child: Text("Login".toUpperCase(),
            style: TextStyle(
              fontSize: 18,
            )),
      ),
    );

    // final forgotLabel = FlatButton(
    //   child: Text(
    //     'Forgot password?',
    //     style: TextStyle(color: Colors.black54),
    //   ),
    //   onPressed: () {
    //     Fluttertoast.showToast(
    //         msg: "Belum tersedia!!!",
    //         toastLength: Toast.LENGTH_SHORT,
    //         gravity: ToastGravity.BOTTOM,
    //         timeInSecForIos: 1,
    //         backgroundColor: Colors.white,
    //         textColor: Colors.black,
    //         fontSize: 16.0);
    //   },
    // );

    final versi = Padding(
      padding: EdgeInsets.all(8.0),
      child: Center(
        child: Text(
          'Ver ' + _packageInfo.version,
          style: TextStyle(
            fontSize: 16.0,
            color: Colors.black,
          ),
        ),
      ),
    );
    return Scaffold(
      backgroundColor: Colors.white,
      body: Theme(
        data: ThemeData(
          primaryColor: Colors.brown,
          accentColor: Colors.brown,
          cursorColor: Colors.brown,
        ),
        child: Center(
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              logo,
              welcome,
              SizedBox(height: 8.0),
              username,
              SizedBox(height: 8.0),
              password,
              SizedBox(height: 8.0),
              loginButton,
              // forgotLabel,
              versi,
              _isLoading
                  ? Stack(
                      children: <Widget>[
                        Opacity(
                          opacity: 0.3,
                          child: ModalBarrier(
                            dismissible: false,
                            color: Colors.grey,
                          ),
                        ),
                        Center(
                          child: CircularProgressIndicator(),
                        ),
                      ],
                    )
                  : Container(),
            ],
          ),
        ),
      ),
    );
  }
}
