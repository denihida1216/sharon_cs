import 'dart:convert';

class UsersModel {
  String id;
  String imageurl;
  String kdsales;
  String namasales;
  String nama;
  String username;
  String password;
  String akses;
  String token;
  String status;
  String lastlogin;

  UsersModel({
    this.id,
    this.imageurl,
    this.kdsales,
    this.namasales,
    this.nama,
    this.username,
    this.password,
    this.akses,
    this.token,
    this.status,
    this.lastlogin,
  });

  factory UsersModel.fromJson(Map<String, dynamic> map) {
    return UsersModel(
      id: map["id"],
      imageurl: map["image_url"],
      kdsales: map["kd_sales"],
      namasales: map["nama_sales"],
      nama: map["nama"],
      username: map["username"],
      password: map["password"],
      akses: map["akses"],
      token: map["token"],
      status: map["status"],
      lastlogin: map["lastlogin"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "image_url": imageurl,
      "kd_sales": kdsales,
      "nama_sales": namasales,
      "nama": nama,
      "username": username,
      "password": password,
      "akses": akses,
      "token": token,
      "status": status,
      "lastlogin": lastlogin,
    };
  }

  @override
  String toString() {
    return 'UsersModel{id: $id, image_url: $imageurl, kd_sales: $kdsales, nama_sales: $namasales, nama: $nama, username: $username, password: $password, akses: $akses, token: $token, lastlogin: $lastlogin, status: $status}';
  }
}

List<UsersModel> usersModelFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<UsersModel>.from(data.map((item) => UsersModel.fromJson(item)));
}

String usersModelToJson(UsersModel data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}
