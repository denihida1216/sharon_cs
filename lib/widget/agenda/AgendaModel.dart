import 'dart:convert';

class AgendaModel {
  String id;
  String tanggal;
  String kdjalur;
  String namajalur;
  String uid;
  String nama;
  String uidubah;
  String namaubah;

  AgendaModel({
    this.id,
    this.tanggal,
    this.kdjalur,
    this.namajalur,
    this.uid,
    this.nama,
    this.uidubah,
    this.namaubah,
  });

  factory AgendaModel.fromJson(Map<String, dynamic> map) {
    return AgendaModel(
      id: map["id"],
      tanggal: map["tanggal"],
      kdjalur: map["kd_jalur"],
      namajalur: map["nama_jalur"],
      uid: map["uid"],
      nama: map["nama"],
      uidubah: map["uid_ubah"],
      namaubah: map["nama_ubah"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "tanggal": tanggal,
      "kd_jalur": kdjalur,
      "nama_jalur": namajalur,
      "uid": uid,
      "nama": nama,
      "uid_ubah": uidubah,
      "nama_ubah": namaubah,
    };
  }

  @override
  String toString() {
    return 'AgendaModel{' +
        'id: $id, ' +
        'tanggal: $tanggal, ' +
        'kd_jalur: $kdjalur, ' +
        'nama_jalur: $namajalur, ' +
        'uid: $uid, ' +
        'nama: $nama, ' +
        'uid_ubah: $uidubah, ' +
        'nama_ubah: $namaubah, ' +
        '}';
  }
}

List<AgendaModel> agendaModelFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<AgendaModel>.from(data.map((item) => AgendaModel.fromJson(item)));
}

String agendaModelToJson(AgendaModel data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}
