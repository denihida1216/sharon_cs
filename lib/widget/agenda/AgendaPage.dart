import 'package:flutter/material.dart';
import 'package:sharon_cs/services/ApiService.dart';
import 'package:sharon_cs/widget/cekstore/Csmodel.dart';

void main() {
  runApp(new MaterialApp(
    title: "Agenda List",
    home: new AgendaPage(),
  ));
}

class AgendaPage extends StatefulWidget {
  static String tag = 'AgendaPage-page';

  @override
  _AgendaPageState createState() => _AgendaPageState();
}

class _AgendaPageState extends State<AgendaPage> {
  BuildContext context;
  ApiService apiService;

  @override
  void initState() {
    super.initState();
    apiService = ApiService();
    // getPref();
  }

  // var title;
  // getPref() async {
  //   SharedPreferences preferences = await SharedPreferences.getInstance();
  //   setState(() {
  //     title = preferences.getString("title");
  //   });
  // }
  
  @override
  Widget build(BuildContext context) {
    this.context = context;
    return Scaffold(
      appBar: AppBar(
        title: new Text("Agenda"),
        backgroundColor: Colors.brown,
      ),
      body: FutureBuilder(
        future: apiService.getCekstore(''),
        builder: (BuildContext context, AsyncSnapshot<List<CsModel>> snapshot) {
          if (snapshot.hasError) {
            return Center(
              child: Text(
                  "Something wrong with message: ${snapshot.error.toString()}"),
            );
          } else if (snapshot.connectionState == ConnectionState.done) {
            List<CsModel> data = snapshot.data;
            return _buildListView(data);
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }

  Widget _buildListView(List<CsModel> list) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
      child: ListView.builder(
        itemBuilder: (context, index) {
          CsModel rec = list[index];
          return Card(
            child: ListTile(
              onTap: null,
              leading: CircleAvatar(
                backgroundColor: Colors.transparent,
                child: Image.network(rec.imagetoko),
                // child: Image.network(rec.gambar),
              ),
              title: Row(
                children: <Widget>[
                  Expanded(
                      child: Text(
                    rec.nama,
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w700,
                    ),
                  )),
                ],
              ),
              subtitle: Row(
                children: <Widget>[
                  Expanded(
                      child: Text(
                    rec.kdjalur +' - '+rec.namajalur + '\n' + rec.alamat,
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w700,
                    ),
                  )),
                ],
              ),
              // trailing: IconButton(
              //     icon: Icon(
              //       Icons.exit_to_app,
              //       size: 40,
              //       ),
              //     onPressed: () {
              //       // signOut();
              //     }),
            ),
          );
        },
        itemCount: list.length,
      ),
    );
  }
}
