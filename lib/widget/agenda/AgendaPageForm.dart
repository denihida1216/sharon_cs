import 'dart:io';

import 'package:flutter/material.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';
import 'package:sharon_cs/services/ApiService.dart';

void main() {
  runApp(new MaterialApp(
    title: "Check Store",
    home: new AgendaPageForm(),
  ));
}

class AgendaPageForm extends StatefulWidget {
  static String tag = 'cs-pageform';
  // CsModel csModel;
  // AgendaPageForm({this.csModel});
  
  // static const String routeName = "/AgendaPageForm";
  @override
  _AgendaPageFormState createState() => _AgendaPageFormState();
}

class _AgendaPageFormState extends State<AgendaPageForm> {
  int _currentStep = 0;
  BuildContext context;
  ApiService apiService;
  bool _isLoading = false;
  String barcode = "";
  bool _isFieldKodeValid;
  bool _isFieldNamaValid;
  bool _isFieldAlamatValid;
  bool _isFieldNoteValid;
  TextEditingController _controllerKode = TextEditingController();
  TextEditingController _controllerNama = TextEditingController();
  TextEditingController _controllerAlamat = TextEditingController();
  TextEditingController _controllerNote = TextEditingController();
  TextEditingController _controllerLat = TextEditingController();
  TextEditingController _controllerLong = TextEditingController();
  List<DropdownMenuItem> items = [];
  String selectedValue1;
  String selectedValue2;
  File _image1;
  File _image2;

  Future scanCode() async {
    try {
      barcode = await BarcodeScanner.scan();
      setState(() => this.barcode = barcode);
      Fluttertoast.showToast(
          msg: "" + barcode,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: Colors.white,
          textColor: Colors.black,
          fontSize: 16.0);
    } catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          this.barcode = 'The user did not grant the camera permission!';
        });
      } else {
        setState(() => this.barcode = 'Unknown error: $e');
      }
    }
  }

  Future getImage1() async {
    var image1 = await ImagePicker.pickImage(source: ImageSource.camera);

    if (image1 != null) {
      setState(() {
        _image1 = image1;
      });
    }
  }

  Future getImage2() async {
    var image2 = await ImagePicker.pickImage(source: ImageSource.camera);

    if (image2 != null) {
      setState(() {
        _image2 = image2;
      });
    }
  }

  @override
  void initState() {
    apiService = ApiService();
    // apiService.getSatuan().then((response) {
    //   if (response != null) {
    //     setState(() {
    //       dataSatuan = response;
    //     });

    //   }
    // });
    // if (widget.bumbu != null) {
    //   _isFieldKodeValid = true;
    //   _isFieldBahanValid = true;
    //   _isFieldTglBeliValid = true;
    //   _isFieldHargaBeliValid = true;
    //   _isFieldQtyValid = true;
    //   _controllerKode.text = widget.bumbu.kodebumbu;
    //   _controllerBahan.text = widget.bumbu.bahan;
    //   _controllerTglBeli.text = widget.bumbu.tglbeli;
    //   _controllerHargaBeli.text = widget.bumbu.hargabeli;
    //   _controllerQty.text = widget.bumbu.qty;
    //   _controllerIdSatuan.text = widget.bumbu.id;
    // }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;

    return Scaffold(
      appBar: AppBar(
        title: Text('Check store'),
        backgroundColor: Colors.brown,
      ),
      body: Theme(
        data: ThemeData(
          primaryColor: Colors.brown,
          accentColor: Colors.brown,
        ),
        child: _typeStep(),
      ),
    );
  }

  Widget _typeStep() {
    return Stepper(
        type: StepperType.horizontal,
        currentStep: _currentStep,
        onStepTapped: (int step) => setState(() => _currentStep = step),
        onStepContinue:
            _currentStep < 2 ? () => setState(() => _currentStep += 1) : null,
        onStepCancel:
            _currentStep > 0 ? () => setState(() => _currentStep -= 1) : null,
        steps: [
          Step(
            title: Text('Informasi\nToko'),
            content: _buildConten1(),
            isActive: _currentStep >= 0,
            state: _currentStep >= 0 ? StepState.complete : StepState.disabled,
          ),
          Step(
            title: Text('Gambar\nSelfing'),
            content: _buildConten2(),
            isActive: _currentStep >= 0,
            state: _currentStep >= 1 ? StepState.complete : StepState.disabled,
          ),
          Step(
            title: Text('Stock\nToko'),
            content: _buildConten3(),
            isActive: _currentStep >= 0,
            state: _currentStep >= 2 ? StepState.complete : StepState.disabled,
          ),
        ],
        controlsBuilder: (BuildContext context,
            {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
          return Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              _currentStep >= 1
                  ? RaisedButton(
                      onPressed: () {
                        onStepCancel();
                      },
                      child: const Text('Kembali'),
                    )
                  : Container(),
              _currentStep >= 2
                  ? RaisedButton(
                      onPressed: () {
                        onStepContinue();
                      },
                      color: Colors.brown,
                      textColor: Colors.white,
                      child: const Text('Simpan'),
                    )
                  : RaisedButton(
                      onPressed: () {
                        onStepContinue();
                      },
                      color: Colors.brown,
                      textColor: Colors.white,
                      child: const Text('Lanjutkan'),
                    ),
            ],
          );
        });
  }

  Widget _buildConten1() {
    return ListView(
      physics: const ClampingScrollPhysics(),
      shrinkWrap: true,
      children: <Widget>[
        _buildDropdownKode(),
        _buildTextFieldKode(),
        _buildTextFieldNama(),
        _buildTextFieldAlamat(),
        SizedBox(height: 16.0),
        Container(
          height: 200.0,
          child: Center(
            child: _image1 == null
                ? Text('No image selected.')
                : Image.file(_image1),
          ),
        ),
        _buildButtonAddImage1(),
        SizedBox(height: 16.0),
      ],
    );
  }

  Widget _buildConten2() {
    return ListView(
      physics: const ClampingScrollPhysics(),
      shrinkWrap: true,
      children: <Widget>[
        Container(
          height: 200.0,
          child: Center(
            child: _image2 == null
                ? Text('No image selected.')
                : Image.file(_image2),
          ),
        ),
        _buildButtonAddImage2(),
        _buildTextFieldNote(),
        _buildTextFieldLat(),
        _buildTextFieldLong(),
        SizedBox(height: 16.0),
      ],
    );
  }

  Widget _buildConten3() {
    return ListView(
      physics: const ClampingScrollPhysics(),
      shrinkWrap: true,
      children: <Widget>[
        _buildMenuStockInput(),
        ListView(
          physics: const ClampingScrollPhysics(),
          shrinkWrap: true,
          children: <Widget>[
            Center(
              child: Text('\n\n\nstock toko belum diinput!!!\n\n\n'),
            ),
          ],
        ),
        SizedBox(height: 16.0),
        _isLoading
            ? Stack(
                children: <Widget>[
                  Opacity(
                    opacity: 0.3,
                    child: ModalBarrier(
                      dismissible: false,
                      color: Colors.grey,
                    ),
                  ),
                  Center(
                    child: CircularProgressIndicator(),
                  ),
                ],
              )
            : Container(),
      ],
    );
  }

  Widget _buildTextFieldKode() {
    return TextField(
      controller: _controllerKode,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        labelText: "Kode",
        errorText: _isFieldKodeValid == null || _isFieldKodeValid
            ? null
            : "Kode harus diisi!!!",
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldKodeValid) {
          setState(() => _isFieldKodeValid = isFieldValid);
        }
      },
    );
  }

  Widget _buildButtonAddImage1() {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
      child: OutlineButton(
        onPressed: () {
          getImage1();
        },
        borderSide: BorderSide(color: Colors.brown, width: 1.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.camera_alt),
            SizedBox(
              width: 5.0,
            ),
            Text('Add Image'),
          ],
        ),
      ),
    );
  }

  Widget _buildTextFieldNama() {
    return TextField(
      controller: _controllerNama,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        labelText: "Nama",
        errorText: _isFieldNamaValid == null || _isFieldNamaValid
            ? null
            : "Nama harus diisi!!!",
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldNamaValid) {
          setState(() => _isFieldNamaValid = isFieldValid);
        }
      },
    );
  }

  Widget _buildTextFieldAlamat() {
    return TextField(
      controller: _controllerAlamat,
      keyboardType: TextInputType.text,
      maxLines: null,
      decoration: InputDecoration(
        labelText: "Alamat",
        errorText: _isFieldAlamatValid == null || _isFieldAlamatValid
            ? null
            : "Alamat harus diisi!!!",
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldAlamatValid) {
          setState(() => _isFieldAlamatValid = isFieldValid);
        }
      },
    );
  }

  Widget _buildButtonAddImage2() {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
      child: OutlineButton(
        onPressed: () {
          getImage2();
        },
        borderSide: BorderSide(color: Colors.brown, width: 1.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.camera_alt),
            SizedBox(
              width: 5.0,
            ),
            Text('Add Image'),
          ],
        ),
      ),
    );
  }

  Widget _buildTextFieldNote() {
    return TextField(
      controller: _controllerNote,
      keyboardType: TextInputType.text,
      maxLines: null,
      decoration: InputDecoration(
        labelText: "Catatan",
        errorText: _isFieldNoteValid == null || _isFieldNoteValid
            ? null
            : "Catatan harus diisi!!!",
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldNoteValid) {
          setState(() => _isFieldNoteValid = isFieldValid);
        }
      },
    );
  }

  Widget _buildTextFieldLat() {
    return TextField(
      controller: _controllerLat,
      keyboardType: TextInputType.text,
      readOnly: true,
      decoration: InputDecoration(
        labelText: "Latitude",
      ),
    );
  }

  Widget _buildTextFieldLong() {
    return TextField(
      controller: _controllerLong,
      keyboardType: TextInputType.text,
      readOnly: true,
      decoration: InputDecoration(
        labelText: "Longitude",
      ),
    );
  }

  Widget _buildDropdownKode() {
    return SearchableDropdown(
      items: items,
      value: selectedValue1,
      isExpanded: true,
      hint: new Text('Pilih Pelanggan'),
      searchHint: new Text(
        'Pelanggan',
        style: new TextStyle(fontSize: 20),
      ),
      onChanged: (value) {
        setState(() {
          selectedValue1 = value;
        });
      },
    );
  }

  Widget _buildMenuStockInput() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        FlatButton(
            child: Column(
              children: <Widget>[
                Icon(
                  Icons.add_to_queue,
                  color: Colors.green,
                ),
                Text("Add Manual")
              ],
            ),
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (context) {
                    return SimpleDialog(
                      children: <Widget>[
                        new Container(
                          height: 100.0,
                          width: 100.0,
                          child: new ListView(
                            children: <Widget>[
                              SearchableDropdown(
                                items: items,
                                value: selectedValue2,
                                hint: new Text('Pilih Barang'),
                                isExpanded: true,
                                searchHint: new Text(
                                  'Barang',
                                  style: new TextStyle(fontSize: 20),
                                ),
                                onChanged: (value) {
                                  setState(() {
                                    selectedValue2 = value;
                                  });
                                },
                              ),
                              Expanded(
                                child: TextField(
                                  keyboardType: TextInputType.number,
                                  decoration: new InputDecoration(
                                      labelText: 'Qty Stock',
                                      hintText: 'Masukan qty stock di toko'),
                                ),
                              ),
                              Expanded(
                                child: TextField(
                                  keyboardType: TextInputType.number,
                                  decoration: new InputDecoration(
                                      labelText: 'Qty Estimasi',
                                      hintText: 'Masukan qty estimasi'),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    );
                    // AlertDialog(
                    //   title: Text("Barang"),
                    //   // title: Text("Meja ${rec.meja}"),
                    //   content: Column(
                    //     children: <Widget>[
                    //       Expanded(
                    //         child: SearchableDropdown(
                    //           items: items,
                    //           value: selectedValue2,
                    //           hint: new Text('Pilih Barang'),
                    //           isExpanded: true,
                    //           searchHint: new Text(
                    //             'Barang',
                    //             style: new TextStyle(fontSize: 20),
                    //           ),
                    //           onChanged: (value) {
                    //             setState(() {
                    //               selectedValue2 = value;
                    //             });
                    //           },
                    //         ),
                    //       ),
                    //       Expanded(
                    //         child: TextField(
                    //           keyboardType: TextInputType.number,
                    //           decoration: new InputDecoration(
                    //               labelText: 'Qty Stock',
                    //               hintText: 'Masukan qty stock di toko'),
                    //         ),
                    //       ),
                    //       Expanded(
                    //         child: TextField(
                    //           keyboardType: TextInputType.number,
                    //           decoration: new InputDecoration(
                    //               labelText: 'Qty Estimasi',
                    //               hintText: 'Masukan qty estimasi'),
                    //         ),
                    //       )
                    //     ],
                    //   ),
                    //   actions: <Widget>[
                    //     FlatButton(
                    //       child: Text("Simpan"),
                    //       onPressed: () {
                    //         Navigator.pop(context);

                    //         // String status = "simpan";
                    //         // TransaksiModel transaksi =
                    //         //     TransaksiModel(
                    //         //         id: rec.id, status: status);
                    //         // apiService
                    //         //     .updateTransaksi(transaksi)
                    //         //     .then((isSuccess) {
                    //         //   if (isSuccess) {
                    //         //     setState(() {});
                    //         //     Scaffold.of(this.context)
                    //         //         .showSnackBar(SnackBar(
                    //         //             content:
                    //         //                 Text("Berhasil...")));
                    //         //   } else {
                    //         //     Scaffold.of(this.context)
                    //         //         .showSnackBar(SnackBar(
                    //         //             content:
                    //         //                 Text("Gagal!!!")));
                    //         //   }
                    //         // });
                    //       },
                    //     ),
                    //     FlatButton(
                    //       child: Text("Kembali"),
                    //       onPressed: () {
                    //         Navigator.pop(context);
                    //       },
                    //     )
                    //   ],
                    // );
                  });
            }),
        FlatButton(
            child: Column(
              children: <Widget>[
                Icon(
                  Icons.settings_remote,
                  color: Colors.blue,
                ),
                Text("Add Scan")
              ],
            ),
            onPressed: () {
              scanCode();
            }),
      ],
    );
  }

  // Widget _buildDropdownKodeBrg() {
  //   return SearchableDropdown(
  //     items: items,
  //     value: selectedValue2,
  //     isExpanded: true,
  //     hint: new Text('Pilih Barang'),
  //     searchHint: new Text(
  //       'Barang',
  //       style: new TextStyle(fontSize: 20),
  //     ),
  //     onChanged: (value) {
  //       setState(() {
  //         selectedValue2 = value;
  //       });
  //     },
  //   );
  // }

  // Widget _buildSubmit() {
  //   return Padding(
  //     padding: const EdgeInsets.only(top: 8.0),
  //     child: RaisedButton(
  //       onPressed: () {
  //         Navigator.pop(context);
  //         // if (_isFieldNamaValid == null ||
  //         // _isFieldUserValid == null ||
  //         // !_isFieldNamaValid || !_isFieldUserValid) {
  //         //   setState(() => {
  //         //     _isFieldNamaValid =  _controllerNama.text.trim().isNotEmpty,
  //         //     _isFieldUserValid =  _controllerUser.text.trim().isNotEmpty
  //         //     });
  //         //   _scaffoldState.currentState.showSnackBar(
  //         //     SnackBar(
  //         //       content: Text("Data belum lengkap !!!"),
  //         //     ),
  //         //   );
  //         //   return;
  //         // }
  //         //   setState(() => _isLoading = true);
  //         //   String nama = _controllerNama.text.toString();
  //         //   String user = _controllerUser.text.toString();
  //         //   String pass = _controllerPass.text.toString();
  //         //   String akses = _controllerAkses.text.toString();
  //         //   UsersModel users = UsersModel(
  //         //       nama: nama, username: user, password: pass, akses: akses);
  //         //   if (widget.users == null) {
  //         //     _apiService.createUsers(users).then((isSuccess) {
  //         //       setState(() => _isLoading = false);
  //         //       if (isSuccess) {
  //         //         Navigator.pop(_scaffoldState.currentState.context);
  //         //       } else {
  //         //         _scaffoldState.currentState.showSnackBar(SnackBar(
  //         //           content: Text("Submit data gagal!!!"),
  //         //         ));
  //         //       }
  //         //     });
  //         //   } else {
  //         //     users.id = widget.users.id;
  //         //     print(users);
  //         //     _apiService.updateUsers(users).then((isSuccess) {
  //         //       print(isSuccess);

  //         //       setState(() => _isLoading = false);
  //         //       if (isSuccess) {
  //         //         Navigator.pop(_scaffoldState.currentState.context);
  //         //       } else {
  //         //         _scaffoldState.currentState.showSnackBar(SnackBar(
  //         //           content: Text("Update data gagal!!!"),
  //         //         ));
  //         //       }
  //         //     });
  //         //   }
  //       },
  //       child: Text(
  //         "Submit".toUpperCase(),
  //         // widget.users == null
  //         //     ? "Submit".toUpperCase()
  //         //     : "Update Data".toUpperCase(),
  //         style: TextStyle(
  //           color: Colors.white,
  //         ),
  //       ),
  //       color: Colors.brown,
  //     ),
  //   );
  // }
}
