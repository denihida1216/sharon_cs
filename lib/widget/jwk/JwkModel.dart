import 'dart:convert';

class JwkModel {
  int id;
  String namajwk;
  int hari;
  String kdjalur;
  String namajalur;

  JwkModel({
    this.id,
    this.namajwk,
    this.hari,
    this.kdjalur,
    this.namajalur,
  });

  factory JwkModel.fromJson(Map<String, dynamic> map) {
    return JwkModel(
      id: int.parse(map["id"].toString()),
      namajwk: map["nama_jwk"],
      hari: int.parse(map["hari"].toString()),
      kdjalur: map["kd_jalur"],
      namajalur: map["nama_jalur"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "nama_jwk": namajwk,
      "hari": hari,
      "kd_jalur": kdjalur,
      "nama_jalur": namajalur,
    };
  }

  @override
  String toString() {
    return 'JwkModel{id: $id, ' +
        'nama_jwk: $namajwk, ' +
        'hari: $hari, ' +
        'kd_jalur: $kdjalur, ' +
        'nama_jalur: $namajalur}';
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['id'] = id;
    map['nama_jwk'] = namajwk;
    map['hari'] = hari;
    map['kd_jalur'] = kdjalur;
    map['nama_jalur'] = namajalur;
    return map;
  }
}

List<JwkModel> jwkModelFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<JwkModel>.from(data.map((item) => JwkModel.fromJson(item)));
}

String jwkModelToJson(JwkModel data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}
