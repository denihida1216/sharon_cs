import 'dart:convert';

class BarangModel {
  String kdbrg;
  String nama;
  double harga;

  BarangModel({
    this.kdbrg,
    this.nama,
    this.harga,
  });

  factory BarangModel.fromJson(Map<String, dynamic> map) {
    return BarangModel(
      kdbrg: map["kd_brg"],
      nama: map["nama"],
      harga: double.parse(map["harga"].toString()),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "kd_brg": kdbrg,
      "nama": nama,
      "harga": harga,
    };
  }

  @override
  String toString() {
    return 'BarangModel{kd_brg: $kdbrg, ' + 'nama: $nama, ' + 'harga: $harga}';
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['kd_brg'] = kdbrg;
    map['nama'] = nama;
    map['harga'] = harga;
    return map;
  }
}

List<BarangModel> barangModelFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<BarangModel>.from(data.map((item) => BarangModel.fromJson(item)));
}

String barangModelToJson(BarangModel data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}
