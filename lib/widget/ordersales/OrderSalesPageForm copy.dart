// import 'package:flutter/material.dart';
// import 'package:barcode_scan/barcode_scan.dart';
// import 'package:flutter/services.dart';
// import 'package:flutter_typeahead/flutter_typeahead.dart';
// import 'package:fluttertoast/fluttertoast.dart';
// import 'package:geocoder/geocoder.dart';
// import 'package:geolocator/geolocator.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:sharon_cs/helper/DbHelper.dart';
// import 'package:sharon_cs/services/ApiService.dart';
// import 'package:sharon_cs/widget/ordersales/OrderSalesDetailModel.dart';
// import 'package:sharon_cs/widget/ordersales/OrderSalesDetailPageForm.dart';
// import 'package:sharon_cs/widget/ordersales/OrderSalesModel.dart';
// import 'package:sqflite/sqflite.dart';

// void main() {
//   runApp(new MaterialApp(
//     title: "Order Sales",
//     home: new OrderSalesPageForm(),
//   ));
// }

// final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

// class OrderSalesPageForm extends StatefulWidget {
//   static String tag = 'OrderSalesPage-pageform';
//   final OrderSalesModel ordersales;
//   OrderSalesPageForm({this.ordersales});

//   // static const String routeName = "/OrderSalesPageForm";
//   @override
//   _OrderSalesPageFormState createState() => _OrderSalesPageFormState();
// }

// class _OrderSalesPageFormState extends State<OrderSalesPageForm> {
//   int _currentStep = 0;
//   BuildContext context;
//   ApiService apiService;
//   // bool _isLoading = false;
//   String barcode = "";
//   bool _isFieldTanggalValid;
//   // bool _isFieldKode1Valid;
//   // bool _isFieldKode2Valid;
//   bool _isFieldNamaValid;
//   // bool _isFieldAlamatValid;
//   // bool _isFieldKdjalurValid;
//   // bool _isFieldNamajalurValid;
//   TextEditingController _controllerID = TextEditingController();
//   TextEditingController _controllerKode1 = TextEditingController();
//   TextEditingController _controllerKode2 = TextEditingController();
//   TextEditingController _controllerTanggal = TextEditingController();
//   TextEditingController _controllerNama = TextEditingController();
//   TextEditingController _controllerAlamat = TextEditingController();
//   TextEditingController _controllerKdjalur = TextEditingController();
//   TextEditingController _controllerNamajalur = TextEditingController();
//   TextEditingController _controllerLat = TextEditingController();
//   TextEditingController _controllerLong = TextEditingController();
//   TextEditingController _controllerAddress = TextEditingController();
//   TextEditingController _controllerNote = TextEditingController();
//   // File _image1;
//   List<OrderSalesDetailModel> orderDetail = List();
//   DbHelper dbHelper = DbHelper();
//   int count = 0;

//   DateTime selectedDate = DateTime.now();
//   Future<Null> _selectDate(BuildContext context) async {
//     final DateTime picked = await showDatePicker(
//       context: context,
//       initialDate: selectedDate,
//       firstDate: DateTime(2015, 8),
//       lastDate: DateTime(2101),
//       builder: (BuildContext context, Widget child) {
//         return Theme(
//           data:
//               ThemeData(primarySwatch: Colors.brown, splashColor: Colors.brown),
//           child: child,
//         );
//       },
//     );
//     if (picked != null && picked != selectedDate)
//       setState(() {
//         selectedDate = picked;
//         _controllerTanggal.text = "${selectedDate.toLocal()}".split(' ')[0];
//       });
//   }

//   var uid;
//   var user;
//   var kdsales;
//   var namasales;
//   getPref() async {
//     SharedPreferences preferences = await SharedPreferences.getInstance();
//     setState(() {
//       uid = preferences.getString("id");
//       user = preferences.getString("user");
//       kdsales = preferences.getString("kd_sales");
//       namasales = preferences.getString("nama_sales");
//     });
//   }

//   Future scanCode() async {
//     try {
//       barcode = await BarcodeScanner.scan();
//       setState(() => this.barcode = barcode);
//       Fluttertoast.showToast(
//           msg: "" + barcode,
//           toastLength: Toast.LENGTH_SHORT,
//           gravity: ToastGravity.BOTTOM,
//           timeInSecForIos: 1,
//           backgroundColor: Colors.white,
//           textColor: Colors.black,
//           fontSize: 16.0);
//     } catch (e) {
//       if (e.code == BarcodeScanner.CameraAccessDenied) {
//         setState(() {
//           this.barcode = 'The user did not grant the camera permission!';
//         });
//       } else {
//         setState(() => this.barcode = 'Unknown error: $e');
//       }
//     }
//   }

//   @override
//   void initState() {
//     apiService = ApiService();
//     getPref();
//     // updateListView();
//     if (widget.ordersales != null) {
//       _isFieldTanggalValid = true;
//       _isFieldNamaValid = true;
//       _controllerID.text = widget.ordersales.id;
//       _controllerTanggal.text = widget.ordersales.tanggal;
//       _controllerNama.text = widget.ordersales.nama;
//       _controllerKode1.text = widget.ordersales.kdplg1;
//       _controllerKode2.text = widget.ordersales.kdplg2;
//       _controllerAlamat.text = widget.ordersales.alamat;
//       _controllerKdjalur.text = widget.ordersales.kdjalur;
//       _controllerNamajalur.text = widget.ordersales.namajalur;
//       _controllerLat.text = widget.ordersales.latitude;
//       _controllerLong.text = widget.ordersales.longitude;
//       _controllerAddress.text = widget.ordersales.address;
//       _controllerNote.text = widget.ordersales.catatan;
//       dbHelper.deleteallOrdersales();
//       print(widget.ordersales.id);
//       apiService.getOrderSalesDetail(widget.ordersales.id).then((order) {
//         print(order);
//         int count = order.length;
//         for (int i = 0; i < count; i++) {
//           print(order[i].kdbrg);
//           OrderSalesDetailModel data = OrderSalesDetailModel(
//             kdbrg: order[i].kdbrg,
//             namabrg: order[i].namabrg,
//             qtyorder: order[i].qtyorder,
//             harga: 0,
//           );
//           dbHelper.insertOrdersales(data);
//         }
//       });
//     } else {
//       dbHelper.deleteallOrdersales();
//     }
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     this.context = context;

//     return Scaffold(
//       key: _scaffoldState,
//       appBar: AppBar(
//         title: Text('Order Sales'),
//         backgroundColor: Colors.brown,
//       ),
//       body: Theme(
//         data: ThemeData(
//           primaryColor: Colors.brown,
//           accentColor: Colors.brown,
//           cursorColor: Colors.brown,
//         ),
//         child: _typeStep(),
//       ),
//     );
//   }

//   // Widget _buildContainer(List<Barang> barangs) {
//   //   return Container(
//   //     padding: EdgeInsets.all(20.0),
//   //     child: Center(
//   //       child: Column(
//   //         children: <Widget>[
//   //           _buildTextField(barangs),
//   //           SizedBox(height: 16.0),
//   //           _buildOutput(),
//   //         ],
//   //       ),
//   //     ),
//   //   );
//   // }

//   // Widget _buildOutput() {
//   //   return Text("""
//   //   Result for barang selected :

//   //   ID    : ${barang.kdbrg ?? '-'}
//   //   Name  : ${barang.nama ?? '-'}""");
//   // }

//   Widget _typeStep() {
//     return Stepper(
//         type: StepperType.horizontal,
//         currentStep: _currentStep,
//         onStepTapped: (int step) => setState(() => _currentStep = step),
//         onStepContinue:
//             _currentStep < 2 ? () => setState(() => _currentStep += 1) : null,
//         onStepCancel:
//             _currentStep > 0 ? () => setState(() => _currentStep -= 1) : null,
//         steps: [
//           Step(
//             title: Text('Pilih Toko'),
//             content: _buildConten1(),
//             isActive: _currentStep >= 0,
//             state: _currentStep >= 0 ? StepState.complete : StepState.disabled,
//           ),
//           Step(
//             title: Text('Order'),
//             content: _buildConten2(),
//             isActive: _currentStep >= 0,
//             state: _currentStep >= 1 ? StepState.complete : StepState.disabled,
//           ),
//         ],
//         controlsBuilder: (BuildContext context,
//             {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
//           return Row(
//             mainAxisSize: MainAxisSize.max,
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             children: <Widget>[
//               _currentStep >= 1
//                   ? RaisedButton(
//                       onPressed: () {
//                         onStepCancel();
//                       },
//                       child: const Text('Kembali'),
//                     )
//                   : Container(),
//               _currentStep >= 1
//                   ? RaisedButton(
//                       onPressed: () {
//                         _simpanDb();
//                         Navigator.pop(_scaffoldState.currentState.context);
//                       },
//                       color: Colors.brown,
//                       textColor: Colors.white,
//                       child: const Text('Simpan'),
//                     )
//                   : RaisedButton(
//                       onPressed: () {
//                         onStepContinue();
//                         _getLocation();
//                       },
//                       color: Colors.brown,
//                       textColor: Colors.white,
//                       child: const Text('Lanjutkan'),
//                     ),
//             ],
//           );
//         });
//   }

//   Widget _buildConten1() {
//     return ListView(
//       physics: const ClampingScrollPhysics(),
//       shrinkWrap: true,
//       children: <Widget>[
//         _buildTextFieldTanggal(),
//         _buildTextFieldNama(),
//         _buildTextFieldKode1(),
//         _buildTextFieldKode2(),
//         _buildTextFieldAlamat(),
//         _buildTextFieldKdJalur(),
//         _buildTextFieldNamaJalur(),
//         // _buildTextFieldLat(),
//         // _buildTextFieldLong(),
//         // _buildTextFieldAddress(),
//         SizedBox(height: 16.0),
//       ],
//     );
//   }

//   Widget _buildConten2() {
//     return ListView(
//       physics: const ClampingScrollPhysics(),
//       shrinkWrap: true,
//       children: <Widget>[
//         _buildMenuStockInput(),
//         FutureBuilder(
//           future: dbHelper.getOrderSalesList(),
//           builder: (BuildContext context, AsyncSnapshot snapshot) {
//             if (snapshot.hasError) {
//               return Center(
//                 child: Text(
//                     "Something wrong with message: ${snapshot.error.toString()}"),
//               );
//             } else if (snapshot.connectionState == ConnectionState.done) {
//               List<OrderSalesDetailModel> data = snapshot.data;
//               return _buildListView(data);
//             } else {
//               return Center(
//                 child: CircularProgressIndicator(),
//               );
//             }
//           },
//         ),
//         _buildTextFieldNote(),
//         SizedBox(height: 16.0),
//         // _isLoading
//         //     ? Stack(
//         //         children: <Widget>[
//         //           Opacity(
//         //             opacity: 0.3,
//         //             child: ModalBarrier(
//         //               dismissible: false,
//         //               color: Colors.grey,
//         //             ),
//         //           ),
//         //           Center(
//         //             child: CircularProgressIndicator(),
//         //           ),
//         //         ],
//         //       )
//         //     : Container(),
//       ],
//     );
//   }

//   _getLocation() async {
//     Position position = await Geolocator()
//         .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
//     print('location: ${position.latitude}');
//     final coordinates = new Coordinates(position.latitude, position.longitude);
//     var addresses =
//         await Geocoder.local.findAddressesFromCoordinates(coordinates);
//     var first = addresses.first;
//     print(
//         "${first.postalCode} : ${first.adminArea} : ${first.countryName} : ${first.countryCode} : ${first.featureName} : ${first.addressLine}");
//     _controllerLat.text = position.latitude.toString();
//     _controllerLong.text = position.longitude.toString();
//     _controllerAddress.text = first.addressLine;
//   }

//   _simpanDb() {
//     OrderSalesModel data = OrderSalesModel(
//       id: _controllerID.text.toString(),
//       tanggal: _controllerTanggal.text.toString(),
//       kdplg1: _controllerKode1.text.toString(),
//       kdplg2: _controllerKode2.text.toString(),
//       nama: _controllerNama.text.toString(),
//       alamat: _controllerAlamat.text.toString(),
//       kdjalur: _controllerKdjalur.text.toString(),
//       namajalur: _controllerNamajalur.text.toString(),
//       latitude: _controllerLat.text.toString(),
//       longitude: _controllerLong.text.toString(),
//       address: _controllerAddress.text.toString(),
//       catatan: _controllerNote.text.toString(),
//       uid: uid,
//       user: user,
//       kdsales: kdsales,
//       namasales: namasales,
//     );
//     // print(data);
//     if (widget.ordersales == null) {
//       apiService.createOrderSales(data).then((idordersales) {
//         // print(cekstore);
//         final Future<Database> dbFuture = dbHelper.initDb();
//         dbFuture.then((database) {
//           Future<List<OrderSalesDetailModel>> contactListFuture =
//               dbHelper.getOrderSalesList();
//           contactListFuture.then((contactList) {
//             int count = contactList.length;
//             for (int i = 0; i < count; i++) {
//               print(idordersales);
//               OrderSalesDetailModel data = OrderSalesDetailModel(
//                 idordersales: idordersales,
//                 kdbrg: contactList[i].kdbrg,
//                 namabrg: contactList[i].namabrg,
//                 qtyorder: contactList[i].qtyorder,
//                 harga: contactList[i].harga,
//               );
//               apiService.createOrderSalesDetail(data);
//             }
//           });
//         });
//       });
//     } else {
//       print('updateOrderSales');
//       print(data);
//       apiService.updateOrderSales(data).then((idordersales) {
//         print('updateOrderSales=>' + idordersales.replaceAll('"', ''));

//         apiService.deleteOrderSalesDetail(idordersales != null
//             ? int.parse(idordersales.replaceAll('"', '').toString())
//             : 0);
//         final Future<Database> dbFuture = dbHelper.initDb();
//         dbFuture.then((database) {
//           Future<List<OrderSalesDetailModel>> contactListFuture =
//               dbHelper.getOrderSalesList();
//           contactListFuture.then((contactList) {
//             int count = contactList.length;
//             for (int i = 0; i < count; i++) {
//               print('idcekstore=>' + idordersales.replaceAll('"', ''));
//               OrderSalesDetailModel data = OrderSalesDetailModel(
//                 idordersales: idordersales.replaceAll('"', ''),
//                 kdbrg: contactList[i].kdbrg,
//                 namabrg: contactList[i].namabrg,
//                 qtyorder: contactList[i].qtyorder,
//                 harga: contactList[i].harga,
//               );
//               apiService.createOrderSalesDetail(data);
//             }
//           });
//         });
//       });
//     }
//     // Navigator.of(context).pop();
//   }

//   //update contact
//   void updateListView() {
//     final Future<Database> dbFuture = dbHelper.initDb();
//     dbFuture.then((database) {
//       Future<List<OrderSalesDetailModel>> contactListFuture =
//           dbHelper.getOrderSalesList();
//       contactListFuture.then((contactList) {
//         setState(() {
//           this.orderDetail = contactList;
//           this.count = contactList.length;
//         });
//       });
//     });
//   }

//   Widget _buildListView(List<OrderSalesDetailModel> list) {
//     return Container(
//       height: 300,
//       child: ListView.builder(
//         shrinkWrap: true,
//         itemBuilder: (context, index) {
//           OrderSalesDetailModel rec = list[index];
//           return Card(
//             child: ListTile(
//               onTap: () {
//                 // print('bismilah' + rec.id);
//                 Navigator.push(context, MaterialPageRoute(builder: (context) {
//                   return OrderSalesDetailPageForm(orderdetail: rec);
//                 }));
//               },
//               leading: CircleAvatar(
//                 backgroundColor: Colors.brown,
//                 child: Icon(
//                   Icons.shopping_cart,
//                   color: Colors.white,
//                 ),
//               ),
//               title: Text(
//                 rec.kdbrg,
//               ),
//               subtitle: Text(
//                 '' +
//                     rec.namabrg +
//                     '\nQty Order: ' +
//                     rec.qtyorder.toString() +
//                     ' Pcs',
//               ),
//               trailing: IconButton(
//                   icon: Icon(
//                     Icons.delete,
//                   ),
//                   onPressed: () {
//                     dbHelper.deleteOrdersales(rec.kdbrg).then((count) {
//                       if (count > 0) {
//                         updateListView();
//                       }
//                     });
//                   }),
//             ),
//           );
//         },
//         itemCount: list.length,
//       ),
//     );
//   }

//   Widget _buildTextFieldTanggal() {
//     return TextField(
//       controller: _controllerTanggal,
//       keyboardType: TextInputType.text,
//       decoration: InputDecoration(
//         labelText: "Tanggal",
//         errorText: _isFieldTanggalValid == null || _isFieldTanggalValid
//             ? null
//             : "Tanggal harus diisi!!!",
//       ),
//       onChanged: (value) {
//         bool isFieldValid = value.trim().isNotEmpty;
//         if (isFieldValid != _isFieldTanggalValid) {
//           setState(() => _isFieldTanggalValid = isFieldValid);
//         }
//       },
//       onTap: () {
//         _selectDate(context);
//       },
//     );
//   }

//   Widget _buildTextFieldNama() {
//     return TypeAheadField(
//       textFieldConfiguration: TextFieldConfiguration(
//         autofocus: false,
//         controller: _controllerNama,
//         style: TextStyle(
//           color: Colors.black54,
//           fontSize: 18.0,
//         ),
//         keyboardType: TextInputType.text,
//         decoration: InputDecoration(
//           labelText: "Nama",
//           errorText: _isFieldNamaValid == null || _isFieldNamaValid
//               ? null
//               : "Nama harus diisi!!!",
//           labelStyle: TextStyle(color: Colors.black38),
//           enabledBorder: UnderlineInputBorder(
//             borderSide: BorderSide(color: Colors.brown),
//           ),
//         ),
//         onChanged: (value) {
//           bool isFieldValid = value.trim().isNotEmpty;
//           if (isFieldValid != _isFieldNamaValid) {
//             setState(() => _isFieldNamaValid = isFieldValid);
//           }
//         },
//       ),
//       suggestionsCallback: (pattern) async {
//         return await apiService.getpelangganSuggestions(pattern);
//       },
//       itemBuilder: (context, suggestion) {
//         return ListTile(
//           // leading: Icon(Icons.check_box),
//           title: Text(suggestion['nama']),
//           subtitle: Text(
//               'Kode internal : ${suggestion['kd_plg']}\nKode external : ${suggestion['jp']}\nJalur : ${suggestion['kd_jalur']} - ${suggestion['nama_jalur']}\nAlamat : ${suggestion['alamat']}'),
//         );
//       },
//       onSuggestionSelected: (suggestion) {
//         _controllerNama.text = suggestion['nama'];
//         _controllerKode1.text = suggestion['kd_plg'];
//         _controllerKode2.text = suggestion['jp'];
//         // _controllerCluster.text = suggestion['cluster'];
//         _controllerAlamat.text = suggestion['alamat'];
//         _controllerKdjalur.text = suggestion['kd_jalur'];
//         _controllerNamajalur.text = suggestion['nama_jalur'];
//         // Navigator.of(context).push(MaterialPageRoute(
//         //     builder: (context) => ProductPage(product: suggestion)));
//       },
//     );
//   }

//   Widget _buildTextFieldKode1() {
//     return TextField(
//       controller: _controllerKode1,
//       keyboardType: TextInputType.text,
//       readOnly: true,
//       decoration: InputDecoration(
//         labelText: "Kode Internal",
//         // errorText: _isFieldKode1Valid == null || _isFieldKode1Valid
//         //     ? null
//         //     : "Kode Internal harus diisi!!!",
//       ),
//       // onChanged: (value) {
//       //   bool isFieldValid = value.trim().isNotEmpty;
//       //   if (isFieldValid != _isFieldKode1Valid) {
//       //     setState(() => _isFieldKode1Valid = isFieldValid);
//       //   }
//       // },
//     );
//   }

//   Widget _buildTextFieldKode2() {
//     return TextField(
//       controller: _controllerKode2,
//       keyboardType: TextInputType.text,
//       readOnly: true,
//       decoration: InputDecoration(
//         labelText: "Kode External",
//         // errorText: _isFieldKode2Valid == null || _isFieldKode2Valid
//         //     ? null
//         //     : "Kode External harus diisi!!!",
//       ),
//       // onChanged: (value) {
//       //   bool isFieldValid = value.trim().isNotEmpty;
//       //   if (isFieldValid != _isFieldKode2Valid) {
//       //     setState(() => _isFieldKode2Valid = isFieldValid);
//       //   }
//       // },
//     );
//   }

//   Widget _buildTextFieldAlamat() {
//     return TextField(
//       controller: _controllerAlamat,
//       keyboardType: TextInputType.text,
//       maxLines: null,
//       readOnly: true,
//       decoration: InputDecoration(
//         labelText: "Alamat",
//         // errorText: _isFieldAlamatValid == null || _isFieldAlamatValid
//         //     ? null
//         //     : "Alamat harus diisi!!!",
//       ),
//       // onChanged: (value) {
//       //   bool isFieldValid = value.trim().isNotEmpty;
//       //   if (isFieldValid != _isFieldAlamatValid) {
//       //     setState(() => _isFieldAlamatValid = isFieldValid);
//       // //   }
//       // },
//     );
//   }

//   Widget _buildTextFieldKdJalur() {
//     return TextField(
//       controller: _controllerKdjalur,
//       keyboardType: TextInputType.text,
//       readOnly: true,
//       decoration: InputDecoration(
//         labelText: "Kode Jalur",
//         // errorText: _isFieldKdjalurValid == null || _isFieldKdjalurValid
//         //     ? null
//         //     : "Kode Jalur harus diisi!!!",
//       ),
//       // onChanged: (value) {
//       //   bool isFieldValid = value.trim().isNotEmpty;
//       //   if (isFieldValid != _isFieldKdjalurValid) {
//       //     setState(() => _isFieldKdjalurValid = isFieldValid);
//       //   }
//       // },
//     );
//   }

//   Widget _buildTextFieldNamaJalur() {
//     return TextField(
//       controller: _controllerNamajalur,
//       keyboardType: TextInputType.text,
//       readOnly: true,
//       decoration: InputDecoration(
//         labelText: "Nama Jalur",
//         // errorText: _isFieldNamajalurValid == null || _isFieldNamajalurValid
//         //     ? null
//         //     : "Nama Jalur harus diisi!!!",
//       ),
//       // onChanged: (value) {
//       //   bool isFieldValid = value.trim().isNotEmpty;
//       //   if (isFieldValid != _isFieldNamajalurValid) {
//       //     setState(() => _isFieldNamajalurValid = isFieldValid);
//       //   }
//       // },
//     );
//   }

//   // Widget _buildTextFieldLat() {
//   //   return TextField(
//   //     controller: _controllerLat,
//   //     keyboardType: TextInputType.text,
//   //     readOnly: true,
//   //     decoration: InputDecoration(
//   //       labelText: "Latitude",
//   //     ),
//   //   );
//   // }

//   // Widget _buildTextFieldLong() {
//   //   return TextField(
//   //     controller: _controllerLong,
//   //     keyboardType: TextInputType.text,
//   //     readOnly: true,
//   //     decoration: InputDecoration(
//   //       labelText: "Longitude",
//   //     ),
//   //   );
//   // }

//   // Widget _buildTextFieldAddress() {
//   //   return TextField(
//   //     controller: _controllerAddress,
//   //     keyboardType: TextInputType.text,
//   //     maxLines: null,
//   //     readOnly: true,
//   //     decoration: InputDecoration(
//   //       labelText: "Address Line",
//   //     ),
//   //   );
//   // }

//   Widget _buildMenuStockInput() {
//     return Row(
//       mainAxisAlignment: MainAxisAlignment.center,
//       children: <Widget>[
//         FlatButton(
//             child: Column(
//               children: <Widget>[
//                 Icon(
//                   Icons.add_to_queue,
//                   color: Colors.green,
//                 ),
//                 Text("Tambah Item")
//               ],
//             ),
//             onPressed: () {
//               Navigator.pushNamed(context, OrderSalesDetailPageForm.tag);
//             }),
//         FlatButton(
//             child: Column(
//               children: <Widget>[
//                 Icon(
//                   Icons.settings_remote,
//                   color: Colors.blue,
//                 ),
//                 Text("Tambah Scan")
//               ],
//             ),
//             onPressed: () {
//               scanCode();
//             }),
//             FlatButton(
//             child: Column(
//               children: <Widget>[
//                 Icon(
//                   Icons.refresh,
//                   color: Colors.orange,
//                 ),
//                 Text("Refresh")
//               ],
//             ),
//             onPressed: () {
//               updateListView();
//             }),
//       ],
//     );
//   }

//   Widget _buildTextFieldNote() {
//     return TextField(
//       controller: _controllerNote,
//       keyboardType: TextInputType.text,
//       maxLines: null,
//       decoration: InputDecoration(
//         labelText: "Catatan",
//         // errorText: _isFieldNoteValid == null || _isFieldNoteValid
//         //     ? null
//         //     : "Catatan harus diisi!!!",
//       ),
//       // onChanged: (value) {
//       //   bool isFieldValid = value.trim().isNotEmpty;
//       //   if (isFieldValid != _isFieldNoteValid) {
//       //     setState(() => _isFieldNoteValid = isFieldValid);
//       //   }
//       // },
//     );
//   }
// }
