import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sharon_cs/helper/DbHelper.dart';
import 'package:sharon_cs/services/ApiService.dart';
import 'package:sharon_cs/widget/ordersales/OrderSalesDetailModel.dart';
import 'package:sharon_cs/widget/ordersales/OrderSalesModel.dart';
import 'package:sharon_cs/widget/ordersales/OrderSalesPageForm.dart';

void main() {
  runApp(new MaterialApp(
    title: "Check Store List",
    home: new OrderSalesPage(),
  ));
}

class OrderSalesPage extends StatefulWidget {
  static String tag = 'OrderSalesPage-page';

  @override
  _OrderSalesPageState createState() => _OrderSalesPageState();
}

class _OrderSalesPageState extends State<OrderSalesPage> {
  BuildContext context;
  ApiService apiService;
  List<OrderSalesDetailModel> orderdetail = List();
  DbHelper dbHelper = DbHelper();
  int count = 0;

  @override
  void initState() {
    super.initState();
    apiService = ApiService();
    getPref();
    // values();
  }

  var title;
  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      title = preferences.getString("title");
    });
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    return Scaffold(
      appBar: AppBar(
        title: new Text(title),
        backgroundColor: Colors.brown,
      ),
      body: FutureBuilder(
        future: apiService.getOrderSales('','','',''),
        builder: (BuildContext context,
            AsyncSnapshot<List<OrderSalesModel>> snapshot) {
          if (snapshot.hasError) {
            return Center(
              child: Text(
                  "Something wrong with message: ${snapshot.error.toString()}"),
            );
          } else if (snapshot.connectionState == ConnectionState.done) {
            List<OrderSalesModel> data = snapshot.data;
            return _buildListView(data);
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
  
  Widget _buildListView(List<OrderSalesModel> list) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
      child: ListView.builder(
        itemBuilder: (context, index) {
          OrderSalesModel rec = list[index];
          return Card(
            child: ListTile(
              onTap: () {
                // print('bismilah' + rec.id);
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return OrderSalesPageForm(ordersales: rec);
                }));
              },
              leading: CircleAvatar(
                backgroundColor: Colors.brown,
                child: Icon(
                  Icons.swap_horiz,
                  color: Colors.white,
                ),
              ),
              title: Text(
                rec.nama,
              ),
              subtitle: Text(
                'Jalur: ' +rec.kdjalur + ' - ' + rec.namajalur + '\n' + rec.alamat,
              ),
              trailing: IconButton(
                  icon: Icon(
                    Icons.delete,
                  ),
                  onPressed: () {
                    dbHelper.deleteOrdersales(rec.id).then((count) {
                      if (count > 0) {
                        // updateListView();
                      }
                    });
                  }),
            ),
          );
          // Card(
          //   child: ListTile(
          //     onTap: () {
          //       print('bismilah' + rec.id);
          //       Navigator.pushNamed(context, OrderSalesPageForm.tag);
          //     },
          //     leading: CircleAvatar(
          //       backgroundColor: Colors.transparent,
          //       child: rec.imageurl == ''
          //           ? Image.asset('assets/sharon.jpg')
          //           : Image.network(rec.imageurl),
          //       // child: Image.network(rec.gambar),
          //     ),
          //     title: Row(
          //       children: <Widget>[
          //         Expanded(
          //             child: Text(
          //           rec.nama,
          //           style: TextStyle(
          //             color: Colors.black,
          //             fontWeight: FontWeight.w700,
          //           ),
          //         )),
          //       ],
          //     ),
          //     subtitle: Row(
          //       children: <Widget>[
          //         Expanded(
          //             child: Text(
          //           rec.kdjalur + ' - ' + rec.namajalur + '\n' + rec.alamat,
          //           style: TextStyle(
          //             color: Colors.black,
          //             fontWeight: FontWeight.w700,
          //           ),
          //         )),
          //       ],
          //     ),
          //     // trailing: IconButton(
          //     //     icon: Icon(
          //     //       Icons.exit_to_app,
          //     //       size: 40,
          //     //       ),
          //     //     onPressed: () {
          //     //       // signOut();
          //     //     }),
          //   ),
          // );
        },
        itemCount: list.length,
      ),
    );
  }
}
