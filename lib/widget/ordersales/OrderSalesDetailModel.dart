import 'dart:convert';

class OrderSalesDetailModel {
  String idordersales;
  String kdbrg;
  String namabrg;
  int qtyorder;
  double harga;

  OrderSalesDetailModel({
    this.idordersales,
    this.kdbrg,
    this.namabrg,
    this.qtyorder,
    this.harga,
  });

  factory OrderSalesDetailModel.fromJson(Map<String, dynamic> map) {
    return OrderSalesDetailModel(
      idordersales: map["id_ordersales"],
      kdbrg: map["kd_brg"],
      namabrg: map["nama_brg"],
      qtyorder: int.parse(map["qty_order"].toString()),
      harga: double.parse(map["harga"].toString())
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id_ordersales": idordersales,
      "kd_brg": kdbrg,
      "nama_brg": namabrg,
      "qty_order": qtyorder,
      "harga": harga
    };
  }

  @override
  String toString() {
    return 'OrderSalesDetailModel{' +
        'id_ordersales: $idordersales, ' +
        'kd_brg: $kdbrg, ' +
        'nama_brg: $namabrg, ' +
        'qty_order: $qtyorder, ' +
        'harga: $harga}';
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['id_ordersales'] = idordersales;
    map['kd_brg'] = kdbrg;
    map['nama_brg'] = namabrg;
    map['qty_order'] = qtyorder;
    map['harga'] = harga;
    return map;
  }
}

List<OrderSalesDetailModel> orderSalesDetailModelFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<OrderSalesDetailModel>.from(
      data.map((item) => OrderSalesDetailModel.fromJson(item)));
}

String orderSalesDetailModelToJson(OrderSalesDetailModel data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}
