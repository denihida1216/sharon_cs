import 'package:flutter/material.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/services.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:sharon_cs/helper/DbHelper.dart';
import 'package:sharon_cs/services/ApiService.dart';
import 'package:sharon_cs/widget/ordersales/OrderSalesDetailModel.dart';
import 'package:sqflite/sqflite.dart';

final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

void main() {
  runApp(new MaterialApp(
    title: "Order Detail Sales",
    home: new OrderSalesDetailPageForm(),
  ));
}

class OrderSalesDetailPageForm extends StatefulWidget {
  static String tag = 'OrderSalesDetailPage-pageform';
  final OrderSalesDetailModel orderdetail;
  OrderSalesDetailPageForm({this.orderdetail});

  @override
  _OrderSalesDetailPageFormState createState() =>
      _OrderSalesDetailPageFormState();
}

class _OrderSalesDetailPageFormState extends State<OrderSalesDetailPageForm> {
  BuildContext context;
  ApiService apiService;
  List<OrderSalesDetailModel> orderDetail = List();
  DbHelper dbHelper = DbHelper();
  int count = 0;
  bool _isLoading = false;
  String barcode = "";
  bool _isFieldKodeBrgValid;
  bool _isFieldNamaBrgValid;
  bool _isFieldQtyBrgValid;
  TextEditingController _controllerKodeBrg = TextEditingController();
  TextEditingController _controllerNamaBrg = TextEditingController();
  TextEditingController _controllerQtyBrg = TextEditingController();
  // TextEditingController _controllerJenis = TextEditingController();
  String dropdownValue = 'persentase';
  String holder = '';

  List<String> jenis = [
    'persentase',
    'quantity',
  ];

  Future scanCode() async {
    try {
      barcode = await BarcodeScanner.scan();
      setState(() => this.barcode = barcode);
      // Fluttertoast.showToast(
      //     msg: "" + barcode,
      //     toastLength: Toast.LENGTH_SHORT,
      //     gravity: ToastGravity.BOTTOM,
      //     timeInSecForIos: 1,
      //     backgroundColor: Colors.white,
      //     textColor: Colors.black,
      //     fontSize: 16.0);
    } catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          this.barcode = 'The user did not grant the camera permission!';
        });
      } else {
        setState(() => this.barcode = 'Unknown error: $e');
      }
    }
  }

  @override
  void initState() {
    apiService = ApiService();
    debugPrint(widget.orderdetail.toString());
    if (widget.orderdetail != null) {
      _isFieldKodeBrgValid = true;
      _isFieldNamaBrgValid = true;
      _isFieldQtyBrgValid = true;
      _controllerKodeBrg.text = widget.orderdetail.kdbrg;
      _controllerNamaBrg.text = widget.orderdetail.namabrg;
      _controllerQtyBrg.text = widget.orderdetail.qtyorder.toString();
      // _controllerJenis.text = widget.orderdetail.;
    }
    super.initState();
  }

  @override
  void dispose() {
    _controllerKodeBrg.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;

    return Scaffold(
      key: _scaffoldState,
      appBar: AppBar(
        title: Text('Order Detail Sales'),
        backgroundColor: Colors.brown,
      ),
      body: Theme(
        data: ThemeData(
          primaryColor: Colors.brown,
          accentColor: Colors.brown,
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: _buildContainer(),
        ),
      ),
    );
  }

  Widget _buildContainer() {
    return ListView(
      shrinkWrap: true,
      children: <Widget>[
        _buildTextFieldKodeBrg(),
        _buildTextFieldNamaBrg(),
        _buildTextFieldQtyBrg(),
        // SizedBox(height: 12.0),
        // Text('Jenis Estimasi'),
        // _buildDropdownJenis(),
        // _buildTextFieldQtyBrgEst(),
        _buildSubmit(),
        _isLoading
            ? Stack(
                children: <Widget>[
                  Opacity(
                    opacity: 0.3,
                    child: ModalBarrier(
                      dismissible: false,
                      color: Colors.grey,
                    ),
                  ),
                  Center(
                    child: CircularProgressIndicator(),
                  ),
                ],
              )
            : Container(),
      ],
    );
  }

  Widget _buildTextFieldKodeBrg() {
    return TypeAheadField(
      textFieldConfiguration: TextFieldConfiguration(
        autofocus: false,
        controller: _controllerKodeBrg,
        style: TextStyle(
          color: Colors.black54,
          fontSize: 18.0,
        ),
        enabled: widget.orderdetail == null ? true : false,
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          labelText: "Kode Barang",
          errorText: _isFieldKodeBrgValid == null || _isFieldKodeBrgValid
              ? null
              : "Kode Barang harus diisi!!!",
          labelStyle: TextStyle(color: Colors.black38),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.brown),
          ),
        ),
        onChanged: (value) {
          bool isFieldValid = value.trim().isNotEmpty;
          if (isFieldValid != _isFieldKodeBrgValid) {
            setState(() {
              _isFieldKodeBrgValid = isFieldValid;
              _isFieldNamaBrgValid = isFieldValid;
            });
          }
        },
      ),
      suggestionsCallback: (pattern) async {
        return await apiService.getbarangSuggestions(pattern);
      },
      itemBuilder: (context, suggestion) {
        return ListTile(
          // leading: Icon(Icons.check_box),
          title: Text(suggestion['nama']),
          subtitle: Text(
              'Kode : ${suggestion['kd_brg']}\nNama : ${suggestion['nama']}\nKelompok : ${suggestion['kelompok']}'),
        );
      },
      onSuggestionSelected: (suggestion) {
        _controllerKodeBrg.text = suggestion['kd_brg'];
        _controllerNamaBrg.text = suggestion['nama'];
        // Navigator.of(context).push(MaterialPageRoute(
        //     builder: (context) => ProductPage(product: suggestion)));
      },
    );
  }

  Widget _buildTextFieldNamaBrg() {
    return TextField(
      controller: _controllerNamaBrg,
      keyboardType: TextInputType.text,
      // readOnly: true,
      decoration: InputDecoration(
        labelText: "Nama Barang",
        errorText: _isFieldNamaBrgValid == null || _isFieldNamaBrgValid
            ? null
            : "Nama Barang harus diisi!!!",
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldNamaBrgValid) {
          setState(() => _isFieldNamaBrgValid = isFieldValid);
        }
      },
    );
  }

  Widget _buildTextFieldQtyBrg() {
    return TextField(
      controller: _controllerQtyBrg,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        labelText: "Qty Order",
        errorText: _isFieldQtyBrgValid == null || _isFieldQtyBrgValid
            ? null
            : "Qty Order harus diisi!!!",
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldQtyBrgValid) {
          setState(() => _isFieldQtyBrgValid = isFieldValid);
        }
      },
    );
  }

  Widget _buildSubmit() {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: RaisedButton(
        onPressed: () {
          if (_isFieldKodeBrgValid == null ||
              _isFieldNamaBrgValid == null ||
              _isFieldQtyBrgValid == null) {
            setState(() => {
                  _isFieldKodeBrgValid =
                      _controllerKodeBrg.text.trim().isNotEmpty,
                  _isFieldNamaBrgValid =
                      _controllerNamaBrg.text.trim().isNotEmpty,
                  _isFieldQtyBrgValid =
                      _controllerQtyBrg.text.trim().isNotEmpty,
                });
            _scaffoldState.currentState.showSnackBar(
              SnackBar(
                content: Text("Data belum lengkap !!!"),
              ),
            );
            return;
          }
          setState(() => _isLoading = true);
          OrderSalesDetailModel data = OrderSalesDetailModel(
            kdbrg: _controllerKodeBrg.text.toString(),
            namabrg: _controllerNamaBrg.text.toString(),
            qtyorder: _controllerQtyBrg.text.toString().trim().isNotEmpty
                ? int.parse(_controllerQtyBrg.text.toString())
                : 0,
            harga: 0,
          );

          print(data);
          if (widget.orderdetail == null) {
            addOrder(data);
            setState(() => _isLoading = false);
            Navigator.pop(_scaffoldState.currentState.context);
          } else {
            data.kdbrg = widget.orderdetail.kdbrg;
            editOrder(data);
            setState(() => _isLoading = false);
            Navigator.pop(_scaffoldState.currentState.context);
          }
        },
        child: Text(
          widget.orderdetail == null
              ? "Submit".toUpperCase()
              : "Update Data".toUpperCase(),
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        color: Colors.brown,
      ),
    );
  }

  //buat order
  void addOrder(OrderSalesDetailModel object) async {
    int result = await dbHelper.insertOrdersales(object);
    if (result > 0) {
      updateListView();
    }
  }

  //edit order
  void editOrder(OrderSalesDetailModel object) async {
    int result = await dbHelper.updateOrdersales(object);
    if (result > 0) {
      updateListView();
    }
  }

  //delete order
  void deleteOrder(OrderSalesDetailModel object) async {
    int result = await dbHelper.deleteOrdersales(object.kdbrg);
    if (result > 0) {
      updateListView();
    }
  }

  //update contact
  void updateListView() {
    final Future<Database> dbFuture = dbHelper.initDb();
    dbFuture.then((database) {
      // Future<List<OrderSalesDetailModel>> contactListFuture =
      //     dbHelper.getOrderSalesList();
      // contactListFuture.then((contactList) {
      //   setState(() {
      //     this.orderDetail = contactList;
      //     this.count = contactList.length;
      //   });
      // });
    });
  }
}
