import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:intl/intl.dart';
import 'package:sharon_cs/helper/DbHelper.dart';
import 'package:sharon_cs/services/ApiService.dart';
import 'package:sharon_cs/widget/ordersales/OrderSalesDetailModel.dart';
import 'package:sharon_cs/widget/ordersales/OrderSalesModel.dart';
import 'package:sqflite/sqlite_api.dart';

void main() {
  runApp(new MaterialApp(
    title: "Order Sales",
    home: new OrderSalesPageForm(),
  ));
}

final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

class OrderSalesPageForm extends StatefulWidget {
  static String tag = 'OrderSalesPage-pageform';
  final OrderSalesModel ordersales;
  OrderSalesPageForm({this.ordersales});

  @override
  _OrderSalesPageFormState createState() => _OrderSalesPageFormState();
}

class _OrderSalesPageFormState extends State<OrderSalesPageForm> {
  BuildContext context;
  ApiService apiService;
  DbHelper dbHelper = DbHelper();
  bool _isFieldKodeBrgValid;
  bool _isFieldNamaBrgValid;
  bool _isFieldQtyBrgValid;
  TextEditingController _controllerKodeBrg = TextEditingController();
  TextEditingController _controllerNamaBrg = TextEditingController();
  TextEditingController _controllerQtyBrg = TextEditingController();
  int _totalitem = 0;
  int _totalqty = 0;
  double _totalharga = 0;
  var noSimbolInUSFormat = NumberFormat.currency(locale: "en_US", symbol: "");

  @override
  void initState() {
    apiService = ApiService();
    // debugPrint(widget.orderdetail.toString());
    // if (widget.orderdetail != null) {
    //   _isFieldKodeBrgValid = true;
    //   _isFieldNamaBrgValid = true;
    //   _isFieldQtyBrgValid = true;
    //   _controllerKodeBrg.text = widget.orderdetail.kdbrg;
    //   _controllerNamaBrg.text = widget.orderdetail.namabrg;
    //   _controllerQtyBrg.text = widget.orderdetail.qtyorder.toString();
    //   // _controllerJenis.text = widget.orderdetail.;
    // }
    updateListView();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    return Scaffold(
      key: _scaffoldState,
      body: Container(
        child: Container(
          height: double.maxFinite,
          child: Stack(
            children: <Widget>[backgroundHeader(), viewList(), summaryCash()],
          ),
        ),
      ),
    );
  }

  Widget backgroundHeader() {
    return Container(
      height: 300,
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.brown,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(30),
          bottomRight: Radius.circular(30),
        ),
      ),
      child: Padding(
        padding:
            const EdgeInsets.only(top: 30, left: 15, right: 15, bottom: 15),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              color: Colors.white,
              child: ListTile(
                leading: CircleAvatar(
                  backgroundColor: Colors.transparent,
                  child: Icon(
                    Icons.account_circle,
                    color: Colors.grey,
                    size: 40,
                  ),
                ),
                title: Text(
                  'Nama Pelanggan',
                ),
                subtitle: Text(
                  'Jalur\nAlamat',
                ),
              ),
            ),
            Container(
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Expanded(
                    flex: 1,
                    child: Container(
                      margin: EdgeInsets.only(left: 5, right: 5),
                      color: Colors.white,
                      child: _buildTextFieldKodeBrg(),
                    ),
                  ),
                  new Expanded(
                    flex: 1,
                    child: Container(
                      margin: EdgeInsets.only(left: 5, right: 5),
                      color: Colors.white,
                      child: _buildTextFieldQtyBrg(),
                    ),
                  ),
                  new Expanded(
                    flex: 0,
                    child: Container(
                      color: Colors.white,
                      child: _buildSubmitOrder(),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget viewList() {
    return Positioned(
      child: new Align(
        alignment: FractionalOffset.center,
        child: FutureBuilder(
          future: dbHelper.getOrderSalesList(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text(
                    "Something wrong with message: ${snapshot.error.toString()}"),
              );
            } else if (snapshot.connectionState == ConnectionState.done) {
              List<OrderSalesDetailModel> data = snapshot.data;
              return _buildListView(data);
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ),
      ),
    );
  }

  Widget summaryCash() {
    return new Positioned(
      child: new Align(
        alignment: FractionalOffset.bottomCenter,
        child: Container(
          color: Colors.white,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              new Expanded(
                flex: 1,
                child: Container(
                  child: ListTile(
                    title: Text(
                      'Total Item : ${_totalitem.toString()}\nTotal Qty   : ${_totalqty.toString()}\nTotal : Rp. ${noSimbolInUSFormat.format(double.parse(_totalharga.toString()))}',
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 0,
                child: Container(
                  margin: EdgeInsets.only(left: 10, right: 10),
                  child: _buildSubmit(),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildListView(List<OrderSalesDetailModel> list) {
    return Container(
      height: double.maxFinite,
      color: Colors.white,
      margin: EdgeInsets.only(top: 155, left: 15, right: 15, bottom: 70),
      child: ListView.builder(
        shrinkWrap: true,
        itemBuilder: (context, index) {
          OrderSalesDetailModel rec = list[index];
          return Card(
            child: ListTile(
              onTap: () {
                // print('bismilah' + rec.id);
                // Navigator.push(context, MaterialPageRoute(builder: (context) {
                //   return OrderSalesDetailPageForm(orderdetail: rec);
                // }));
              },
              leading: CircleAvatar(
                backgroundColor: Colors.brown,
                child: Icon(
                  Icons.shopping_cart,
                  color: Colors.white,
                ),
              ),
              title: Text(
                rec.kdbrg,
              ),
              subtitle: Text(
                '' +
                    rec.namabrg +
                    '\nQty Order: ' +
                    rec.qtyorder.toString() +
                    ' Pcs',
              ),
              trailing: IconButton(
                  icon: Icon(
                    Icons.delete,
                  ),
                  onPressed: () {
                    dbHelper.deleteOrdersales(rec.kdbrg).then((count) {
                      if (count > 0) {
                        updateListView();
                      }
                    });
                  }),
            ),
          );
        },
        itemCount: list.length,
      ),
    );
  }

  Widget _buildTextFieldKodeBrg() {
    return TypeAheadField(
      textFieldConfiguration: TextFieldConfiguration(
        autofocus: false,
        controller: _controllerKodeBrg,
        style: TextStyle(
          color: Colors.black54,
          fontSize: 18.0,
        ),
        // enabled: widget.orderdetail == null ? true : false,
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          labelText: "Kode Barang",
          errorText: _isFieldKodeBrgValid == null || _isFieldKodeBrgValid
              ? null
              : "Kode Barang harus diisi!!!",
          labelStyle: TextStyle(color: Colors.black38),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.brown),
          ),
        ),
        onChanged: (value) {
          bool isFieldValid = value.trim().isNotEmpty;
          if (isFieldValid != _isFieldKodeBrgValid) {
            setState(() {
              _isFieldKodeBrgValid = isFieldValid;
              _isFieldNamaBrgValid = isFieldValid;
            });
          }
        },
      ),
      suggestionsCallback: (pattern) async {
        return await apiService.getbarangSuggestions(pattern);
      },
      itemBuilder: (context, suggestion) {
        return ListTile(
          // leading: Icon(Icons.check_box),
          title: Text(suggestion['nama']),
          subtitle: Text(
              'Kode : ${suggestion['kd_brg']}\nNama : ${suggestion['nama']}\nKelompok : ${suggestion['kelompok']}'),
        );
      },
      onSuggestionSelected: (suggestion) {
        _controllerKodeBrg.text = suggestion['kd_brg'];
        _controllerNamaBrg.text = suggestion['nama'];
        // Navigator.of(context).push(MaterialPageRoute(
        //     builder: (context) => ProductPage(product: suggestion)));
      },
    );
  }

  Widget _buildTextFieldQtyBrg() {
    return TextField(
      controller: _controllerQtyBrg,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        labelText: "Qty Order",
        errorText: _isFieldQtyBrgValid == null || _isFieldQtyBrgValid
            ? null
            : "Qty Order harus diisi!!!",
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldQtyBrgValid) {
          setState(() => _isFieldQtyBrgValid = isFieldValid);
        }
      },
    );
  }

  Widget _buildSubmitOrder() {
    return FlatButton(
        child: Column(
          children: <Widget>[
            Icon(
              Icons.add_circle,
              color: Colors.brown,
              size: 40,
            ),
          ],
        ),
        onPressed: () {
          OrderSalesDetailModel data = OrderSalesDetailModel(
            kdbrg: _controllerKodeBrg.text.toString(),
            namabrg: _controllerNamaBrg.text.toString(),
            qtyorder: _controllerQtyBrg.text.toString().trim().isNotEmpty
                ? int.parse(_controllerQtyBrg.text.toString())
                : 0,
            harga: 0,
          );
          addOrder(data);
          // Navigator.pushNamed(context, OrderSalesDetailPageForm.tag);
        });
  }

  Widget _buildSubmit() {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: RaisedButton(
          color: Colors.brown,
          child: Text(
            'SIMPAN',
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          onPressed: () {
            OrderSalesDetailModel data = OrderSalesDetailModel(
              kdbrg: _controllerKodeBrg.text.toString(),
              namabrg: _controllerNamaBrg.text.toString(),
              qtyorder: _controllerQtyBrg.text.toString().trim().isNotEmpty
                  ? int.parse(_controllerQtyBrg.text.toString())
                  : 0,
              harga: 0,
            );
            addOrder(data);
            // Navigator.pushNamed(context, OrderSalesDetailPageForm.tag);
          }),
    );
  }

  //buat order
  void addOrder(OrderSalesDetailModel object) async {
    int result = await dbHelper.insertOrdersales(object);
    if (result > 0) {
      updateListView();
    }
  }

  //edit order
  void editOrder(OrderSalesDetailModel object) async {
    int result = await dbHelper.updateOrdersales(object);
    if (result > 0) {
      updateListView();
    }
  }

  //delete order
  void deleteOrder(OrderSalesDetailModel object) async {
    int result = await dbHelper.deleteOrdersales(object.kdbrg);
    if (result > 0) {
      updateListView();
    }
  }

  //update contact
  void updateListView() {
    final Future<Database> dbFuture = dbHelper.initDb();
    dbFuture.then((database) {
      Future<List<OrderSalesDetailModel>> contactListFuture =
          dbHelper.getOrderSalesList();
      contactListFuture.then((data) {
        setState(() {
          _buildListView(data);
          _totalitem = 0;
          _totalqty = 0;
          _totalharga = 0;
          for (var i = 0; i < data.length; i++) {
            _totalitem += 1;
            _totalqty += data[i].qtyorder;
            _totalharga += (data[i].qtyorder * data[i].harga);
          }
        });
      });
    });
  }
}
