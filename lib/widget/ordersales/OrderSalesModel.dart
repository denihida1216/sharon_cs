import 'dart:convert';

class OrderSalesModel {
  String id;
  String imageurl;
  String tanggal;
  String nama;
  String kdplg1;
  String kdplg2;
  String alamat;
  String kdsales;
  String namasales;
  String kdjalur;
  String namajalur;
  String latitude;
  String longitude;
  String address;
  String totalitem;
  String totalqty;
  String totalharga;
  String catatan;
  String uid;
  String user;

  OrderSalesModel({
    this.id,
    this.imageurl,
    this.tanggal,
    this.nama,
    this.kdplg1,
    this.kdplg2,
    this.alamat,
    this.kdsales,
    this.namasales,
    this.kdjalur,
    this.namajalur,
    this.latitude,
    this.longitude,
    this.address,
    this.totalitem,
    this.totalqty,
    this.totalharga,
    this.catatan,
    this.uid,
    this.user,
  });

  factory OrderSalesModel.fromJson(Map<String, dynamic> map) {
    return OrderSalesModel(
      id: map["id"],
      imageurl: map["image_url"],
      tanggal: map["tanggal"],
      nama: map["nama"],
      kdplg1: map["kd_plg"],
      kdplg2: map["jp"],
      alamat: map["alamat"],
      kdsales: map["kd_sales"],
      namasales: map["nama_sales"],
      kdjalur: map["kd_jalur"],
      namajalur: map["nama_jalur"],
      latitude: map["latitude"],
      longitude: map["longitude"],
      address: map["address"],
      totalitem: map["total_item"],
      totalqty: map["total_qty"],
      totalharga: map["total_harga"],
      catatan: map["catatan"],
      uid: map["uid"],
      user: map["user"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "image_url": imageurl,
      "tanggal": tanggal,
      "nama": nama,
      "kd_plg": kdplg1,
      "jp": kdplg2,
      "alamat": alamat,
      "kd_sales": kdsales,
      "nama_sales": namasales,
      "kd_jalur": kdjalur,
      "nama_jalur": namajalur,
      "latitude": latitude,
      "longitude": longitude,
      "address": address,
      "total_item": totalitem,
      "total_qty": totalqty,
      "total_harga": totalharga,
      "catatan": catatan,
      "uid": uid,
      "user": user,
    };
  }

  @override
  String toString() {
    return 'OrderSalesModel{id: $id, ' +
        'image_url: $imageurl, ' +
        'tanggal: $tanggal, ' +
        'nama: $nama, ' +
        'kd_plg: $kdplg1, ' +
        'jp: $kdplg2, ' +
        'alamat: $alamat, ' +
        'kd_sales: $kdsales, ' +
        'nama_sales: $namasales, ' +
        'kd_jalur: $kdjalur, ' +
        'nama_jalur: $namajalur, ' +
        'latitude: $latitude, ' +
        'longitude: $longitude, ' +
        'address: $address, ' +
        'total_item: $totalitem, ' +
        'total_qty: $totalqty, ' +
        'total_harga: $totalharga, ' +
        'catatan: $catatan, ' +
        'uid: $uid, ' +
        'user: $user}';
  }
}

List<OrderSalesModel> orderSalesModelFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<OrderSalesModel>.from(
      data.map((item) => OrderSalesModel.fromJson(item)));
}

String orderSalesModelToJson(OrderSalesModel data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}
