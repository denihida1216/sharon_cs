import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

void main() {
  runApp(new MaterialApp(
    title: "Lokasi",
    home: new Page4(),
  ));
}

class Page4 extends StatefulWidget {
  @override
  _Page4State createState() => _Page4State();
}

class _Page4State extends State<Page4> {
  static String tag = 'Page4-page';
  
  final Set<Marker> _markers = {};
  final LatLng _currentPosition = LatLng(-7.064166, 107.746914);

  @override
  void initState() {
    _markers.add(
      Marker(
        markerId: MarkerId("-7.064166, 107.746914"),
        position: _currentPosition,
        icon: BitmapDescriptor.defaultMarker,
        infoWindow: InfoWindow(title: 'PT. Multi Start Rukun Abadi'),
        onTap:(){},
      ),
    );
    
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: new Text("Lokasi"),
        backgroundColor: Colors.brown,
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.search),
              onPressed: () {
                // signOut();
              }),
        ],
      ),
      body: GoogleMap(
        mapType: MapType.normal,
        initialCameraPosition: CameraPosition(
          target: _currentPosition,
          zoom: 14.0,
        ),
        markers: _markers,
      ),
    );
  }
}
