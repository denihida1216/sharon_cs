import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sharon_cs/widget/home/page1.dart';
import 'package:sharon_cs/widget/home/page2.dart';
import 'package:sharon_cs/widget/users/loginpage.dart';

class HomePage extends StatefulWidget {
  static String tag = 'HomePage-page';
  
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State {
  int _selectedIndex = 0;

  final _widgetOptions = [
    Page1(),
    Page2(),
    // Page3(),
    // Page4(),
  ];

  @override
  void initState() {
    super.initState();
    getPref();
  }

  signOut() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      preferences.setInt("value", null);
      // preferences.commit();
    });
    Navigator.of(context).pushNamedAndRemoveUntil(
        LoginPage.tag, (Route<dynamic> route) => false);
  }

  var value;
  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      value = preferences.getString("akses");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _widgetOptions.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.store),
            title: Text('Cekstore'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_cart),
            title: Text('Order Sales'),
          ),
          // BottomNavigationBarItem(
          //   icon: Icon(Icons.view_list),
          //   title: Text('Jadwal'),
          // ),
          // BottomNavigationBarItem(
          //   icon: Icon(Icons.location_on),
          //   title: Text('Lokasi'),
          // ),
        ],
        type: BottomNavigationBarType.fixed,
        currentIndex: _selectedIndex,
        fixedColor: Colors.brown,
        onTap: _onItemTapped,
      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
}
