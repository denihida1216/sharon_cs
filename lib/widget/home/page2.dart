import 'package:flutter/material.dart';
import 'package:sharon_cs/widget/home/page2jwk.dart';
import 'package:sharon_cs/widget/home/page2order.dart';
import 'package:sharon_cs/widget/home/page2orderhistory.dart';

void main(){
  runApp(
    new MaterialApp(
      title: "Order Sales",
      home: new Page2(),
    )
  );
}


class Page2 extends StatefulWidget {
  @override
  _Page2State createState() => _Page2State();
}

class _Page2State extends State<Page2> with SingleTickerProviderStateMixin{

  TabController _tabController;

  @override
  void initState(){
    _tabController = new TabController(length: 3, vsync: this);
    super.initState();
  }
  
  @override
  void dispose(){
    _tabController.dispose();
    super.dispose();
  }

// signOut() async {
//     SharedPreferences preferences = await SharedPreferences.getInstance();
//     setState(() {
//       preferences.setInt("value", null);
//       preferences.commit();
//     });
//     Navigator.of(context).pushNamedAndRemoveUntil(
//         LoginPage.tag, (Route<dynamic> route) => false);
//   }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: new Text("Order Sales"),
        backgroundColor: Colors.brown,
        bottom: _buildTabBar(),
        // actions: [
        //   IconButton(
        //       icon: Icon(Icons.exit_to_app),
        //       onPressed: () {
        //         // signOut();
        //       }),
        // ],
      ),
      body: TabBarView(
        controller: _tabController,
        children: <Widget>[
          Page2Jwk(),
          Page2Order(),
          Page2OrderHistory(),
        ],
      ),
    );
  }

  TabBar _buildTabBar(){
    return TabBar(
      controller: _tabController,
      labelColor: Colors.white,
      indicatorColor: Colors.brown,
      tabs: <Widget>[
        Tab(
          // icon: Icon(Icons.restaurant_menu),
          text: "Jwk",
        ),Tab(
          // icon: Icon(Icons.restaurant_menu),
          text: "Order",
        ),
        Tab(
          // icon: Icon(Icons.dock),
          text: "History",
        ),
      ],
      );
  }
}
// final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

// void main() {
//   runApp(new MaterialApp(
//     title: "Order Sales",
//     home: new Page2(),
//   ));
// }

// class Page2 extends StatefulWidget {
//   static String tag = 'page2-page';
//   // UsersModel usersModel;
//   // Page2({this.usersModel});

//   @override
//   _Page2State createState() => _Page2State();
// }

// class _Page2State extends State<Page2> {
//   BuildContext context;
//   ApiService apiService;
//   bool _isFieldTg1Valid;
//   bool _isFieldTg2Valid;
//   TextEditingController controller = new TextEditingController();
//   TextEditingController _controllertg1 = new TextEditingController();
//   TextEditingController _controllertg2 = new TextEditingController();
//   String filter;
//   String tg1 = '';
//   String tg2 = '';
//   String kdjalur = '';
//   String namajalur = '';
//   List<OrderSalesModel> ordersales = List();
//   DbHelper dbHelper = DbHelper();
//   int count = 0;
//   double _total = 0;
//   int _totaltoko = 0;
//   var noSimbolInUSFormat = NumberFormat.currency(locale: "en_US", symbol: "");

//   // CustomPopupMenu _selectedChoices = choices[0];
//   // void _select(CustomPopupMenu choice) {
//   //   setState(() {
//   //      _selectedChoices = choice;
//   //   });
//   // }

//   DateTime selectedDate1 = DateTime.now();
//   Future<Null> _selectDate1(BuildContext context) async {
//     final DateTime picked = await showDatePicker(
//       context: context,
//       initialDate: selectedDate1,
//       firstDate: DateTime(2015, 8),
//       lastDate: DateTime(2101),
//       builder: (BuildContext context, Widget child) {
//         return Theme(
//           data:
//               ThemeData(primarySwatch: Colors.brown, splashColor: Colors.brown),
//           child: child,
//         );
//       },
//     );
//     if (picked != null && picked != selectedDate1)
//       setState(() {
//         selectedDate1 = picked;
//         _controllertg1.text = "${selectedDate1.toLocal()}".split(' ')[0];
//       });
//   }

//   DateTime selectedDate2 = DateTime.now();
//   Future<Null> _selectDate2(BuildContext context) async {
//     final DateTime picked = await showDatePicker(
//       context: context,
//       initialDate: selectedDate2,
//       firstDate: DateTime(2015, 8),
//       lastDate: DateTime(2101),
//       builder: (BuildContext context, Widget child) {
//         return Theme(
//           data:
//               ThemeData(primarySwatch: Colors.brown, splashColor: Colors.brown),
//           child: child,
//         );
//       },
//     );
//     if (picked != null && picked != selectedDate2)
//       setState(() {
//         selectedDate2 = picked;
//         _controllertg2.text = "${selectedDate2.toLocal()}".split(' ')[0];
//       });
//   }

//   var uid;
//   var user;
//   var nama;
//   getPref() async {
//     SharedPreferences preferences = await SharedPreferences.getInstance();
//     setState(() {
//       uid = preferences.getString("id");
//       user = preferences.getString("user");
//       nama = preferences.getString("nama");
//       // kdjalur = preferences.getString("kd_jalur");
//       // namajalur = preferences.getString("nama_jalur");
//     });
//   }

//   @override
//   void initState() {
//     apiService = ApiService();
//     getPref();
//     controller.addListener(() {
//       setState(() {
//         filter = controller.text;
//       });
//     });
//     _controllertg1.text = "${selectedDate1.toLocal()}".split(' ')[0];
//     _controllertg2.text = "${selectedDate2.toLocal()}".split(' ')[0];
//     super.initState();
//   }

//   @override
//   void dispose() {
//     controller.dispose();
//     super.dispose();
//   }

//   signOut() async {
//     SharedPreferences preferences = await SharedPreferences.getInstance();
//     setState(() {
//       preferences.setInt("value", null);
//       // preferences.commit();
//     });
//     Navigator.of(context).pushNamedAndRemoveUntil(
//         LoginPage.tag, (Route<dynamic> route) => false);
//   }

//   pagelist(String value) async {
//     SharedPreferences preferences = await SharedPreferences.getInstance();
//     setState(() {
//       preferences.setString("title", value);
//       // preferences.commit();
//     });
//     Navigator.pushNamed(context, CsPage.tag);
//   }

//   @override
//   Widget build(BuildContext context) {
//     this.context = context;
//     apiService.getDashboard(uid).then((dashboard) {
//       for (var i = 0; i < dashboard.length; i++) {
//         kdjalur = dashboard[i].kdjalur;
//         namajalur = dashboard[i].namajalur;
//       }
//     });
//     return Scaffold(
//       key: _scaffoldState,
//       appBar: AppBar(
//         title: new Text("Order Sales"),
//         backgroundColor: Colors.brown,
//         actions: [
//           IconButton(
//               icon: Icon(Icons.filter_list),
//               onPressed: () {
//                 showDialog(
//                     context: context,
//                     builder: (BuildContext context) {
//                       return AlertDialog(
//                         content: Form(
//                           // key: _formKey,
//                           child: Theme(
//                             data: ThemeData(
//                               primaryColor: Colors.brown,
//                               accentColor: Colors.brown,
//                               cursorColor: Colors.brown,
//                             ),
//                             child: Column(
//                               mainAxisSize: MainAxisSize.min,
//                               children: <Widget>[
//                                 Text('Pencarian'),
//                                 Padding(
//                                   padding: EdgeInsets.all(8.0),
//                                   child: _buildTextFieldTg1(),
//                                 ),
//                                 Padding(
//                                   padding: EdgeInsets.all(8.0),
//                                   child: _buildTextFieldTg2(),
//                                 ),
//                                 Padding(
//                                   padding: const EdgeInsets.all(8.0),
//                                   child: RaisedButton(
//                                     textColor: Colors.white,
//                                     child: Text("Filter"),
//                                     color: Colors.brown,
//                                     onPressed: () {
//                                       setState(() {
//                                         tg1 = _controllertg1.text.toString();
//                                         tg2 = _controllertg2.text.toString();
//                                       });
//                                       // Navigator.pop(context);
//                                       Navigator.pop(
//                                           _scaffoldState.currentState.context);
//                                       // if (_formKey.currentState.validate()) {
//                                       //   _formKey.currentState.save();
//                                       // }
//                                     },
//                                   ),
//                                 )
//                               ],
//                             ),
//                           ),
//                         ),
//                       );
//                     });
//               }),
//         ],
//       ),
//       body: ListView(
//         physics: const ClampingScrollPhysics(),
//         shrinkWrap: true,
//         children: <Widget>[
//           Card(
//             child: Column(
//               mainAxisAlignment: MainAxisAlignment.start,
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: <Widget>[
//                 ListTile(
//                   onTap: () {
//                     setState(() {
//                       kdjalur = kdjalur;
//                       namajalur = namajalur;
//                       _total = _total;
//                       _totaltoko = _totaltoko;
//                     });
//                   },
//                   leading: Icon(
//                     Icons.refresh,
//                     color: Colors.brown,
//                     size: 30,
//                   ),
//                   trailing: new Text(
//                     _total != null
//                         ? 'Rp. ${noSimbolInUSFormat.format(double.parse(_total.toString()))}'
//                         : 'Rp. 0',
//                     // rec.totaldikunjungi,
//                     style: TextStyle(
//                       color: Colors.black,
//                       fontWeight: FontWeight.w700,
//                       fontSize: 20.0,
//                     ),
//                   ),
//                 ),
//                 ListTile(
//                   subtitle: Text(
//                   ('Sales : ${nama}' +
//                       '\nTanggal : ' +
//                       _controllertg1.text.toString() +
//                       ' s/d ' +
//                       _controllertg2.text.toString() +
//                       '\nTotal : ${_totaltoko.toString()} Toko  ' +
//                       '\nJalur : ${kdjalur} - ${namajalur}'),
//                   style: TextStyle(
//                     color: Colors.black,
//                     fontWeight: FontWeight.w700,
//                     fontSize: 16.0,
//                   ),
//                 ),
//                 ),                
//               ],
//             ),
//           ),
//           FutureBuilder(
//             future: apiService.getOrderSales(uid, tg1, tg2),
//             builder: (BuildContext context,
//                 AsyncSnapshot<List<OrderSalesModel>> snapshot) {
//               if (snapshot.hasError) {
//                 return Center(
//                   child: Text(
//                       "Something wrong with message: ${snapshot.error.toString()}"),
//                 );
//               } else if (snapshot.connectionState == ConnectionState.done) {
//                 List<OrderSalesModel> data = snapshot.data;
//                 double total = 0;
//                 for (var i = 0; i < data.length; i++) {
//                   total += double.parse(data[i].totalharga);
//                   // print('satuan='+data[i].totalharga.toString()+' total='+total.toString());
//                 }
//                 _total = total;
//                 _totaltoko = data.length;
//                 return _buildListView(data);
//               } else {
//                 return Center(
//                   child: CircularProgressIndicator(),
//                 );
//               }
//             },
//           ),
//         ],
//       ),
//       floatingActionButton: FloatingActionButton(
//         onPressed: () {
//           Navigator.pushNamed(context, OrderSalesPageForm.tag);
//         },
//         tooltip: 'Add Order',
//         child: Icon(Icons.add_shopping_cart),
//         backgroundColor: Colors.brown,
//       ),
//     );
//   }

//   Widget _buildListView(List<OrderSalesModel> list) {
//     return Container(
//       height: 400,
//       child: ListView.builder(
//         shrinkWrap: true,
//         itemBuilder: (context, index) {
//           OrderSalesModel rec = list[index];
//           return Card(
//             child: ListTile(
//               onTap: () {
//                 Navigator.push(context, MaterialPageRoute(builder: (context) {
//                   return OrderSalesPageForm(ordersales: rec);
//                 }));
//               },
//               leading: CircleAvatar(
//                 backgroundColor: Colors.brown,
//                 child: Icon(
//                   Icons.shopping_cart,
//                   color: Colors.white,
//                 ),
//               ),
//               // CircleAvatar(
//               //   backgroundColor: Colors.transparent,
//               //   // backgroundImage: rec.imageurl == ''
//               //   //     ? AssetImage('assets/sharon.jpg')
//               //   //     : NetworkImage(apiService.baseImageUrl + rec.imageurl),
//               //   backgroundImage: AssetImage('assets/sharon.jpg'),
//               // ),
//               title: Text(
//                 rec.nama,
//               ),
//               subtitle: Text('Tanggal : ' +
//                   rec.tanggal +
//                   '\nJalur : ' +
//                   rec.kdjalur +
//                   ' - ' +
//                   rec.namajalur +
//                   '\nNama : ' +
//                   rec.nama +
//                   '\nAlamat : ' +
//                   rec.alamat +
//                   '\nTotal Item : ' +
//                   rec.totalitem +
//                   '\nTotal Qty : ${rec.totalqty.toString()}' +
//                   '\nTotal Harga : Rp. ${noSimbolInUSFormat.format(double.parse(rec.totalharga.toString()))}'),
//             ),
//             //   trailing: IconButton(
//             //       icon: Icon(
//             //         Icons.delete,
//             //       ),
//             //       onPressed: () {
//             //         dbHelper.deleteOrdersales(rec.id).then((count) {
//             //           if (count > 0) {
//             //             updateListView();
//             //           }
//             //         });
//             //       }),
//             // ),
//           );
//           // Container(
//           //   padding: EdgeInsets.all(4),
//           //   child: Row(
//           //     children: <Widget>[
//           //       Container(
//           //         width: 100.0,
//           //         height: 150.0,
//           //         color: Colors.brown,
//           //         child: new Icon(
//           //           Icons.store,
//           //           color: Colors.white,
//           //           size: 50.0,
//           //         ),
//           //       ),
//           //       Expanded(
//           //         child: Container(
//           //           padding: EdgeInsets.all(8.0),
//           //           height: 150.0,
//           //           color: Colors.brown[200],
//           //           child: Column(
//           //             crossAxisAlignment: CrossAxisAlignment.start,
//           //             children: <Widget>[
//           //               Text('Tanggal : ' + rec.tanggal,
//           //                   style:
//           //                       TextStyle(color: Colors.white, fontSize: 15.0)),
//           //               Text('Jalur : ' + rec.kdjalur + ' - ' + rec.namajalur,
//           //                   style: TextStyle(color: Colors.white)),
//           //               Text('Nama : ' + rec.nama,
//           //                   style: TextStyle(color: Colors.white)),
//           //               Text('Alamat : ' + rec.alamat,
//           //                   style: TextStyle(color: Colors.white)),
//           //               Text('Total Item : ' + rec.totalitem,
//           //                   style: TextStyle(color: Colors.white)),
//           //               Text('Total Qty : ' + rec.totalqty,
//           //                   style: TextStyle(color: Colors.white)),
//           //               Text('Total Harga : ' + rec.totalharga,
//           //                   style: TextStyle(color: Colors.white)),
//           //             ],
//           //           ),
//           //         ),
//           //       )
//           //     ],
//           //   ),
//           // );
//         },
//         itemCount: list.length,
//       ),
//     );
//   }

//   Widget _buildTextFieldTg1() {
//     return TextField(
//       controller: _controllertg1,
//       keyboardType: TextInputType.text,
//       decoration: InputDecoration(
//         labelText: "Tanggal Awal",
//         errorText: _isFieldTg1Valid == null || _isFieldTg1Valid
//             ? null
//             : "Tanggal harus diisi!!!",
//       ),
//       onChanged: (value) {
//         bool isFieldValid = value.trim().isNotEmpty;
//         if (isFieldValid != _isFieldTg1Valid) {
//           setState(() => _isFieldTg1Valid = isFieldValid);
//         }
//       },
//       onTap: () {
//         _selectDate1(context);
//       },
//     );
//   }

//   Widget _buildTextFieldTg2() {
//     return TextField(
//       controller: _controllertg2,
//       keyboardType: TextInputType.text,
//       decoration: InputDecoration(
//         labelText: "Tanggal Akhir",
//         errorText: _isFieldTg2Valid == null || _isFieldTg2Valid
//             ? null
//             : "Tanggal Akhir harus diisi!!!",
//       ),
//       onChanged: (value) {
//         bool isFieldValid = value.trim().isNotEmpty;
//         if (isFieldValid != _isFieldTg2Valid) {
//           setState(() => _isFieldTg2Valid = isFieldValid);
//         }
//       },
//       onTap: () {
//         _selectDate2(context);
//       },
//     );
//   }
// }
