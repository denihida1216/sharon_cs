import 'package:flutter/material.dart';
import 'package:sharon_cs/services/ApiService.dart';
import 'package:sharon_cs/widget/agenda/AgendaModel.dart';

void main() {
  runApp(new MaterialApp(
    title: "List",
    home: new Page3(),
  ));
}

class Page3 extends StatefulWidget {
  static String tag = 'Page3-page';

  @override
  _Page3State createState() => _Page3State();
}

class _Page3State extends State<Page3> {
  BuildContext context;
  ApiService apiService;

  @override
  void initState() {
    super.initState();
    apiService = ApiService();
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    return Scaffold(
      appBar: AppBar(
        title: new Text("List"),
        backgroundColor: Colors.brown,
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.search),
              onPressed: () {
                // signOut();
              }),
          IconButton(
              icon: Icon(Icons.filter_list),
              onPressed: () {
                // signOut();
              }),
        ],
      ),
      body: FutureBuilder(
        future: apiService.getAgenda(),
        builder:
            (BuildContext context, AsyncSnapshot<List<AgendaModel>> snapshot) {
          if (snapshot.hasError) {
            return Center(
              child: Text(
                  "Something wrong with message: ${snapshot.error.toString()}"),
            );
          } else if (snapshot.connectionState == ConnectionState.done) {
            List<AgendaModel> data = snapshot.data;
            return _buildListView(data);
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }

  Widget _buildListView(List<AgendaModel> list) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 8.0),
      child: ListView.builder(
        itemBuilder: (context, index) {
          AgendaModel rec = list[index];
          return Container(
            padding: EdgeInsets.all(10),
            child: Row(
              children: <Widget>[
                Container(
                  width: 100.0,
                  height: 100.0,
                  color: Colors.brown,
                  child: new Icon(
                    Icons.store,
                    color: Colors.white,
                    size: 50.0,
                  ),
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.all(15.0),
                    height: 100.0,
                    color: Colors.brown[200],
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('User : ' + rec.nama,
                            style: TextStyle(color: Colors.white)),
                        Text('Tanggal : ' + rec.tanggal,
                            style:
                                TextStyle(color: Colors.white, fontSize: 15.0)),
                        Text('Jalur : ' + rec.kdjalur + ' - ' + rec.namajalur,
                            style: TextStyle(color: Colors.white)),
                      ],
                    ),
                  ),
                )
              ],
            ),
          );
        },
        itemCount: list.length,
      ),
    );
  }
}
