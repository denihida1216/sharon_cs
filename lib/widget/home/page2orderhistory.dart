import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sharon_cs/services/ApiService.dart';
import 'package:sharon_cs/widget/ordersales/OrderSalesDetailModel.dart';
import 'package:sharon_cs/widget/ordersales/OrderSalesModel.dart';
import 'package:sharon_cs/widget/ordersales/OrderSalesPageForm.dart';

final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

class Page2OrderHistory extends StatefulWidget {
  static String tag = 'page2orderhistory-page';

  @override
  _Page2OrderHistoryState createState() => _Page2OrderHistoryState();
}

class _Page2OrderHistoryState extends State<Page2OrderHistory> {
  BuildContext context;
  ApiService apiService;
  int count = 0;
  double _total = 0;
  int _totaltoko = 0;
  bool _isFieldNamaValid;
  bool _isFieldTg1Valid;
  bool _isFieldTg2Valid;
  TextEditingController _controllerKode = TextEditingController();
  TextEditingController _controllerNama = TextEditingController();
  TextEditingController _controllertg1 = TextEditingController();
  TextEditingController _controllertg2 = TextEditingController();
  var noSimbolInUSFormat = NumberFormat.currency(locale: "en_US", symbol: "");
  String tg1 = '';
  String tg2 = '';
  String kdjalur = '';
  String namajalur = '';
  String filter = '';

  DateTime selectedDate1 = DateTime.now();
  Future<Null> _selectDate1(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate1,
      firstDate: DateTime(2015, 8),
      lastDate: DateTime(2101),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data:
              ThemeData(primarySwatch: Colors.brown, splashColor: Colors.brown),
          child: child,
        );
      },
    );
    if (picked != null && picked != selectedDate1)
      setState(() {
        selectedDate1 = picked;
        _controllertg1.text = "${selectedDate1.toLocal()}".split(' ')[0];
      });
  }

  DateTime selectedDate2 = DateTime.now();
  Future<Null> _selectDate2(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate2,
      firstDate: DateTime(2015, 8),
      lastDate: DateTime(2101),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data:
              ThemeData(primarySwatch: Colors.brown, splashColor: Colors.brown),
          child: child,
        );
      },
    );
    if (picked != null && picked != selectedDate2)
      setState(() {
        selectedDate2 = picked;
        _controllertg2.text = "${selectedDate2.toLocal()}".split(' ')[0];
      });
  }

  @override
  void initState() {
    super.initState();
    apiService = ApiService();
    getPref();
  }

  var uid;
  var user;
  var nama;
  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      uid = preferences.getString("id");
      user = preferences.getString("user");
      nama = preferences.getString("nama");
      // kdjalur = preferences.getString("kd_jalur");
      // namajalur = preferences.getString("nama_jalur");
    });
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    return Scaffold(
      key: _scaffoldState,
      body: Theme(
        data: ThemeData(
          primaryColor: Colors.brown,
          accentColor: Colors.brown,
          cursorColor: Colors.brown,
        ),
        child: ListView(
          physics: const ClampingScrollPhysics(),
          shrinkWrap: true,
          children: <Widget>[
            Card(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  _buildTextFieldNama(),
                  Row(
                    children: <Widget>[
                      Flexible(
                          child: Row(
                        children: <Widget>[
                          Container(
                            width: 90.0,
                            child: _buildTextFieldTg1(),
                          ),
                          Container(
                            width: 10.0,
                          ),
                          Container(
                            width: 90.0,
                            child: _buildTextFieldTg2(),
                          ),
                          Container(
                            width: 10.0,
                          ),
                          Container(
                            child: _buildSubmit(),
                          ),
                          //container
                        ],
                      ))
                    ],
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  ),
                ],
              ),
            ),
            FutureBuilder(
              future: apiService.getOrderSales(uid, filter, tg1, tg2),
              builder: (BuildContext context,
                  AsyncSnapshot<List<OrderSalesModel>> snapshot) {
                if (snapshot.hasError) {
                  return Center(
                    child: Text(
                        "Something wrong with message: ${snapshot.error.toString()}"),
                  );
                } else if (snapshot.connectionState == ConnectionState.done) {
                  List<OrderSalesModel> data = snapshot.data;
                  // double total = 0;
                  print(data);
                  // for (var i = 0; i < data.length; i++) {
                  //   total += double.parse(data[i].totalharga);
                  //   // print('satuan='+data[i].totalharga.toString()+' total='+total.toString());
                  // }
                  // _total = total;
                  // _totaltoko = data.length;
                  return _buildListView(data);
                } else {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
              },
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, OrderSalesPageForm.tag);
        },
        tooltip: 'Add Order',
        child: Icon(Icons.add_shopping_cart),
        backgroundColor: Colors.brown,
      ),
    );
  }

  Widget _buildListView(List<OrderSalesModel> list) {
    return Container(
      height: 400,
      child: ListView.builder(
        shrinkWrap: true,
        itemBuilder: (context, index) {
          OrderSalesModel rec = list[index];
          return Card(
            child: ListTile(
              onTap: () {
                _displayDialog(context, rec);
                // Navigator.push(context, MaterialPageRoute(builder: (context) {
                //   return OrderSalesPageForm(ordersales: rec);
                // }));
              },
              leading: CircleAvatar(
                backgroundColor: Colors.brown,
                child: Icon(
                  Icons.shopping_cart,
                  color: Colors.white,
                ),
              ),
              // CircleAvatar(
              //   backgroundColor: Colors.transparent,
              //   // backgroundImage: rec.imageurl == ''
              //   //     ? AssetImage('assets/sharon.jpg')
              //   //     : NetworkImage(apiService.baseImageUrl + rec.imageurl),
              //   backgroundImage: AssetImage('assets/sharon.jpg'),
              // ),
              title: Text(
                rec.nama,
              ),
              subtitle: Text('Tanggal : ' +
                  rec.tanggal +
                  '\nJalur : ' +
                  rec.kdjalur +
                  ' - ' +
                  rec.namajalur +
                  '\nNama : ' +
                  rec.nama +
                  '\nAlamat : ' +
                  rec.alamat +
                  '\nTotal Item : ' +
                  rec.totalitem +
                  '\nTotal Qty : ${rec.totalqty.toString()}' +
                  '\nTotal Harga : Rp. ${noSimbolInUSFormat.format(double.parse(rec.totalharga.toString()))}'),
            ),
            //   trailing: IconButton(
            //       icon: Icon(
            //         Icons.delete,
            //       ),
            //       onPressed: () {
            //         dbHelper.deleteOrdersales(rec.id).then((count) {
            //           if (count > 0) {
            //             updateListView();
            //           }
            //         });
            //       }),
            // ),
          );
          // Container(
          //   padding: EdgeInsets.all(4),
          //   child: Row(
          //     children: <Widget>[
          //       Container(
          //         width: 100.0,
          //         height: 150.0,
          //         color: Colors.brown,
          //         child: new Icon(
          //           Icons.store,
          //           color: Colors.white,
          //           size: 50.0,
          //         ),
          //       ),
          //       Expanded(
          //         child: Container(
          //           padding: EdgeInsets.all(8.0),
          //           height: 150.0,
          //           color: Colors.brown[200],
          //           child: Column(
          //             crossAxisAlignment: CrossAxisAlignment.start,
          //             children: <Widget>[
          //               Text('Tanggal : ' + rec.tanggal,
          //                   style:
          //                       TextStyle(color: Colors.white, fontSize: 15.0)),
          //               Text('Jalur : ' + rec.kdjalur + ' - ' + rec.namajalur,
          //                   style: TextStyle(color: Colors.white)),
          //               Text('Nama : ' + rec.nama,
          //                   style: TextStyle(color: Colors.white)),
          //               Text('Alamat : ' + rec.alamat,
          //                   style: TextStyle(color: Colors.white)),
          //               Text('Total Item : ' + rec.totalitem,
          //                   style: TextStyle(color: Colors.white)),
          //               Text('Total Qty : ' + rec.totalqty,
          //                   style: TextStyle(color: Colors.white)),
          //               Text('Total Harga : ' + rec.totalharga,
          //                   style: TextStyle(color: Colors.white)),
          //             ],
          //           ),
          //         ),
          //       )
          //     ],
          //   ),
          // );
        },
        itemCount: list.length,
      ),
    );
  }

  Widget _buildTextFieldNama() {
    return TypeAheadField(
      textFieldConfiguration: TextFieldConfiguration(
        autofocus: false,
        controller: _controllerNama,
        style: TextStyle(
          color: Colors.black54,
          fontSize: 18.0,
        ),
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          labelText: "Nama Pelanggan",
          errorText: _isFieldNamaValid == null || _isFieldNamaValid
              ? null
              : "Nama harus diisi!!!",
          labelStyle: TextStyle(color: Colors.black38),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.brown),
          ),
        ),
        onChanged: (value) {
          bool isFieldValid = value.trim().isNotEmpty;
          if (isFieldValid != _isFieldNamaValid) {
            setState(() => _isFieldNamaValid = isFieldValid);
          }
        },
      ),
      suggestionsCallback: (pattern) async {
        return await apiService.getpelangganSuggestions(pattern);
      },
      itemBuilder: (context, suggestion) {
        return ListTile(
          // leading: Icon(Icons.check_box),
          title: Text(suggestion['nama']),
          subtitle: Text(
              'Kode internal : ${suggestion['kd_plg']}\nKode external : ${suggestion['jp']}\nJalur : ${suggestion['kd_jalur']} - ${suggestion['nama_jalur']}\nAlamat : ${suggestion['alamat']}'),
        );
      },
      onSuggestionSelected: (suggestion) {
        _controllerNama.text = suggestion['nama'];
        _controllerKode.text = suggestion['kd_plg'];
        // _controllerKode2.text = suggestion['jp'];
        // // _controllerCluster.text = suggestion['cluster'];
        // _controllerAlamat.text = suggestion['alamat'];
        // _controllerKdjalur.text = suggestion['kd_jalur'];
        // _controllerNamajalur.text = suggestion['nama_jalur'];
        // // Navigator.of(context).push(MaterialPageRoute(
        // //     builder: (context) => ProductPage(product: suggestion)));
      },
    );
  }

  Widget _buildTextFieldTg1() {
    return TextField(
      controller: _controllertg1,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        labelText: "Tgl Awal",
        errorText: _isFieldTg1Valid == null || _isFieldTg1Valid
            ? null
            : "Tgl harus diisi!!!",
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldTg1Valid) {
          setState(() => _isFieldTg1Valid = isFieldValid);
        }
      },
      onTap: () {
        _selectDate1(context);
      },
    );
  }

  Widget _buildTextFieldTg2() {
    return TextField(
      controller: _controllertg2,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        labelText: "Tgl Akhir",
        errorText: _isFieldTg2Valid == null || _isFieldTg2Valid
            ? null
            : "Tgl harus diisi!!!",
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldTg2Valid) {
          setState(() => _isFieldTg2Valid = isFieldValid);
        }
      },
      onTap: () {
        _selectDate2(context);
      },
    );
  }

  Widget _buildSubmit() {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: RaisedButton(
        onPressed: () {
          if (_isFieldNamaValid == null) {
            setState(() => {
                  _isFieldNamaValid = _controllerNama.text.trim().isNotEmpty,
                });
            // _scaffoldState.currentState.showSnackBar(
            //   SnackBar(
            //     content: Text("Data belum lengkap !!!"),
            //   ),
            // );
            return;
          }
          setState(() {
            filter = _controllerKode.text.toString();
            tg1 = _controllertg1.text.toString();
            tg2 = _controllertg2.text.toString();
            // print(filter);
          });
        },
        child: Text(
          'Cari',
          // widget.orderdetail == null
          //     ? "Submit".toUpperCase()
          //     : "Update Data".toUpperCase(),
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        color: Colors.brown,
      ),
    );
  }

  _displayDialog(BuildContext context, OrderSalesModel data) async {
    return showDialog(
        context: context,
        builder: (context) {
          return Theme(
            data: ThemeData(
              primaryColor: Colors.brown,
              accentColor: Colors.brown,
              cursorColor: Colors.brown,
            ),
            child: AlertDialog(
              title: Text(
                  '${data.nama}\nTanggal: ${data.tanggal}\nTotal Item: ${data.totalitem}, Total Qty: ${data.totalqty}\nTotal Rp.${noSimbolInUSFormat.format(double.parse(data.totalharga))}',
                  style: TextStyle(color: Colors.black, fontSize: 15.0)),
              content: Container(
                width: double.maxFinite,
                height: 300.0,
                child: FutureBuilder(
                  future: apiService.getOrderSalesDetail(data.id),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    if (snapshot.hasError) {
                      return Center(
                        child: Text(
                            "Something wrong with message: ${snapshot.error.toString()}"),
                      );
                    } else if (snapshot.connectionState ==
                        ConnectionState.done) {
                      List<OrderSalesDetailModel> data = snapshot.data;
                      return _buildListViewDetail(data);
                    } else {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    }
                  },
                ),
              ),
              actions: <Widget>[
                FlatButton(
                  color: Colors.brown,
                  child: Text('Kembali'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            ),
          );
        });
  }

  Widget _buildListViewDetail(List<OrderSalesDetailModel> list) {
    return Container(
      height: 300,
      child: ListView.builder(
        shrinkWrap: true,
        itemBuilder: (context, index) {
          OrderSalesDetailModel rec = list[index];
          return Card(
            child: ListTile(
              onTap: () {
                // // print('bismilah' + rec.id);
                // Navigator.push(context, MaterialPageRoute(builder: (context) {
                //   return OrderSalesDetailPageForm(orderdetail: rec);
                // }));
              },
              // leading: CircleAvatar(
              //   backgroundColor: Colors.brown,
              //   child: Icon(
              //     Icons.shopping_cart,
              //     color: Colors.white,
              //   ),
              // ),
              title: Text(
                rec.kdbrg,
              ),
              subtitle: Text(
                '' +
                    rec.namabrg +
                    '\nQty Harga: ${rec.qtyorder.toString()} * ${noSimbolInUSFormat.format(double.parse(rec.harga.toString()))}' +
                    '\nSubtotal: Rp.${noSimbolInUSFormat.format(double.parse((rec.qtyorder * rec.harga).toString()))}',
              ),
            ),
          );
        },
        itemCount: list.length,
      ),
    );
  }
}
