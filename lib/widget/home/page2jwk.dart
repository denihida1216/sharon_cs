import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sharon_cs/helper/DbHelper.dart';
import 'package:sharon_cs/services/ApiService.dart';
import 'package:sharon_cs/widget/barang/BarangModel.dart';
import 'package:sharon_cs/widget/discount/DiscountDetailModel.dart';
import 'package:sharon_cs/widget/pelanggan/PelangganModel.dart';

class Page2Jwk extends StatefulWidget {
  static String tag = 'Page2Jwk-page';

  @override
  _Page2JwkState createState() => _Page2JwkState();
}

class _Page2JwkState extends State<Page2Jwk> {
  BuildContext context;
  ApiService apiService;
  DbHelper dbHelper = DbHelper();

  @override
  void initState() {
    super.initState();
    apiService = ApiService();
    getPref();
  }

  var uid;
  var user;
  var nama;
  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      uid = preferences.getString("id");
      user = preferences.getString("user");
      nama = preferences.getString("nama");
      // kdjalur = preferences.getString("kd_jalur");
      // namajalur = preferences.getString("nama_jalur");
    });
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    return Scaffold(
      body: ListView(
        children: <Widget>[
          _buildPelanggan(),
          _buildBarang(),
          _buildDiscount(),
        ],
      ),
    );
  }

  Widget _buildPelanggan() {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: RaisedButton(
        onPressed: () {
          dbHelper.deleteallPelanggan();
          apiService.getPelanggan('').then((order) {
            print(order);
            int count = order.length;
            for (int i = 0; i < count; i++) {
              PelangganModel data = PelangganModel(
                kdplg: order[i].kdplg,
                jp: order[i].jp,
                kdbaru: order[i].kdbaru,
                nama: order[i].nama,
                alamat: order[i].alamat,
                telp: order[i].telp,
                hp: order[i].hp,
                npwp: order[i].npwp,
                kdplggr: order[i].kdplggr,
                namakdplggr: order[i].namakdplggr,
                kddiscount: order[i].kddiscount,
                kdjalur: order[i].kdjalur,
                namajalur: order[i].namajalur,
                kddivisi: order[i].kddivisi,
                namadivisi: order[i].namadivisi,
                latitude: order[i].latitude,
                longitude: order[i].longitude,
                address: order[i].address,
                imageurl: order[i].imageurl,
              );
              dbHelper.insertPelanggan(data);
            }
          });
          dbHelper.getPelangganList();
        },
        child: Text(
          'PELANGGAN',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        color: Colors.brown,
      ),
    );
  }

  Widget _buildBarang() {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: RaisedButton(
        onPressed: () {
          dbHelper.deleteallBarang();
          apiService.getBarang('').then((order) {
            int count = order.length;
            for (int i = 0; i < count; i++) {
              print(order[i].kdbrg);
              BarangModel data = BarangModel(
                kdbrg: order[i].kdbrg,
                nama: order[i].nama,
                harga: order[i].harga,
              );
              dbHelper.insertBarang(data);
            }
          });
          dbHelper.getBarangList();
        },
        child: Text(
          'BARANG',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        color: Colors.brown,
      ),
    );
  }

  Widget _buildDiscount() {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: RaisedButton(
        onPressed: () {
          dbHelper.deleteallDiscountDetail();
          apiService.getDiscountDetail(uid).then((order) {
            int count = order.length;
            for (int i = 0; i < count; i++) {
              print(order[i].kddiscount);
              DiscountDetailModel data = DiscountDetailModel(
                kddiscount: order[i].kddiscount,
                kdbrg: order[i].kdbrg,
                jenisnilai: order[i].jenisnilai,
                discount: order[i].discount,
              );
              dbHelper.insertDiscountDetail(data);
            }
          });
          dbHelper.getDiscountDetailList();
        },
        child: Text(
          'DISCOUNT',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        color: Colors.brown,
      ),
    );
  }
}
