import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sharon_cs/services/ApiService.dart';
import 'package:sharon_cs/widget/ordersales/OrderSalesModel.dart';
import 'package:sharon_cs/widget/ordersales/OrderSalesPageForm.dart';

class Page2Order extends StatefulWidget {
  static String tag = 'page2order-page';

  @override
  _Page2OrderState createState() => _Page2OrderState();
}

class _Page2OrderState extends State<Page2Order> {
  BuildContext context;
  ApiService apiService;
  int count = 0;
  double _total = 0;
  int _totaltoko = 0;
  var noSimbolInUSFormat = NumberFormat.currency(locale: "en_US", symbol: "");
  String kdjalur = '';
  String namajalur = '';
  String tanggal = '';

  @override
  void initState() {
    super.initState();
    apiService = ApiService();
    getPref();
  }

  var uid;
  var user;
  var nama;
  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      uid = preferences.getString("id");
      user = preferences.getString("user");
      nama = preferences.getString("nama");
      // kdjalur = preferences.getString("kd_jalur");
      // namajalur = preferences.getString("nama_jalur");
    });
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    apiService.getDashboard(uid).then((dashboard) {
      for (var i = 0; i < dashboard.length; i++) {
        kdjalur = dashboard[i].kdjalur;
        namajalur = dashboard[i].namajalur;
        tanggal = dashboard[i].tanggal;
      }
    });
    return Scaffold(
      body: ListView(
        physics: const ClampingScrollPhysics(),
        shrinkWrap: true,
        children: <Widget>[
          Card(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ListTile(
                  onTap: () {
                    setState(() {
                      kdjalur = kdjalur;
                      namajalur = namajalur;
                      _total = _total;
                      _totaltoko = _totaltoko;
                    });
                  },
                  leading: Icon(
                    Icons.refresh,
                    color: Colors.brown,
                    size: 30,
                  ),
                  trailing: new Text(
                    _total != null
                        ? 'Rp. ${noSimbolInUSFormat.format(double.parse(_total.toString()))}'
                        : 'Rp. 0',
                    // rec.totaldikunjungi,
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w700,
                      fontSize: 20.0,
                    ),
                  ),
                ),
                ListTile(
                  subtitle: Text(
                    ('Sales : ${nama}' +
                        '\nTanggal : ${tanggal.toString()}' +
                        '\nTotal : ${_totaltoko.toString()} Toko  ' +
                        '\nJalur : ${kdjalur} - ${namajalur}'),
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w700,
                      fontSize: 16.0,
                    ),
                  ),
                ),
              ],
            ),
          ),
          FutureBuilder(
            future: apiService.getOrderSales(uid, '', '', ''),
            builder: (BuildContext context,
                AsyncSnapshot<List<OrderSalesModel>> snapshot) {
              if (snapshot.hasError) {
                return Center(
                  child: Text(
                      "Something wrong with message: ${snapshot.error.toString()}"),
                );
              } else if (snapshot.connectionState == ConnectionState.done) {
                List<OrderSalesModel> data = snapshot.data;
                double total = 0;
                for (var i = 0; i < data.length; i++) {
                  total += double.parse(data[i].totalharga);
                  // print('satuan='+data[i].totalharga.toString()+' total='+total.toString());
                }
                _total = total;
                _totaltoko = data.length;
                return _buildListView(data);
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            },
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, OrderSalesPageForm.tag);
        },
        tooltip: 'Add Order',
        child: Icon(Icons.add_shopping_cart),
        backgroundColor: Colors.brown,
      ),
    );
  }

  Widget _buildListView(List<OrderSalesModel> list) {
    return Container(
      height: 400,
      child: ListView.builder(
        shrinkWrap: true,
        itemBuilder: (context, index) {
          OrderSalesModel rec = list[index];
          return Card(
            child: ListTile(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return OrderSalesPageForm(ordersales: rec);
                }));
              },
              leading: CircleAvatar(
                backgroundColor: Colors.brown,
                child: Icon(
                  Icons.shopping_cart,
                  color: Colors.white,
                ),
              ),
              // CircleAvatar(
              //   backgroundColor: Colors.transparent,
              //   // backgroundImage: rec.imageurl == ''
              //   //     ? AssetImage('assets/sharon.jpg')
              //   //     : NetworkImage(apiService.baseImageUrl + rec.imageurl),
              //   backgroundImage: AssetImage('assets/sharon.jpg'),
              // ),
              title: Text(
                rec.nama,
              ),
              subtitle: Text('Tanggal : ' +
                  rec.tanggal +
                  '\nJalur : ' +
                  rec.kdjalur +
                  ' - ' +
                  rec.namajalur +
                  '\nNama : ' +
                  rec.nama +
                  '\nAlamat : ' +
                  rec.alamat +
                  '\nTotal Item : ' +
                  rec.totalitem +
                  '\nTotal Qty : ${rec.totalqty.toString()}' +
                  '\nTotal Harga : Rp. ${noSimbolInUSFormat.format(double.parse(rec.totalharga.toString()))}'),
            ),
            //   trailing: IconButton(
            //       icon: Icon(
            //         Icons.delete,
            //       ),
            //       onPressed: () {
            //         dbHelper.deleteOrdersales(rec.id).then((count) {
            //           if (count > 0) {
            //             updateListView();
            //           }
            //         });
            //       }),
            // ),
          );
          // Container(
          //   padding: EdgeInsets.all(4),
          //   child: Row(
          //     children: <Widget>[
          //       Container(
          //         width: 100.0,
          //         height: 150.0,
          //         color: Colors.brown,
          //         child: new Icon(
          //           Icons.store,
          //           color: Colors.white,
          //           size: 50.0,
          //         ),
          //       ),
          //       Expanded(
          //         child: Container(
          //           padding: EdgeInsets.all(8.0),
          //           height: 150.0,
          //           color: Colors.brown[200],
          //           child: Column(
          //             crossAxisAlignment: CrossAxisAlignment.start,
          //             children: <Widget>[
          //               Text('Tanggal : ' + rec.tanggal,
          //                   style:
          //                       TextStyle(color: Colors.white, fontSize: 15.0)),
          //               Text('Jalur : ' + rec.kdjalur + ' - ' + rec.namajalur,
          //                   style: TextStyle(color: Colors.white)),
          //               Text('Nama : ' + rec.nama,
          //                   style: TextStyle(color: Colors.white)),
          //               Text('Alamat : ' + rec.alamat,
          //                   style: TextStyle(color: Colors.white)),
          //               Text('Total Item : ' + rec.totalitem,
          //                   style: TextStyle(color: Colors.white)),
          //               Text('Total Qty : ' + rec.totalqty,
          //                   style: TextStyle(color: Colors.white)),
          //               Text('Total Harga : ' + rec.totalharga,
          //                   style: TextStyle(color: Colors.white)),
          //             ],
          //           ),
          //         ),
          //       )
          //     ],
          //   ),
          // );
        },
        itemCount: list.length,
      ),
    );
  }
}
