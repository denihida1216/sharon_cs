import 'package:flutter/material.dart';
import 'package:sharon_cs/popupmenu.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sharon_cs/services/ApiService.dart';
import 'package:sharon_cs/widget/cekstore/CsPage.dart';
import 'package:sharon_cs/widget/cekstore/CsPageForm.dart';
import 'package:sharon_cs/widget/dashboard/DashboardModel.dart';
import 'package:sharon_cs/widget/users/loginpage.dart';

void main() {
  runApp(new MaterialApp(
    title: "Cekstore",
    home: new Page1(),
  ));
}

class Page1 extends StatefulWidget {
  static String tag = 'page1-page';
  // UsersModel usersModel;
  // Page1({this.usersModel});

  @override
  _Page1State createState() => _Page1State();
}

List<CustomPopupMenu> choices = <CustomPopupMenu>[
  CustomPopupMenu(title: 'Daftar User', icon: Icons.supervisor_account),
  CustomPopupMenu(title: 'Logout', icon: Icons.exit_to_app),
];

class _Page1State extends State<Page1> {
  BuildContext context;
  ApiService apiService;
  // CustomPopupMenu _selectedChoices = choices[0];
  // void _select(CustomPopupMenu choice) {
  //   setState(() {
  //      _selectedChoices = choice;
  //   });
  // }

  @override
  void initState() {
    apiService = ApiService();
    getPref();
    super.initState();
  }

  var uid;
  var nama;
  var kdjalur;
  var namajalur;
  getPref() async {
    
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      uid = preferences.getString("id");
      nama = preferences.getString("nama");
      kdjalur = preferences.getString("kd_jalur");
      namajalur = preferences.getString("nama_jalur");
      // preferences.setString("kd_jalur", kdjalur);
      // preferences.setString("nama_jalur", namajalur);
      // print(uid);
      // print(nama);
    });
  }

  // savePref(String kdjalur, String namajalur) async {
  //   SharedPreferences preferences = await SharedPreferences.getInstance();
  //   setState(() {
  //     preferences.setString("kd_jalur", kdjalur);
  //     preferences.setString("nama_jalur", namajalur);
  //     print(kdjalur);
  //     print(namajalur);
  //   });
  // }

  signOut() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      preferences.setInt("value", null);
      // preferences.commit();
    });
    Navigator.of(context).pushNamedAndRemoveUntil(
        LoginPage.tag, (Route<dynamic> route) => false);
  }

  pagelist(String value) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      preferences.setString("title", value);
      // preferences.commit();
    });
    Navigator.pushNamed(context, CsPage.tag);
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    return Scaffold(
      appBar: AppBar(
        title: new Text("Cekstore"),
        backgroundColor: Colors.brown,
        actions: [
          // IconButton(
          //     icon: Icon(Icons.exit_to_app),
          //     onPressed: () {
          //       signOut();
          //     }),
        ],
      ),
      body: FutureBuilder(
        future: apiService.getDashboard(uid),
        builder: (BuildContext context,
            AsyncSnapshot<List<DashboardModel>> snapshot) {
          if (snapshot.hasError) {
            return Center(
              child: Text(
                  "Something wrong with message: ${snapshot.error.toString()}"),
            );
          } else if (snapshot.connectionState == ConnectionState.done) {
            print(snapshot.data);
            List<DashboardModel> data = snapshot.data;
            DashboardModel rec = data[0];
            return ListView(
              children: <Widget>[
                Card(
                  child: ListTile(
                    onTap: null,
                    leading: CircleAvatar(
                      backgroundColor: Colors.transparent,
                      child: Image.asset('assets/user.png'),
                      // child: Image.network(rec.gambar),
                    ),
                    title: Row(
                      children: <Widget>[
                        Expanded(
                            child: Text(
                          rec.nama,
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w700,
                          ),
                        )),
                      ],
                    ),
                    subtitle: Row(
                      children: <Widget>[
                        Expanded(
                            child: Text(
                          'Tanggal: ' + rec.tanggal,
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w700,
                          ),
                        )),
                      ],
                    ),
                    trailing: IconButton(
                        icon: Icon(
                          Icons.exit_to_app,
                          size: 40,
                        ),
                        onPressed: () {
                          signOut();
                        }),
                  ),
                ),
                Card(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      CircleAvatar(
                        backgroundColor: Colors.transparent,
                        child: Container(
                          color: Colors.transparent,
                          child: Icon(
                            Icons.calendar_today,
                            color: Colors.brown,
                            size: 40,
                          ),
                        ),
                      ),
                      Text(
                        'kunjungan hari ini'.toUpperCase(),
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w700,
                          fontSize: 24.0,
                        ),
                      ),
                      Text(
                        rec.totaldikunjungi,
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w700,
                          fontSize: 36.0,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 10.0, left: 10.0, right: 10.0),
                        child: OutlineButton(
                          onPressed: () {
                            pagelist('kunjungan hari ini'.toUpperCase());
                          },
                          borderSide:
                              BorderSide(color: Colors.brown, width: 1.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(Icons.format_list_numbered),
                              SizedBox(
                                width: 5.0,
                              ),
                              Text('Lihat'),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          // pagelist('kunjungan hari ini'.toUpperCase());
          Navigator.pushNamed(context, CsPageForm.tag);
        },
        tooltip: 'Add Check Store',
        child: Icon(Icons.add),
        backgroundColor: Colors.brown,
      ),
    );
  }
}
